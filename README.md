# website

[![Documentation Status](https://readthedocs.org/projects/newmexicoforensicsclub/badge/?version=latest)](https://newexicoforensicsclub.readthedocs.io/en/latest/?badge=latest)

This [GitLab](https://about.gitlab.com/) repository is the source for the
[New Mexico Forensics Club website](https://newmexicoforensicsclub.readthedocs.io),
which is hosted for free by [ReadTheDocs](https://readthedocs.org/).

**Contents**
*  [Directory & File Structure](#directory-file-structure)
*  [Modifying a Page](#modifying-a-page)
*  [Adding a Page](#adding-a-page)
*  [Deleting a Page](#deleting-a-page)
*  [Uploading Files](#uploading-files)
*  [Troubleshooting](#troubleshooting)

## Directory & File Structure

All the source files for the various pages of the website live in the
[`doc`](/doc) directory.  The files use what's known as the
[reStructuredText](https://www.sphinx-doc.org/en/master/usage/restructuredtext/)
mark-up language, which allows you to write readable plain text, which can then
be translated to rich HTML.  They all use the `.rst` file extension to denote
that they're reStructuredText files.

Additionally, the [`images`](/images) directory contains any images used
throughout the site.
[Portable Network Graphics (PNGs)](https://en.wikipedia.org/wiki/Portable_Network_Graphics)
are the preferred file format.  The [`files`](/files) directory contains any
PDFs downloadable from the site.

## Modifying a Page

If you wanted to make changes to, for instance, the
[Resources](https://newmexicoforensicsclub.readthedocs.io/en/latest/resources.html)
page on the website, here are the steps you'd walk through:

1. Navigate to the [`doc`](/doc) page in GitLab.
1. Click on the [`resources.rst`](/doc/resources.rst) file.
1. Click on the "Edit" button.
1. Make whatever changes you like in the text editor.
   > **Note:**  You can click "Preview" to get an idea of what you're changes
   > will look like, but it won't be exact.  The "Preview" tab interprets the
   > text as [Markdown](https://en.wikipedia.org/wiki/Markdown) instead of
   > reStructuredText.
1. Below the text editor, you can modify the commit message to tie a meaningful
   message to you changes, e.g., "Finished introduction".
1. Click "Commit changes".

At this point you can go stretch your legs, grab a cup of coffee, etc.
ReadTheDocs will automatically begin rebuilding the website behind the scenes.
When a few minutes have passed, you can refresh the
[Resources](https://newmexicoforensicsclub.readthedocs.io/en/latest/resources.html)
page and see the updated changes.

## Adding a Page

The process of adding a page to the website is largely similar to the process
for modifying a page.

1. Navigate to the [`doc`](/doc) page in GitLab.
1. Near the top of the page, click on the "+" dropdown menu and choose "New
   file".
1. Enter a filename, which should be the title of the page, all lowercase,
   without any special characters, with spaces replaced by hyphens, and with
   the `.rst` extension.
1. Enter the contents of the page in the text editor, and commit them as you
   would for modifying a page.

> **Note:**  Be sure to make sure your new page is linked to from some other
> page.  This is normally done by adding the filename, without the extension,
> to a `.. toctree::` directive on another page.  See, e.g.,
> [`index.rst`](/doc/index.rst).

## Removing a Page

1. Navigate to the file in question in GitLab.
1. Near the top of the page, click the "Delete" button.
1. Change the commit message, if you like, and then click "Delete file".
1. Remember to remove any references to the deleted file.

## Uploading Files

If you need to add a files to the repository&mdash;for instance, a picture to
include in a page, or a PDF to link to&mdash;you'll need to:

1. Navigate to the directory in GitLab where you want the file to live
   ([`docs`](/docs), [`images`](/images), or [`files`](/files)).
1. Click the "+" dropdown menu as you did when creating a new file, but this
   time select "Upload file".
1. Drag and drop the file into the box or "click to upload".
1. Modify the commit message, if you like, and then click "Upload file".

To then include the image in a page, or create a link to download the file,
look for existing examples in the repository and just copy, paste, and modify.

## Troubleshooting

You may wind up in a situation where a change you make doesn't work out quite
right.  If you commit your change, wait a little bit, refresh the page on the
website, and either you don't see your changes or your changes don't look the
way you expected them to, here's how you go about troubleshooting the problem.

On the website, there's a little "Read the Docs" menu down in the bottom-left
corner of the page.  Click to open it up, and then click on the "Builds" link.
If the website is still being rebuilt, you'll see "Building" at the top.  If
something went wrong, you'll see "Failed".  Clicking on any of these statuses
will show you the details of that build, which may give you an idea of what
went wrong with your changes.  If you can't figure it out, just send an email
to newmexicoforensicsclub@gmail.com.
