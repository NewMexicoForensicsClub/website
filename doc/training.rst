Training
--------

The following is a collection of materials intended to teach you how to do
forensics.

.. toctree::
   :titlesonly:

   training/worldview-analysis
   training/biblical-hermeneutics

Speech-Writing
==============

.. toctree::
   :titlesonly:

   training/selecting-a-speech-topic
   training/conducting-research
   training/organizing-your-research
   training/structuring-your-speech
   training/using-evidence-in-a-speech
   training/outlining-your-speech

Presentation
============

.. toctree::
   :titlesonly:

   training/memorizing-your-piece
   training/delivering-your-speech

Argumentation
=============

.. toctree::
   :titlesonly:

   training/logical-fallacies
   training/asking-questions
   training/crafting-an-argument

Debate
======

.. toctree::
   :titlesonly:

   training/analyzing-a-resolution
   training/taking-notes-in-debate

