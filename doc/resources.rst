Resources
---------

.. _curricula:

Curricula
=========

.. note::

   These curricula are not essential for you to purchase or utilize in order to
   participate in our club; however, they are quality resources that you will
   find tremendously beneficial should you decide to invest in them.  You
   probably only need to purchase the Competitor's Handbook for any guide, as
   that should give you access to the Parent's Guide as a PDF download.

.. figure:: https://iew.com/sites/default/files/styles/product_main/public/paperbasedcourse/images/nc-gs-cp_thumb.jpg?itok=1C9HdE7g
   :figwidth: 125px
   :align: right

* The NCFCA Comprehensive Guide to Speech

  * `Competitor's Handbook <https://iew.com/shop/products/ncfca-comprehensive-guide-speech-competitors-handbook>`__ ($49)
  * `Parent's Guide <https://iew.com/shop/products/ncfca-comprehensive-guide-speech-parents-guide>`__ ($19)
  * `Coach's Manual <https://iew.com/shop/products/ncfca-comprehensive-guide-speech-coachs-manual>`__ ($59)
  * `Complete Set <https://iew.com/shop/products/ncfca-comprehensive-guide-speech-complete-set>`__ ($99)

.. figure:: https://iew.com/sites/default/files/styles/product_main/public/paperbasedcourse/images/nc-gpd-cp_thumb.jpg?itok=IWF5eewj
   :figwidth: 125px
   :align: right

* The NCFCA Comprehensive Guide to Policy Debate

  * `Competitor's Handbook <https://iew.com/shop/products/ncfca-comprehensive-guide-policy-debate-competitors-handbook>`__ ($49)
  * `Parent's Guide <https://iew.com/shop/products/ncfca-comprehensive-guide-policy-debate-parents-guide>`__ ($19)
  * `Coach's Manual <https://iew.com/shop/products/ncfca-comprehensive-guide-policy-debate-coachs-manual>`__ ($69)
  * `Complete Set <https://iew.com/shop/products/ncfca-comprehensive-guide-policy-debate-complete-set>`__ ($129)

.. figure:: https://iew.com/sites/default/files/styles/product_main/public/paperbasedcourse/images/nc-gvd-cp_thumb.jpg?itok=Nqp9bKgu
   :figwidth: 125px
   :align: right

* The NCFCA Comprehensive Guide to Value Debate

  * `Competitor's Handbook <https://iew.com/shop/products/ncfca-comprehensive-guide-value-debate-competitors-handbook>`__ ($49)
  * `Parent's Guide <https://iew.com/shop/products/ncfca-comprehensive-guide-value-debate-parents-guide>`__ ($19)
  * `Coach's Manual <https://iew.com/shop/products/ncfca-comprehensive-guide-value-debate-coachs-manual>`__ ($69)
  * `Complete Set <https://iew.com/shop/products/ncfca-comprehensive-guide-value-debate-complete-set>`__ ($129)

Books
=====

.. note::

   Again, you don't need to purchase any of these to participate in the club.
   For any that aren't available online for free, see if you can find them at
   your local library before purchasing them.  The order in which the books are
   listed doesn't have any particular significance.

General
^^^^^^^

* `How to Read a Book <https://smile.amazon.com/How-Read-Book-Classic-Intelligent/dp/0671212095/ref=sr_1_1?dchild=1&keywords=how+to+read+a+book&sr=8-1>`_, by Mortimer J. Adler and Charles Van Doren ($11)
* `How to Speak; How to Listen <https://smile.amazon.com/How-Speak-Listen-Mortimer-Adler/dp/0684846470/ref=sr_1_1?dchild=1&keywords=how+to+speak+how+to+listen&qid=1603465742&sr=8-1>`_, by Mortimer J. Adler ($17)
* `The Art of Public Speaking:  The Original Tool for Improving Public Oration <https://smile.amazon.com/Art-Public-Speaking-Original-Improving/dp/1945186488/ref=sr_1_2?crid=3C5HQQ45L0ZOI&dchild=1&keywords=the+art+of+public+speaking+dale+carnegie&qid=1600779619&sprefix=the+art+of+public+speaking+dale+%2Caps%2C224&sr=8-2>`_, by Dale Carnegie ($12)

.. _logic_books:

Logic
^^^^^

* `The Fallacy Detective:  Thirty-Eight Lessons on How to Recognize Bad Reasoning <https://smile.amazon.com/Fallacy-Detective-Thirty-Eight-Recognize-Reasoning/dp/097453157X/ref=sr_1_1?crid=2R5JXXAJH46X5&keywords=the+fallacy+detective&qid=1659901752&sprefix=the+fallacy+detec%2Caps%2C448&sr=8-1>`_, by Nathaniel Bluedorn & Hans Bluedorn ($26)
* `Introductory Logic:  The Fundamentals of Thinking Well <https://canonpress.com/products/intrologic/>`_, by Canon Press ($29)
* `Intermediate Logic:  Mastering Propositional Arguments <https://canonpress.com/products/new-intermediate-logic-complete-program-with-dvd-course/>`_, by Canon Press ($29)
* `The Amazing Dr. Ransom's Bestiary of Adorable Fallacies <https://smile.amazon.com/Amazing-Ransoms-Bestiary-Adorable-Fallacies/dp/1591281873/ref=sr_1_1?keywords=dr+ransoms+adorable+fallacies&qid=1659901832&sprefix=dr+ransom%2Caps%2C176&sr=8-1>`_, by Douglas Wilson & N.D. Wilson ($18)

Christian Worldview
^^^^^^^^^^^^^^^^^^^

* `A History of Western Philosophy and Theology <https://smile.amazon.com/History-Western-Philosophy-Theology/dp/162995084X/ref=sr_1_1?crid=L6DK6V39RXNO&dchild=1&keywords=a+history+of+western+philosophy+and+theology&sprefix=a+history+of+western+philosophy+and%2Caps%2C228&sr=8-1>`_, by John Frame ($45)
* `Introduction to Philosophy:  A Christian Perspective <https://smile.amazon.com/Introduction-Philosophy-Perspective-Norman-Geisler/dp/0801038189/ref=sr_1_1?dchild=1&keywords=introduction+to+philosophy+geisler&sr=8-1>`_, by Norman Geisler ($22)
* `Political Visions & Illusions:  A Survey & Christian Critique of Contemporary Ideologies <https://smile.amazon.com/Political-Visions-Illusions-Contemporary-Ideologies/dp/0830852425/ref=sr_1_1?keywords=political+visions+and+illusions&qid=1675889176&s=books&sprefix=political+visions%2Cstripbooks%2C149&sr=1-1>`_, by David Koyzis and Richard Mouw ($24)
* `Total Truth:  Liberating Christianity from Its Cultural Captivity <https://www.amazon.com/Total-Truth-Study-Guide-Christianity/dp/1433502208/ref=sr_1_1?crid=I7JJWRY1GM7C&dchild=1&keywords=total+truth+nancy+pearcey&qid=1606515393&sprefix=total+truth+na%2Caps%2C203&sr=8-1>`_, by Nancy Pearcey ($17)
* `The Consequences of Ideas:  Understanding the Concepts that Shaped Our World <https://smile.amazon.com/Consequences-Ideas-Redesign-Understanding-Concepts/dp/1433563770/ref=sr_1_1?dchild=1&keywords=the+consequences+of+ideas+sproul&sr=8-1>`_, by R.C. Sproul ($13)
* `Why You Think the Way You Do:  The Story of Western Worldviews from Rome to Home <https://smile.amazon.com/Why-You-Think-Way-Worldviews/dp/0310292301/ref=sr_1_1?crid=3BK89N9J7SYOX&keywords=why+you+think+the+way+you+do+sunshine&qid=1659901552&sprefix=why+you+think+the+way+you+do+sunshi%2Caps%2C224&sr=8-1>`_, by Glenn Sunshine ($19)

Philosophy
^^^^^^^^^^

* `Metaphysics (ca. 350 BC) <https://smile.amazon.com/Metaphysics-Dover-Thrift-Editions/dp/0486817490/ref=sr_1_1_sspa?dchild=1&keywords=metaphysics+aristotle&sr=8-1-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEzRjBYNzlGSUpZWDdaJmVuY3J5cHRlZElkPUEwMzI1Nzk5MkVMVkE2VUszRUdTMyZlbmNyeXB0ZWRBZElkPUEwODUxMDM5MjZXOThUTVpDQVIyMyZ3aWRnZXROYW1lPXNwX2F0ZiZhY3Rpb249Y2xpY2tSZWRpcmVjdCZkb05vdExvZ0NsaWNrPXRydWU=>`_, by Aristotle ($9)
* Five Dialogues (`Euthyphro <http://gutenberg.org/ebooks/1642>`_, `Apology <http://gutenberg.org/ebooks/1656>`_, `Crito <http://gutenberg.org/ebooks/1657>`_, `Meno <http://gutenberg.org/ebooks/1643>`_, `Phaedo <http://gutenberg.org/ebooks/1658>`_) (ca. 350 BC), by Plato ($0)
* Summa Theologica (`Part I <http://gutenberg.org/ebooks/17611>`_, `Part I-II <http://gutenberg.org/ebooks/17897>`_, `Part II-II <http://gutenberg.org/ebooks/18755>`_, `Part III <http://gutenberg.org/ebooks/19950>`_) (AD 1485), by Thomas Aquinas ($0)
* `Meditations on First Philosophy (1641) <https://smile.amazon.com/Meditations-First-Philosophy-Ren%C3%A9-Descartes/dp/B08GVJ6K2N/ref=sr_1_2_sspa?dchild=1&keywords=meditations+on+first+philosophy&sr=8-2-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUFSSFVUSEdMODFUNkkmZW5jcnlwdGVkSWQ9QTA0Njk1NTcyNUY1VjJJNERIM1dZJmVuY3J5cHRlZEFkSWQ9QTA0MzIzNTUxVUtDTTcxQVozVkhUJndpZGdldE5hbWU9c3BfYXRmJmFjdGlvbj1jbGlja1JlZGlyZWN0JmRvTm90TG9nQ2xpY2s9dHJ1ZQ==>`_, by René Descartes ($8)
* `An Enquiry Concerning Human Understanding (1748) <http://gutenberg.org/ebooks/9662>`_, by David Hume ($0)
* `Critique of Pure Reason (1781) <http://gutenberg.org/ebooks/4280>`_, by Immanuel Kant ($0)
* `Either/Or:  A Fragment of Life (1843) <https://smile.amazon.com/Either-Fragment-Life-Penguin-Classics/dp/0140445773/ref=sr_1_1?crid=1HS509OP6ZIJ8&dchild=1&keywords=either+or+kierkegaard&sprefix=either+or+kier%2Caps%2C224&sr=8-1>`_, by Søren Kierkegaard ($16)
* `A System of Logic, Ratiocinative and Inductive (1843) <http://gutenberg.org/ebooks/27942>`_, by John Stuart Mill ($0)

Politics
^^^^^^^^

* `The Republic (381 BC) <http://gutenberg.org/ebooks/1497>`_, by Plato ($0)
* City of God (`Volume I <http://gutenberg.org/ebooks/45304>`__, `Volume II <http://gutenberg.org/ebooks/45305>`__) (AD 426), by Augustine ($0)
* `The Prince (1532) <http://gutenberg.org/ebooks/1232>`_, by Niccolò Machiavelli ($0)
* `Vindiciae Contra Tyrannos: A Defense of Liberty Against Tyrants (1581) <https://www.amazon.com/Vindiciae-Contra-Tyrannos-Defense-Liberty/dp/1952410525/ref=sr_1_1?sr=8-1>`_, by Junius Brutus ($10)
* `Lex Rex (1644) <https://www.amazon.com/Lex-Rex-King-Samuel-Rutherford/dp/1952410533/ref=sr_1_1?keywords=lex+rex+samuel+rutherford&sr=8-1>`_, by Samuel Rutherford ($17)
* `Leviathan (1651) <https://www.gutenberg.org/ebooks/3207>`_, by Thomas Hobbes ($0)
* `Second Treatise on Civil Government (1689) <https://www.gutenberg.org/ebooks/7370/>`_, by John Locke ($0)
* `The Social Contract (1762) <http://gutenberg.org/ebooks/46333>`_, by Jean-Jaques Rousseau ($0)
* `The Declaration of Independence of the United States of America (1776) <http://gutenberg.org/ebooks/16780>`_, by Thomas Jefferson ($0)
* `Common Sense (1776) <http://gutenberg.org/ebooks/147>`_, by Thomas Paine ($0)
* `The Constitution of the United States of America (1787) <http://gutenberg.org/ebooks/5>`_ ($0)
* `The Federalist Papers (1787--1788) <http://gutenberg.org/ebooks/1404>`_, by Alexander Hamilton, James Madison, and John Jay ($0)
* `Reflections on the Revolution in France (1790) <http://gutenberg.org/ebooks/15679>`_, by Edmund Burke ($0)
* Democracy in America (`Volume I <http://gutenberg.org/ebooks/815>`__, `Volume II <http://gutenberg.org/ebooks/816>`__) (1835--1840), by Alexis de Tocqueville ($0)
* `The Law (1850) <http://gutenberg.org/ebooks/44800>`_, by Frédéric Bastiat ($0)
* `On Liberty (1859) <http://gutenberg.org/ebooks/34901>`_, by John Stuart Mill ($0)
* `Christian Political Action in an Age of Revolution (1860) <https://www.amazon.com/Christian-Political-Action-Revolution-Heritage/dp/1957905123/ref=sr_1_1?sr=8-1>`_, by Guillaume Groen van Prinsterer ($15)
* `The Communist Manifesto (1888) <http://gutenberg.org/ebooks/61>`_, by Karl Marx and Friedrich Engels ($0)
* `A Theory of Justice (1971) <https://smile.amazon.com/Theory-Justice-John-Rawls/dp/0674000781/ref=sr_1_1?crid=3LRT7N12SN7J2&dchild=1&keywords=a+theory+of+justice+john+rawls&qid=1600831409&sprefix=a+theory+of+justice%2Caps%2C241&sr=8-1>`_, by John Rawls ($38)
* `Law, Legistlation and Liberty (1973--1979) <https://smile.amazon.com/Law-Legislation-Liberty-statement-principles/dp/0415522293/ref=sr_1_3?crid=19F9F3DI9A7QM&dchild=1&keywords=law%2C+legislation+and+liberty&qid=1615752657&sprefix=law%2C+legis%2Caps%2C317&sr=8-3>`_, by F.A. Hayek ($33)
* `Christianity and the Constitution: The Faith of Our Founding Fathers (1987) <https://www.amazon.com/Christianity-Constitution-Faith-Founding-Fathers/dp/0801034442/ref=sr_1_1?sr=8-1>`_, by John Eidsmoe ($13)
* `A Conflict of Visions (1987) <https://smile.amazon.com/Conflict-Visions-Ideological-Political-Struggles/dp/0465002056/ref=sr_1_1?crid=3TK5FIBBMTRC1&dchild=1&keywords=a+conflict+of+visions+thomas+sowell&qid=1600831487&sprefix=a+conflict+of+visions%2Caps%2C231&sr=8-1>`_, by Thomas Sowell ($24)
* `The Vision of the Anointed: Self-Congratulation as a Basis for Social Policy (1996) <https://www.amazon.com/Vision-Anointed-Self-Congratulation-Social-Policy/dp/046508995X/ref=tmm_pap_swatch_0?_encoding=UTF8&sr=8-1>`_, by Thomas Sowell ($12)
* `Slaying Leviathan:  Limited Government and Resistance in the Christian Tradition (2020) <https://smile.amazon.com/Slaying-Leviathan-Government-Resistance-Christian/dp/195241072X/ref=sr_1_1?keywords=slaying+leviathan&qid=1659901947&sprefix=slaying+lev%2Caps%2C208&sr=8-1>`_, by Glenn Sunshine ($12)
* `The Case for Christian Nationalism (2022) <https://www.amazon.com/Case-Christian-Nationalism-Stephen-Wolfe/dp/1957905336/ref=sr_1_1?sr=8-1>`_, by Stephen Wolfe ($23)
* `Mere Christendom (2023) <https://www.amazon.com/Mere-Christendom-Bringing-Christianity-Secularism/dp/1957905573/ref=sr_1_1?sr=8-1>`_, by Douglas Wilson ($19)

Economics
^^^^^^^^^

* `The Wealth of Nations (1776) <http://gutenberg.org/ebooks/3300>`_, by Adam Smith ($0)
* `On the Principles of Political Economy, and Taxation (1817) <http://gutenberg.org/ebooks/33310>`_, by David Ricardo ($0)
* `Principles of Political Economy (1848) <http://gutenberg.org/ebooks/30107>`_, by John Stuart Mill ($0)
* `Das Kapital (1867) <https://smile.amazon.com/Capital-Classics-World-Literature-Marx/dp/1840226994/ref=sr_1_5?dchild=1&keywords=das+kapital+marx&sr=8-5>`_, by Karl Marx ($8)
* `Progress and Poverty (1879) <http://gutenberg.org/ebooks/55308>`_, by Henry George ($0)
* `The General Theory of Employment, Interest, and Money (1936) <https://smile.amazon.com/General-Theory-Employment-Interest-Money/dp/0156347113/ref=sr_1_1?crid=3DRSX2OMEX5SX&dchild=1&keywords=general+theory+of+employment+keynes&sprefix=general+theory+of+employment%2Caps%2C221&sr=8-1>`_, by John Maynard Keynes ($13)
* `The Road to Serfdom (1944) <https://smile.amazon.com/Road-Serfdom-Documents-Definitive-Collected/dp/0226320553/ref=sr_1_2?dchild=1&keywords=road+to+serfdom&sr=8-2>`_, by F.A. Hayek ($15)
* `Economics in One Lesson (1946) <https://smile.amazon.com/Economics-One-Lesson-Shortest-Understand/dp/0517548232/ref=sr_1_2?crid=2C13NIYKROOSH&dchild=1&keywords=economics+in+one+lesson+by+henry+hazlitt&sprefix=economics+in+on%2Caps%2C304&sr=8-2>`_, by Henry Hazlitt ($12)
* `Human Action:  A Treatise on Economics (1949) <https://smile.amazon.com/Human-Action-Ludwig-von-Mises/dp/1610161459/ref=sr_1_1?crid=3F7JVZJ7KWQC5&dchild=1&keywords=human+action+ludwig+von+mises&sprefix=human+action+%2Caps%2C202&sr=8-1>`_, by Ludwig von Mises ($25)
* `Free to Choose (1980) <https://smile.amazon.com/Free-Choose-Statement-Milton-Friedman/dp/0156334607/ref=sr_1_1?crid=8BMRWJHL6K0A&dchild=1&keywords=free+to+choose+by+milton+friedman&sprefix=free+to+choose%2Caps%2C212&sr=8-1>`_, by Milton and Rose Friedman ($13)
* `Basic Economics:  A Common Sense Guide to the Economy (2000) <https://smile.amazon.com/Basic-Economics-Thomas-Sowell/dp/0465060730/ref=sr_1_1?crid=2XAHBKCSPZ7IT&dchild=1&keywords=basic+economics+thomas+sowell&sprefix=basic+econom%2Caps%2C246&sr=8-1>`_, by Thomas Sowell ($30)
* `World Poverty and Human Rights (2002) <https://smile.amazon.com/World-Poverty-Human-Rights-Thomas/dp/074564144X/ref=sr_1_1?crid=PS5YGQIPUD4V&dchild=1&keywords=world+poverty+and+human+rights&sprefix=world+poverty%2Caps%2C236&sr=8-1>`_, by Thomas Pogge ($32)
* `Mis-Inflation: The Truth about Inflation, Pricing, and the Creation of Wealth (2022) <https://www.amazon.com/Mis-Inflation-Inflation-Pricing-Creation-Wealth/dp/1957905093/ref=sr_1_1?sr=8-1>`_, by David L. Bahsen & Douglas Wilson ($13)

Culture
^^^^^^^

* `Black Rednecks and White Liberals (2006) <https://www.amazon.com/Black-Rednecks-Liberals-Thomas-Sowell/dp/1594031436/ref=sr_1_1?sr=8-1>`_, by Thomas Sowell ($23)
* `The Church Impotent: The Feminization of Christianity (1999) <https://podles.org/church-impotent.htm>`_, by Leon J. Podles ($0)
* `Black & Tan: A Collection of Essays and Excursions on Slavery, Culture War, and Scripture in America (2005) <https://www.amazon.com/Black-Tan-Collection-Excursions-Scripture/dp/159128032X/ref=sr_1_1?sr=8-1>`_, by Douglas Wilson ($17)
* `Live Not by Lies: A Manual for Christian Dissidents (2020) <https://www.amazon.com/Live-Not-Lies-Christian-Dissidents/dp/0593087399/ref=sr_1_1?dchild=1&sr=8-1>`_, by Rod Dreher ($14)
* `Masculine Christianity (2020) <https://www.amazon.com/Masculine-Christianity-Zachary-Garris/dp/1735473901/ref=sr_1_1?sr=8-1>`_, by Zachary Garris ($17)
* `Fault Lines: The Social Justice Movement and Evangelicalism’s Looming Catastrophe (2021) <https://www.amazon.com/Fault-Lines-Movement-Evangelicalisms-Catastrophe/dp/1684511801/ref=sr_1_1?dchild=1&sr=8-1>`_, by Voddie Baucham Jr ($10)
* `The Toxic War on Masculinity: How Christianity Reconciles the Sexes (2023) <https://www.amazon.com/Toxic-War-Masculinity-Christianity-Reconciles/dp/0801075734/ref=sr_1_1?sr=8-1>`_, by Nancy Pearcey ($16)
* `It’s Not Like Being Black: How Sexual Activists Hijacked the Civil Rights Movement (2024) <ihttps://www.amazon.com/gp/product/1684513642/ref=ppx_od_dt_b_asin_title_s00?ie=UTF8&psc=1>`_, by Voddie Baucham Jr ($30)

Apologetics
^^^^^^^^^^^

Overviews
+++++++++

* `Expository Apologetics:  Answering Objections with the Power of the Word <https://smile.amazon.com/Expository-Apologetics-Answering-Objections-Power/dp/1433533790/ref=sr_1_1?crid=NBF4U5SDL8L&dchild=1&keywords=expository+apologetics+voddie+baucham&qid=1600779698&sprefix=expository+apolo%2Caps%2C313&sr=8-1>`_, by Voddie Baucham Jr ($16)
* `The Ever-Loving Truth:  Can Faith Thrive in a Post-Christian Culture? <https://smile.amazon.com/Ever-Loving-Truth-Thrive-Post-Christian-Culture/dp/0805427880/ref=sr_1_1?keywords=the+ever+loving+truth+voddie+baucham&qid=1675887814&sprefix=the+ever+loving+t%2Caps%2C163&sr=8-1&ufe=app_do%3Aamzn1.fos.18ed3cb5-28d5-4975-8bc7-93deae8f9840>`_, by Voddie Baucham Jr ($22)
* `How Now Shall We Live? <https://smile.amazon.com/How-Now-Shall-We-Live/dp/084235588X/ref=sr_1_1?crid=2485KML0L7P3V&keywords=how+now+shall+we+live&qid=1675911516&s=books&sprefix=how+now+shall+we+liv%2Cstripbooks%2C137&sr=1-1>`_, by Charles Colson and Nancy Pearcey ($20)
* `Evidence for God:  50 Arguments for Faith from the Bible, History, Philosophy, and Science <https://smile.amazon.com/Evidence-God-Arguments-History-Philosophy/dp/0801072603/ref=sr_1_1?keywords=evidence+for+god+dembski&qid=1675887991&sprefix=evidence+for+god+demb%2Caps%2C155&sr=8-1>`_, edited by William Dembski and Michael Licona ($20)
* `Mama Bear Apologetics:  Empowering Your Kids to Challenge Cultural Lies <https://smile.amazon.com/Mama-Bear-Apologetics-Empowering-Challenge/dp/0736976159/ref=tmm_pap_swatch_0?_encoding=UTF8&qid=1668194039&sr=8-1>`_, edited by Hillary Morgan Ferrer ($15)
* `When Skeptics Ask:  A Handbook on Christian Evidences <https://smile.amazon.com/When-Skeptics-Ask-Christian-Evidences/dp/0801014980/ref=sr_1_1?keywords=when+skeptics+ask+norman+geisler&qid=1675888172&sprefix=when+skeptics+%2Caps%2C152&sr=8-1>`_, by Norman Geisler and Ronald Brooks ($18)
* `Dug Down Deep:  Building Your Life on Truths That Last <https://smile.amazon.com/Dug-Down-Deep-Building-Truths/dp/1601423713/ref=sr_1_1?crid=F9O6V8EEQ881&keywords=dug+down+deep+harris&qid=1675889550&s=books&sprefix=dug+down+deep+harris%2Cstripbooks%2C140&sr=1-1>`_, by Joshua Harris ($17)
* `The Right Questions <https://smile.amazon.com/Right-Questions-Meaning-Public-Debate-ebook/dp/B00EQVVVRA?ref_=ast_author_dp>`_, by Phillip E. Johnson ($13)
* `Tactics: A Game Plan for Discussing Your Christian Convictions (2019) <https://www.amazon.com/Tactics-10th-Anniversary-Discussing-Convictions/dp/0310101468/ref=sr_1_1?s=books&sr=1-1>`_, by Greg Koukl ($12)
* `The Case for a Creator:  A Journalist Investigates Scientific Evidence That Points Toward God <https://smile.amazon.com/Case-Creator-Journalist-Investigates-Scientific/dp/0310339286/ref=sr_1_1?crid=RL5K9HL25Q73&keywords=the+case+for+creator+strobel&qid=1675888920&s=books&sprefix=the+case+for+creator+strobel%2Cstripbooks%2C135&sr=1-1>`_, by Lee Strobel ($20)
* `The Case for Christ:  A Journalist's Personal Investigation of the Evidence for Jesus <https://smile.amazon.com/Case-Christ-Journalists-Personal-Investigation/dp/0310345863/ref=sr_1_1?crid=3RSIPW1621DBJ&keywords=the+case+for+christ&qid=1675888823&s=books&sprefix=the+case+for+christ%2Cstripbooks%2C152&sr=1-1>`_, by Lee Strobel ($17)
* `The Case for Faith:  A Journalist Investigates the Toughest Objections to Christianity <https://smile.amazon.com/Case-Faith-Journalist-Investigates-Christianity/dp/0310364272/ref=sr_1_1?crid=2MN4TCINU7MO5&keywords=the+case+for+faith+strobel&qid=1675888878&s=books&sprefix=the+case+for+faith+strobel%2Cstripbooks%2C144&sr=1-1>`_, by Lee Strobel ($18)
* `Cold-Case Christianity:  A Homicide Detective Investigates the Claims of the Gospels <https://smile.amazon.com/Cold-Case-Christianity-Homicide-Detective-Investigates/dp/1434704696/ref=sr_1_1?keywords=cold+case+christianity&qid=1675911296&s=books&sprefix=cold+case+ch%2Cstripbooks%2C165&sr=1-1>`_, by J. Warner Wallace ($17)
* `Person of Interest:  Why Jesus Still Matters in a World that Rejects the Bible (2021) <https://www.amazon.com/Person-Interest-Jesus-Matters-Rejects/dp/0310111277/ref=sr_1_1?sr=8-1>`_, by J. Warner Wallace ($16)
* `Beyond Opinion:  Living the Faith We Defend <https://smile.amazon.com/Beyond-Opinion-Living-Faith-Defend/dp/0849946530/ref=sr_1_1?crid=32CQ5SMNPP1EC&keywords=beyond+opinion+zacharias&qid=1675889515&s=books&sprefix=beyond+opinion+zacharia%2Cstripbooks%2C153&sr=1-1>`_, edited by Ravi Zacharias ($16)

Reference Books
+++++++++++++++

* `Historical Theology:  An Introduction to Christian Doctrine <https://www.amazon.com/Historical-Theology-Introduction-Christian-Doctrine/dp/0310230136/ref=sr_1_1?sr=8-1>`_, by Gregg Allison ($34)
* `Baker Encyclopedia of Christian Apologetics <https://smile.amazon.com/Encyclopedia-Christian-Apologetics-Reference-Library/dp/0801021510/ref=sr_1_1?keywords=baker+encyclopedia+of+christian+apologetics&qid=1675888097&sprefix=baker+enc%2Caps%2C217&sr=8-1&ufe=app_do%3Aamzn1.fos.18ed3cb5-28d5-4975-8bc7-93deae8f9840>`_, by Norman L. Geisler ($18)
* `Systematic Theology:  An Introduction to Biblical Doctrine <https://smile.amazon.com/Systematic-Theology-Second-Introduction-Biblical/dp/0310517974/ref=sr_1_1?keywords=systematic+theology+wayne+grudem&qid=1675912074&sprefix=systemati%2Caps%2C156&sr=8-1>`_, by Wayne Grudem ($37)
* `The Portable Seminary:  A Master's Level Overview in One Volume <https://smile.amazon.com/Portable-Seminary-Masters-Level-Overview/dp/0764219650/ref=sr_1_1?keywords=portable+seminary+david+horton&qid=1675912132&sprefix=portable+seminary%2Caps%2C149&sr=8-1>`_, edited by David Horton ($29)
* `Evidence That Demands a Verdict:  Life-Changing Truth for a Skeptical World <https://smile.amazon.com/Evidence-That-Demands-Verdict-Life-Changing/dp/1401676707/ref=sr_1_1?crid=1WHBKUBSY9GVB&dchild=1&keywords=evidence+that+demands+a+verdict&qid=1615752606&sprefix=evidenc%2Caps%2C239&sr=8-1>`_, by Josh McDowell ($17)
* `Essential Truths of the Christian Faith <https://smile.amazon.com/Essential-Truths-Christian-Faith-Sproul/dp/0842320016/ref=sr_1_1?crid=HREJLM7LOX17&keywords=essentials+of+the+christian+faith+sproul&qid=1675889367&s=books&sprefix=essentials+of+the+christian+faith+sproul%2Cstripbooks%2C141&sr=1-1>`_, by R.C. Sproul ($15)
* `Evangelical Dictionary of Theology <https://smile.amazon.com/Evangelical-Dictionary-Theology-Daniel-Treier/dp/0801039460/ref=sr_1_1?crid=R8JUWG1DNTQI&keywords=evangelical+dictionary+of+theology&qid=1675911903&sprefix=evangelical+dictionary+of+theology%2Caps%2C176&sr=8-1>`_, edited by Daniel Treler and Walter Elwell ($44)

Naturalism
++++++++++

* `In Six Days: Why Fifty Scientists Choose to Believe in Creation (2001) <https://www.amazon.com/Six-Days-Scientists-Believe-Creation/dp/0890513414/ref=sr_1_1?sr=8-1>`_, edited by John F. Ashton ($14)
* `Darwin's Black Box:  The Biochemical Challenge to Evolution <https://smile.amazon.com/Darwins-Black-Box-Biochemical-Challenge/dp/0743290313/ref=sr_1_1?crid=1NCK1FGNVP1ND&keywords=darwin%27s+black+box&qid=1675887898&sprefix=darwin%27s+black+box%2Caps%2C149&sr=8-1>`_, by Michael J. Behe ($11)
* `Darwin on Trial <https://smile.amazon.com/Darwin-Trial-Phillip-Johnson-ebook/dp/B017QL9AVU?ref_=ast_author_dp>`_, by Phillip E. Johnson ($21)
* `Defeating Darwinism by Opening Minds <https://smile.amazon.com/Defeating-Darwinism-Opening-Phillip-Johnson-ebook/dp/B00436EZV2?ref_=ast_author_mpb>`_, by Phillip E. Johnson ($13)
* `Objections Sustained:  Subversive Essays on Evolution, Law & Culture <https://smile.amazon.com/Objections-Sustained-Subversive-Evolution-Culture/dp/083081941X?ref_=ast_author_dp>`_, by Phillip E. Johnson ($12)
* `How to Be an Atheist: Why Many Skeptics Aren’t Skeptical Enough (2016) <https://www.amazon.com/How-Be-Atheist-Skeptics-Skeptical/dp/1433542986/ref=sr_1_1?sr=8-1>`_, by Mitch Stokes ($10)
* `Refuting the New Atheists: A Christian Response to Sam Harris, Christopher Hitchens, and Richard Dawkins (2021) <https://www.amazon.com/Refuting-New-Atheists-Christian-Christopher/dp/1952410924/ref=sr_1_1?sr=8-1>`_, by Douglas Wilson ($19)

Other Religions
+++++++++++++++

* `Encyclopedia of Cults and New Religions <https://smile.amazon.com/Encyclopedia-Cults-New-Religions-Unitarianism/dp/0736900748/ref=sr_1_1?crid=2EWERE2DU0VLB&keywords=encyclopedia+of+cults+and+new+religions&qid=1675887683&sprefix=encyclopedia+of+cults+and+new+religions%2Caps%2C217&sr=8-1>`_, by John Ankerberg and John Weldon ($11)
* `Encyclopedia of New Age Beliefs <https://smile.amazon.com/Encyclopedia-New-Beliefs-Defense-Faith/dp/1565071603/ref=sr_1_1?crid=1VWW1G3VHMA4V&keywords=encyclopedia+of+new+age+beliefs&qid=1675887766&sprefix=encyclopedia+of+new+age+beliefs%2Caps%2C154&sr=8-1>`_, by John Ankerberg and John Weldon ($10)
* `Strong and Courageous:  Following Jesus Amid the Rise of America's New Religion <https://smile.amazon.com/Strong-Courageous-Following-Americas-Religion-ebook/dp/B08ZW7N2JD/ref=sr_1_1?crid=1I2A2KV8WNT9U&keywords=strong+and+courageous+ascol&qid=1675889278&s=books&sprefix=strong+and+courageous+ascol%2Cstripbooks%2C135&sr=1-1>`_, by Tom Ascol and Jared Longshore ($19)
* `The Kingdom of the Cults <https://smile.amazon.com/Kingdom-Cults-Definitive-Work-Subject/dp/0764232657/ref=sr_1_1?crid=2UPIBO4ZB9R0W&keywords=the+kingdom+of+the+cults&qid=1675889062&s=books&sprefix=the+kingdom+of+the+cult%2Cstripbooks%2C145&sr=1-1>`_, by Walter Martin ($18)
* `Handbook of Today's Religions <https://smile.amazon.com/Handbook-Todays-Religions-Josh-McDowell/dp/0840735014/ref=sr_1_1?crid=1KYAF2QAHLXXE&keywords=handbook+of+today%27s+religions&qid=1675888636&s=books&sprefix=handbook+of+today%27s+religions%2Cstripbooks%2C192&sr=1-1>`_, by Josh McDowell and Don Stewart ($9)
* `Seeking Allah, Finding Jesus: A Devout Muslim Encounters Christianity (2018) <https://www.amazon.com/Seeking-Allah-Finding-Jesus-Christianity/dp/0310515025/ref=sr_1_2?sr=8-2>`_, by Nabeel Qureshi ($28)
* `So What's the Difference? <https://smile.amazon.com/So-Whats-Difference-Fritz-Ridenour/dp/0764215647/ref=sr_1_1?crid=1CIY5KE8EQ37L&keywords=what%27s+the+difference+ridenour&qid=1675888768&s=books&sprefix=what%27s+the+difference+ridenou%2Cstripbooks%2C140&sr=1-1>`_, by Fritz Ridenour ($13)
* `Jesus Among Other Gods:  The Absolute Claims of the Christian Message <https://smile.amazon.com/Jesus-Among-Other-Gods-Christian/dp/0849943272/ref=sr_1_1?crid=2IQ5OS1MCKL6F&keywords=jesus+among+other+gods&qid=1675889011&s=books&sprefix=jesus+among+other+gods%2Cstripbooks%2C145&sr=1-1>`_, by Ravi Zacharias ($30)

