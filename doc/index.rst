New Mexico Forensics Club
-------------------------

.. role:: raw-html(raw)
   :format: html

Welcome to the New Mexico Forensics Club, an academic speech and debate
organization geared toward the homeschool Christian community, but open to
participants from any educational setting or faith background.

.. admonition::  Announcement

   The club is on hiatus for the 2024--2025 school year while much of my free
   time is eaten up by another project.

   If you're wanting to incorporate speech and debate into your family
   homeschool on your own, the :ref:`curricula` we recommend are really
   top-notch.

   You can always make use of our free :doc:`training materials <training>` and
   :doc:`workshop and seminar recordings <workshops>`.  If your church or
   homeschool co-op would like to book me for a class or event on any of these
   materials, send an email to newmexicoforensicsclub@gmail.com.

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/NVSDNDDBvBE" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

.. raw:: html

   <br>
   <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
       <iframe src="https://calendar.google.com/calendar/embed?height=600&amp;wkst=1&amp;bgcolor=%23ffffff&amp;ctz=America%2FDenver&amp;src=bmV3bWV4aWNvZm9yZW5zaWNzY2x1YkBnbWFpbC5jb20&amp;color=%23039BE5&amp;showNav=1&amp;showPrint=1&amp;showTabs=1&amp;showCalendars=1&amp;showTitle=0&amp;mode=MONTH" style="border:solid 1px #777; position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;" frameborder="0" scrolling="no"></iframe>
   </div>

.. toctree::
   :hidden:

   about
   events
   training
   resources
   workshops
   videos
   join
   contact
