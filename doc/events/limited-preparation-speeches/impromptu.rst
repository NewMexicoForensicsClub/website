Impromptu
---------

+--------+------------+
| League | Event Name |
+========+============+
| NCFCA  | Impromptu  |
+--------+------------+
| NSDA   | Impromptu  |
+--------+------------+
| Stoa   |            |
+--------+------------+

*Impromptu* is an event in which competitors use very little preparation time to
give a brief speech about a randomly selected topic.  Topics can be anything
under the sun, and speeches can be serious, light-hearted, or anywhere in
between.

.. note::

   * The NSDA's *Impromptu* is a consolation event at national competitions,
     meaning if you are knocked out of your main event, you can re-register in
     this one.
   * In the NCFCA, you have two minutes to prepare a five-minute speech.  In
     the NSDA, you have seven minutes total to allocate between preparation and
     speaking time however you like.

Example
=======

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/rF5mN5p1N78" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Resources
=========

* :download:`Impromptu Rules (NCFCA) </../files/ncfca-impromptu-rules.pdf>`
* :download:`General Speech Rules (NCFCA) </../files/ncfca-speech-rules.pdf>`
* :download:`An Introduction to Impromptu (NSDA) </../files/nsda-impromptu-description.pdf>`

Judging
=======

* :download:`Impromptu Ballot (NSDA) </../files/nsda-impromptu-ballot.pdf>`
* :download:`Impromptu Ballot with Comments (NSDA) </../files/nsda-impromptu-ballot-comments.pdf>`
