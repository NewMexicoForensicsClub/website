Extemporaneous Speaking
-----------------------

+--------+-----------------------------------------+---------------------------+
| League | Event Name                              | Variations                |
+========+=========================================+===========================+
| NCFCA  | Extemporaneous                          |                           |
+--------+-----------------------------------------+---------------------------+
| NSDA   | | United States Extemporaneous Speaking | Extemporaneous Commentary |
|        | | International Extemporaneous Speaking |                           |
+--------+-----------------------------------------+---------------------------+
| Stoa   | Extemporaneous                          |                           |
+--------+-----------------------------------------+---------------------------+

*Extemporaneous Speaking* is an event in which participants are given a
half-hour to prepare a seven-minute speech on a randomly drawn question
relating to current events.  Speeches should answer the question definitively
and provide supporting evidence and analysis.

.. note::

   * The NSDA separates domestic and international topics into two separate
     events, in which students will compete in only one, but the NCFCA and Stoa
     maintain that a competitor should be prepared to speak on any current
     issue, domestic or international.
   * The NSDA's *Extemporaneous Commentary* is a supplemental event at national
     competitions, meaning if you qualify in a main event, you can register to
     compete in this as well.  It's twenty minutes of preparation for a
     five-minute speech that is more akin to the commentary you may here in a
     TV or radio news program.

Example
=======

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/fvf4qrgiOQg" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Resources
=========

* `Extemp Genie <https://extempgenie.com/>`_
* :download:`Extemporaneous Rules (NCFCA) </../files/ncfca-extemporaneous-rules.pdf>`
* :download:`General Speech Rules (NCFCA) </../files/ncfca-speech-rules.pdf>`
* :download:`An Introduction to Extemporaneous Speaking (NSDA) </../files/nsda-extemporaneous-speaking-description.pdf>`
* :download:`An Introduction to United States Extemporaneous Speaking (NSDA) </../files/nsda-united-states-extemporaneous-speaking-description.pdf>`
* :download:`An Introduction to International Extemporaneous Speaking (NSDA) </../files/nsda-international-extemporaneous-speaking-description.pdf>`
* :download:`What to Expect Competing in United States Extemporaneous Speaking (NSDA) </../files/nsda-united-states-extemporaneous-speaking-what-to-expect.pdf>`
* :download:`What to Expect Competing in International Extemporaneous Speaking (NSDA) </../files/nsda-international-extemporaneous-speaking-what-to-expect.pdf>`
* :download:`An Introduction to Extemporaneous Commentary (NSDA) </../files/nsda-extemporaneous-commentary-description.pdf>`
* :download:`Extemporaneous Rules (Stoa) </../files/stoa-extemporaneous-rules.pdf>`

Judging
=======

* :download:`An Introduction to Evaluating Extemporaneous Speaking (NSDA) </../files/nsda-extemporaneous-speaking-judging.pdf>`
* :download:`Extemporaneous Speaking Ballot (NSDA) </../files/nsda-extemporaneous-speaking-ballot.pdf>`
* :download:`Extemporaneous Speaking Ballot with Comments (NSDA) </../files/nsda-extemporaneous-speaking-ballot-comments.pdf>`
* :download:`Extemporaneous Ballot (Stoa) </../files/stoa-extemporaneous-ballot.pdf>`
