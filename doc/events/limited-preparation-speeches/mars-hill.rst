Mars Hill
---------

+--------+------------+
| League | Event Name |
+========+============+
| NCFCA  |            |
+--------+------------+
| NSDA   |            |
+--------+------------+
| Stoa   | Mars Hill  |
+--------+------------+

*Mars Hill*, taking its name from
`Paul's speech on Mars Hill <https://en.wikipedia.org/wiki/Areopagus_sermon>`_
in Athens, is an event in which competitors have four minutes to prepare a
six-minute speech, in which they use contemporary cultural references as a
vehicle for communicating biblical truths.

Example
=======

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/fnf4SP_1AIA" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Resources
=========

* :download:`Mars Hill Rules (Stoa) </../files/stoa-mars-hill-rules.pdf>`
* :download:`Mars Hill Topics (Stoa) </../files/stoa-mars-hill-topics.pdf>`

Judging
=======

* :download:`Mars Hill Ballot (Stoa) </../files/stoa-mars-hill-ballot.pdf>`
