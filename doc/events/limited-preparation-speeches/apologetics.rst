Apologetics
-----------

+--------+-------------+
| League | Event Name  |
+========+=============+
| NCFCA  | Apologetics |
+--------+-------------+
| NSDA   |             |
+--------+-------------+
| Stoa   | Apologetics |
+--------+-------------+

*Apologetics* is an event in which competitors have four minutes to prepare a
six-minute speech on a randomly drawn topic.  The goal is to present
well-reasoned arguments in support of Christian doctrine and worldview.

Example
=======

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/rp1dGUKFQtY" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Resources
=========

* :download:`Apologetics Rules (NCFCA) </../files/ncfca-apologetics-rules.pdf>`
* :download:`General Speech Rules (NCFCA) </../files/ncfca-speech-rules.pdf>`
* :download:`Apologetics Topics (NCFCA) </../files/ncfca-apologetics-topics.pdf>`
* :download:`Apologetics Rules (Stoa) </../files/stoa-apologetics-rules.pdf>`
* :download:`Apologetics Topics (Stoa) </../files/stoa-apologetics-topics.pdf>`

Judging
=======

* :download:`Apologetics Ballot (Stoa) </../files/stoa-apologetics-ballot.pdf>`
