Limited Preparation Speeches
----------------------------

While platform speeches help a speaker develop their overall speech-writing and
delivery skills, limited preparation speeches help them to hone those skills
for the times when one is required to speak on the spur of the moment.
Depending on the event, the time the speaker has to prepare may be as long as a
half-hour, or as short as a few minutes.  While one cannot prepare the speech
itself ahead of time, there is plenty of work to be done in advance in terms of
research and practice.

Limited preparation speeches help students develop the ability to:

* catalogue and organize copious amounts of research,
* stay well-informed on a wide spectrum of issues,
* quickly craft a compelling argument,
* support that argument with convincing sources,
* speak on the fly with poise and confidence, and
* authentically connect with the audience.

There are four general types of limited preparation speeches:

.. toctree::
   :titlesonly:

   limited-preparation-speeches/extemporaneous-speaking
   limited-preparation-speeches/apologetics
   limited-preparation-speeches/mars-hill
   limited-preparation-speeches/impromptu
