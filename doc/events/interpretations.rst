Interpretations
---------------

Interpretation events are where performers get to develop and hone their acting
skills.  While some events are akin to short plays, others are along the lines
of a monologue or literature reading.  In all cases, the events are designed to
allow the speaker(s) to bring their own interpretation of a piece (or pieces)
of literature to bear.

Interpretations help students to:

* understand literature from the author's perspective,
* develop their own take on a piece of literature,
* draw the audience into the scene, without props or costumes,
* develop their sense of comedic/dramatic timing,
* evoke emotion from the audience, and
* communicate truths using performance of literature as the medium.

The various types of interpretations include:

.. toctree::
   :titlesonly:

   interpretations/humorous-interpretation
   interpretations/dramatic-interpretation
   interpretations/duo-interpretation
   interpretations/thematic-interpretation
   interpretations/prose
   interpretations/poetry
   interpretations/storytelling
