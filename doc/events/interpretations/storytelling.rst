Storytelling
------------

+--------+--------------+
| League | Event Name   |
+========+==============+
| NCFCA  |              |
+--------+--------------+
| NSDA   | Storytelling |
+--------+--------------+
| Stoa   |              |
+--------+--------------+

*Storytelling* is a five-minute event in which competitors select a children's
story that fits a given theme, and then act out the story as if to an audience
of children.

.. note::
    
   The NSDA's *Storytelling* is a consolation event at national competitions,
   meaning if you get knocked out of a main event, you can re-register in this
   one.

Example
=======

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/Jq-uktlp_R8" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Resources
=========

* :download:`An Introduction to Storytelling (NSDA) </../files/nsda-storytelling-description.pdf>`

Judging
=======

* :download:`Storytelling Ballot (NSDA) </../files/nsda-storytelling-ballot.pdf>`
