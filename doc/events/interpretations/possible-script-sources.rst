Possible Script Sources
=======================

Finding a good script takes time and discernment.  The following is a list of
sites you might try searching.  Keep in mind that you will be cutting your
script down to size to fit the time allotted for your event, and you have the
ability to slightly alter the wording (i.e., censor the piece), if necessary.
Christian sources are denoted with a cross ✝.

.. raw:: html

   <div style="display: flex;">
     <div style="flex: 50%;">
       <ul>
         <li><a href="http://www.alibris.com/">Alibris</a>
         <li><a href="https://www.brookpub.com/">Brooklyn Publishers</a>
         <li><a href="http://www.christian-drama.org/">Christian Drama Scripts</a> ✝
         <li><a href="https://christianplaysandskits.com/wp/">Christian Plays and Skits</a> ✝
         <li><a href="https://christianskitsource.com/ink-script-categories/">Christian Skit Source</a> ✝
         <li><a href="http://www.samuelfrench.com/">Concord Theatricals</a>
         <li><a href="http://www.dramaticpublishing.com/">Dramatic Publishing</a>
         <li><a href="http://www.dramatists.com/index.asp">Dramatists Play Service</a>
         <li><a href="https://www.dramatix.org.nz/">Dramatix</a> ✝
         <li><a href="https://dramashare.org/">Drama Share</a> ✝
         <li><a href="http://www.histage.com/">Eldrige Plays & Musicals</a>
         <li><a href="http://www.forensicscommunity.com/home">Forensics Community</a>
         <li><a href="https://godlypearls.com/christian-drama-scripts/">Godly Pearls</a> ✝
       </ul>
     </div>
     <div style="flex: 50%;">
       <ul>
         <li><a href="https://www.greenroompress.com/default.aspx?pg=sl&cl=Creative+Ideas">Green Room Press</a>
         <li><a href="http://www.hitplays.com/?gclid=CIy-rK2C94gCFQgZUAodrRR20g">Heuer Publishing</a>
         <li><a href="https://howitillerdrama.com/">Howi Tiller Christian Drama</a> ✝
         <li><a href="http://jddramapublishing.com/">JD Drama Publishing</a>
         <li><a href="https://www.lillenasdrama.com/nphweb/html/ldol/downloadableScripts.jsp">Lillenas Drama</a> ✝
         <li><a href="https://openlibrary.org/">Open Library</a>
         <li><a href="http://www.pioneerdrama.com/">Pioneer Drama Service</a>
         <li><a href="https://www.playscripts.com/forensics">Playscripts</a>
         <li><a href="https://scriptsbywarren.com/">Scripts by Warren</a> ✝
         <li><a href="http://speechgeekmarket.com/collections/sgm-interp">SpeechGeek</a>
         <li><a href="https://www.theatrefolk.com/">Theaterfolk</a>
         <li><a href="http://www.theinterpstore.com/">The Interp Store</a>
         <li><a href="https://skitguys.com/scripts">The Skit Guys</a> ✝
       </ul>
     </div>
   </div>

