Thematic Interpretation
-----------------------

+--------+-----------------------------+
| League | Event Name                  |
+========+=============================+
| NCFCA  | Biblical Thematic           |
+--------+-----------------------------+
| NSDA   | Program Oral Interpretation |
+--------+-----------------------------+
| Stoa   |                             |
+--------+-----------------------------+

*Thematic Interpretation* is an event in which competitors select three or more
pieces of literature (whether plays, poetry, prose, or songs) and arrange them
to speak to a particular theme.  Though the speech is memorized, the performer
uses a three-ringed binder with the script in it as if they're reading from a
book.

.. note::

   The NCFCA's *Biblical Thematic* must include at least one selection from the
   Bible, and also includes a visual aid of some sort.

Example
=======

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/nkyMaJ1tPHY" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Resources
=========

* :download:`Biblical Thematic Rules (NCFCA) </../files/ncfca-biblical-thematic-rules.pdf>`
* :download:`General Speech Rules (NCFCA) </../files/ncfca-speech-rules.pdf>`
* :download:`An Introduction to Program Oral Interpretation (NSDA) </../files/nsda-program-oral-interpretation-description.pdf>`
* :download:`What to Expect Competing in Program Oral Interpretation (NSDA) </../files/nsda-program-oral-interpretation-what-to-expect.pdf>`

Judging
=======

* :download:`An Introduction to Evaluating Program Oral Interpretation (NSDA) </../files/nsda-program-oral-interpretation-judging.pdf>`
* :download:`Program Oral Interpretation Ballot (NSDA) </../files/nsda-program-oral-interpretation-ballot.pdf>`
* :download:`Program Oral Interpretation Ballot with Comments (NSDA) </../files/nsda-program-oral-interpretation-ballot-comments.pdf>`
