Poetry
------

+--------+------------+
| League | Event Name |
+========+============+
| NCFCA  |            |
+--------+------------+
| NSDA   | Poetry     |
+--------+------------+
| Stoa   |            |
+--------+------------+

*Poetry* is a five-minute event in which competitors recite one or more poetic
works.  Though the speech is memorized, the performer uses a three-ringed
binder with the script in it as if they're reading from a book.

.. note::
    
   The NSDA's *Poetry* is a supplemental event at national competitions, meaning
   if you qualify in a main event, you can register in this one as well.

Example
=======

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/rHIUo00VihM" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Resources
=========

* :download:`An Introduction to Poetry (NSDA) </../files/nsda-poetry-description.pdf>`

Judging
=======

* :download:`Poetry Ballot (NSDA) </../files/nsda-prose-poetry-ballot.pdf>`
* :download:`Poetry Ballot with Comments (NSDA) </../files/nsda-prose-poetry-ballot-comments.pdf>`
