Humorous Interpretation
-----------------------

+--------+-------------------------+---------------------+
| League | Event Name              | Variations          |
+========+=========================+=====================+
| NCFCA  | Humorous                | Open                |
+--------+-------------------------+---------------------+
| NSDA   | Humorous Interpretation |                     |
+--------+-------------------------+---------------------+
| Stoa   | Humorous Interpretation | Open Interpretation |
+--------+-------------------------+---------------------+

A *Humorous Interpretation* is a ten-minute performance of a humorous piece of
published literature.  Think of it as a short play, without any costumes or
props, in which the performer plays all the characters.

.. note::

   The NCFCA's *Open* and Stoa's *Open Interpretation* allow you the freedom to
   perform a self-authored interpretation that's humorous, dramatic, or some
   combination of the two.

Example
=======

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/yHre9XircII?start=8" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Resources
=========

* :download:`Humorous Rules (NCFCA) </../files/ncfca-humorous-rules.pdf>`
* :download:`Open Rules (NCFCA) </../files/ncfca-open-rules.pdf>`
* :download:`General Speech Rules (NCFCA) </../files/ncfca-speech-rules.pdf>`
* :download:`An Introduction to Humorous Interpretation (NSDA) </../files/nsda-humorous-interpretation-description.pdf>`
* :download:`What to Expect Competing in Humorous Interpretation (NSDA) </../files/nsda-humorous-interpretation-what-to-expect.pdf>`
* :download:`Humorous Interpretation Rules (Stoa) </../files/stoa-humorous-interpretation-rules.pdf>`
* :download:`Open Interpretation Rules (Stoa) </../files/stoa-open-interpretation-rules.pdf>`

Judging
=======

* :download:`An Introduction to Evaluating Interpretation (NSDA) </../files/nsda-interpretation-judging.pdf>`
* :download:`Interpretation Ballot (NSDA) </../files/nsda-interpretation-ballot.pdf>`
* :download:`Interpretation Ballot with Comments (NSDA) </../files/nsda-interpretation-ballot-comments.pdf>`
* :download:`Humorous Interpretation Ballot (Stoa) </../files/stoa-humorous-interpretation-ballot.pdf>`

.. include:: possible-script-sources.rst

