Dramatic Interpretation
-----------------------

+--------+-------------------------+---------------------+
| League | Event Name              | Variations          |
+========+=========================+=====================+
| NCFCA  |                         | Open                |
+--------+-------------------------+---------------------+
| NSDA   | Dramatic Interpretation |                     |
+--------+-------------------------+---------------------+
| Stoa   | Dramatic Interpretation | Open Interpretation |
+--------+-------------------------+---------------------+

A *Dramatic Interpretation* is a ten-minute performance of a dramatic piece of
published literature.  Think of it as a short play, without any costumes or
props, in which the performer plays all the characters.

.. note::

   * The NCFCA's *Open* and Stoa's *Open Interpretation* allow you the freedom to
     perform a self-authored interpretation that's humorous, dramatic, or some
     combination of the two.
   * If you'd like to do a dramatic interpretation at a NCFCA tournament, you
     can use the *Open* event.

Example
=======

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/AysLoG0mSYc" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Resources
=========

* :download:`Open Rules (NCFCA) </../files/ncfca-open-rules.pdf>`
* :download:`General Speech Rules (NCFCA) </../files/ncfca-speech-rules.pdf>`
* :download:`An Introduction to Dramatic Interpretation (NSDA) </../files/nsda-dramatic-interpretation-description.pdf>`
* :download:`What to Expect Competing in Dramatic Interpretation (NSDA) </../files/nsda-dramatic-interpretation-what-to-expect.pdf>`
* :download:`Dramatic Interpretation Rules (Stoa) </../files/stoa-dramatic-interpretation-rules.pdf>`
* :download:`Open Interpretation Rules (Stoa) </../files/stoa-open-interpretation-rules.pdf>`

Judging
=======

* :download:`An Introduction to Evaluating Interpretation (NSDA) </../files/nsda-interpretation-judging.pdf>`
* :download:`Interpretation Ballot (NSDA) </../files/nsda-interpretation-ballot.pdf>`
* :download:`Interpretation Ballot with Comments (NSDA) </../files/nsda-interpretation-ballot-comments.pdf>`
* :download:`Dramatic Interpretation Ballot (Stoa) </../files/stoa-dramatic-interpretation-ballot.pdf>`

.. include:: possible-script-sources.rst

