Duo Interpretation
------------------

+--------+--------------------+
| League | Event Name         |
+========+====================+
| NCFCA  | Duo                |
+--------+--------------------+
| NSDA   | Duo Interpretation |
+--------+--------------------+
| Stoa   | Duo Interpretation |
+--------+--------------------+

A *Duo Interpretation* is a ten-minute performance of a piece of published
literature, which can be humorous, dramatic, or a combination of the two.
Think of it as a short play, without any costumes or props, in which two
performers play all the characters.

Example
=======

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/kzheCjwF5Ro" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Resources
=========

* :download:`Duo Rules (NCFCA) </../files/ncfca-duo-rules.pdf>`
* :download:`General Speech Rules (NCFCA) </../files/ncfca-speech-rules.pdf>`
* :download:`An Introduction to Duo Interpretation (NSDA) </../files/nsda-duo-interpretation-description.pdf>`
* :download:`What to Expect Competing in Duo Interpretation (NSDA) </../files/nsda-duo-interpretation-what-to-expect.pdf>`
* :download:`Duo Interpretation Rules (Stoa) </../files/stoa-duo-interpretation-rules.pdf>`

Judging
=======

* :download:`An Introduction to Evaluating Interpretation (NSDA) </../files/nsda-interpretation-judging.pdf>`
* :download:`Interpretation Ballot (NSDA) </../files/nsda-interpretation-ballot.pdf>`
* :download:`Interpretation Ballot with Comments (NSDA) </../files/nsda-interpretation-ballot-comments.pdf>`
* :download:`Duo Interpretation Ballot (Stoa) </../files/stoa-duo-interpretation-ballot.pdf>`

.. include:: possible-script-sources.rst

