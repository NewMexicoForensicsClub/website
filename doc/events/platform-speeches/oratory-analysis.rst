Oratory Analysis
----------------

+--------+------------------+
| League | Event Name       |
+========+==================+
| NCFCA  |                  |
+--------+------------------+
| NSDA   |                  |
+--------+------------------+
| Stoa   | Oratory Analysis |
+--------+------------------+

An *Oratory Analysis* is an eight-minute speech, written by the speaker on a
historical or contemporary speech of their choice, that is intended to analyze
its purpose, content, and cultural impact.

Resources
=========

* :download:`Oratory Analysis Rules (Stoa) </../files/stoa-oratory-analysis-rules.pdf>`
* :download:`Platform Speech Citations (Stoa) </../files/stoa-platform-speech-citations.pdf>`
* `Oratory Analysis Workshop at Stoa Academy 2019 <https://www.youtube.com/watch?v=jiWC5dz0VWw>`_

Judging
=======

* :download:`Oratory Analysis Ballot (Stoa) </../files/stoa-oratory-analysis-ballot.pdf>`
