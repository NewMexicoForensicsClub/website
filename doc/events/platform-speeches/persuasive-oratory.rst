Persuasive Oratory
------------------

+--------+------------------+----------------------+
| League | Event Name       | Variations           |
+========+==================+======================+
| NCFCA  | Persuasive       | Digital Presentation |
+--------+------------------+----------------------+
| NSDA   | Original Oratory |                      |
+--------+------------------+----------------------+
| Stoa   | Persuasive       |                      |
+--------+------------------+----------------------+

A *Persuasive Oratory* is a ten-minute speech, written by the speaker on a topic
of their choice, that is intended to persuade the audience to adopt a
particular position or engage in a course of action.

.. note::

   If you'd like to craft your speech around digitally-displayed content, along
   the lines of a `TED talk <https://www.ted.com/talks>`_, you can compete in
   the NCFCA's *Digital Presentation*.

Example
=======

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/E3fsBjoU5JY" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Resources
=========

* :download:`Persuasive Rules (NCFCA) </../files/ncfca-persuasive-rules.pdf>`
* :download:`Digital Presentation Rules (NCFCA) </../files/ncfca-digital-presentation-rules.pdf>`
* :download:`General Speech Rules (NCFCA) </../files/ncfca-speech-rules.pdf>`
* :download:`An Introduction to Original Oratory (NSDA) </../files/nsda-original-oratory-description.pdf>`
* :download:`What to Expect Competing in Original Oratory (NSDA) </../files/nsda-original-oratory-what-to-expect.pdf>`
* :download:`Persuasive Rules (Stoa) </../files/stoa-persuasive-rules.pdf>`
* :download:`Platform Speech Citations (Stoa) </../files/stoa-platform-speech-citations.pdf>`
* :download:`Possible Topics </../files/persuasive-oratory-topics.pdf>`

Judging
=======

* :download:`An Introduction to Evaluating Original Oratory (NSDA) </../files/nsda-original-oratory-judging.pdf>`
* :download:`Original Oratory Ballot (NSDA) </../files/nsda-original-oratory-ballot.pdf>`
* :download:`Original Oratory Ballot with Comments (NSDA) </../files/nsda-original-oratory-ballot-comments.pdf>`
* :download:`Persuasive Ballot (Stoa) </../files/stoa-persuasive-ballot.pdf>`
