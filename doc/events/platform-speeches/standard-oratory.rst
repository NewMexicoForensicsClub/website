Standard Oratory
----------------

+--------+-------------+
| League | Event Name  |
+========+=============+
| NCFCA  |             |
+--------+-------------+
| NSDA   | Declamation |
+--------+-------------+
| Stoa   |             |
+--------+-------------+

A *Standard Oratory* is a persuasive or informative oratory that somebody else
has written and performed in the past.  This could be from past competition
years, or also from outside the realm of forensics.

.. note::

   The NSDA's *Declamation* is a consolation event at national competitions,
   meaning if you get knocked out of your main event, you may re-register in
   this one and continue competing.

Example
=======

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/zZmdmNDoy8Y?start=92" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Resources
=========

* :download:`An Introduction to Declamation (NSDA) </../files/nsda-declamation-description.pdf>`

Judging
=======

* :download:`Declamation Ballot (NSDA) </../files/nsda-declamation-ballot.pdf>`
* :download:`Declamation Ballot with Comments (NSDA) </../files/nsda-declamation-ballot-comments.pdf>`

Possible Sources
================
* `American Rhetoric <https://www.americanrhetoric.com/>`_
* `Famous Speeches
  <http://www.famous-speeches-and-speech-topics.info/famous-speeches/index.htm>`_
* `TED Talks
  <https://www.ted.com/playlists/171/the_most_popular_talks_of_all>`_
* `Ten Famous Speeches in American History
  <https://www.learnoutloud.com/content/blog/archives/2009/07/ten_famous_spee.php>`_
* `The 35 Greatest Speeches in History
  <https://www.artofmanliness.com/character/knowledge-of-men/the-35-greatest-speeches-in-history/>`_

