Informative Oratory
-------------------

+--------+----------------------+----------------------+
| League | Event Name           | Variations           |
+========+======================+======================+
| NCFCA  | Informative          | Digital Presentation |
+--------+----------------------+----------------------+
| NSDA   | Informative Speaking | Expository Speaking  |
+--------+----------------------+----------------------+
| Stoa   | Original Oratory     | Expository           |
+--------+----------------------+----------------------+

An *Informative Oratory* is a ten-minute speech, written by the speaker on a
topic of their choice, that is intended to educate, inform, and expose
something in a novel way.

.. note::

   * The NSDA's *Informative Speaking* and Stoa's *Expository* involve the use
     of visual aids.
   * If you'd like to craft your speech around digitally-displayed content,
     along the lines of a `TED talk <https://www.ted.com/talks>`_, you can
     compete in the NCFCA's *Digital Presentation* or Stoa's *Expository*.
   * The NSDA's *Expository Speaking* is a five-minute supplemental event at
     national competitions, meaning if you qualify in one of the main events,
     you may register in this as well.

Example
=======

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/0EQiG43nuaQ" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Resources
=========

* :download:`Informative Rules (NCFCA) </../files/ncfca-informative-rules.pdf>`
* :download:`Digital Presentation Rules (NCFCA) </../files/ncfca-digital-presentation-rules.pdf>`
* :download:`General Speech Rules (NCFCA) </../files/ncfca-speech-rules.pdf>`
* :download:`An Introduction to Informative Speaking (NSDA) </../files/nsda-informative-speaking-description.pdf>`
* :download:`What to Expect Competing in Informative Speaking (NSDA) </../files/nsda-informative-speaking-what-to-expect.pdf>`
* :download:`An Introduction to Expository Speaking (NSDA) </../files/nsda-expository-speaking-description.pdf>`
* :download:`Original Oratory Rules (Stoa) </../files/stoa-original-oratory-rules.pdf>`
* :download:`Expository Rules (Stoa) </../files/stoa-expository-rules.pdf>`
* :download:`Platform Speech Citations (Stoa) </../files/stoa-platform-speech-citations.pdf>`
* :download:`Possible Topics </../files/informative-oratory-topics.pdf>`

Judging
=======

* :download:`An Introduction to Evaluating Informative Speaking (NSDA) </../files/nsda-informative-speaking-judging.pdf>`
* :download:`Informative Speaking Ballot (NSDA) </../files/nsda-informative-speaking-ballot.pdf>`
* :download:`Informative Speaking Ballot with Comments (NSDA) </../files/nsda-informative-speaking-ballot-comments.pdf>`
* :download:`Expository Ballot (Stoa) </../files/stoa-expository-ballot.pdf>`
* :download:`Original Oratory Ballot (Stoa) </../files/stoa-original-oratory-ballot.pdf>`
