Platform Speeches
-----------------

Platform speeches are probably what come to mind when you think of someone
giving a speech to an audience.  Whether it's a valedictorian giving a
commencement speech to a graduating class, a
`TED talk <https://www.ted.com/talks>`_ speaker engaging with an audience in
person and via `YouTube <https://www.youtube.com>`_, or a pastor connecting
with the congregation, platform speeches are prepared well in advance, and are
intended to inform, persuade, and perhaps entertain.

Platform speeches help students develop the ability to both craft and deliver a
speech, including:

* developing a line of thinking,
* ensuring points flow well from one to another,
* supporting points with engaging reasoning and examples,
* memorizing the content,
* engaging with the audience,
* speaking confidently and passionately, and
* inspiring and motivating people to action.

There are four general types of platform speeches:

.. toctree::
   :titlesonly:

   platform-speeches/persuasive-oratory
   platform-speeches/informative-oratory
   platform-speeches/standard-oratory
   platform-speeches/oratory-analysis
