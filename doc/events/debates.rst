Debates
-------

Debates are where two or more individuals clash when arguing either in favor of
or against a particular resolution.  If you think of attorneys in a courtroom,
or legislators in Congress, that's the general idea.  The goal is to use
evidence, philosophy, and logical reasoning to convince your audience that your
position is true, and to do so in a way that is winsome and gracious.  In all
debate events, students must be prepared to support either side of the issue at
hand in any given round.

Debates help students develop the ability to:

* catalogue and organize copious amounts of research,
* develop arguments in support of a thesis,
* see complex issues from multiple perspectives,
* construct a line of questioning to discern another's viewpoint,
* understand that there's more to an issue than what appears on the surface,
* answer questions succinctly on the fly,
* identify logical fallacies that are detrimental to dialogue, and
* search for common ground to better isolate the key differences of opinion.

The various types of debate include:

.. toctree::
   :titlesonly:

   debates/policy-debate
   debates/value-debate
   debates/public-forum-debate
   debates/congressional-debate
   debates/parliamentary-debate
   debates/moot-court
   debates/big-questions-debate
   debates/world-schools-debate
   debates/extemporaneous-debate
