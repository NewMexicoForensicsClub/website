Congressional Debate
--------------------

+--------+----------------------+
| League | Event Name           |
+========+======================+
| NCFCA  |                      |
+--------+----------------------+
| NSDA   | Congressional Debate |
+--------+----------------------+
| Stoa   |                      |
+--------+----------------------+

*Congressional Debate* is an event in which competitors simulate being
legislators in a chamber of Congress.  Teams submit legislation ahead of a
competition, and competitors will give speeches for and against the bills or
resolutions, some prepared ahead of time, and others more extemporaneous,
reacting to the clash in the chamber.

Example
=======

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/HQ07Dmc1L0g" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Resources
=========

* :download:`An Introduction to Congressional Debate (NSDA) </../files/nsda-congressional-debate-description.pdf>`
* :download:`What to Expect Competing in Congressional Debate (NSDA) </../files/nsda-congressional-debate-what-to-expect.pdf>`

Judging
=======

* :download:`Congressional Debate Ballot (NSDA) </../files/nsda-congressional-debate-ballot.pdf>`
