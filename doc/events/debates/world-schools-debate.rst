World Schools Debate
--------------------

+--------+----------------------+
| League | Event Name           |
+========+======================+
| NCFCA  |                      |
+--------+----------------------+
| NSDA   | World Schools Debate |
+--------+----------------------+
| Stoa   |                      |
+--------+----------------------+

*World Schools Debate* is a three-on-three debate format in which the
proposition team will be proposing a prepared or impromptu motion, and the
opposition team will be advocating for the rejection of the motion.

.. note::

   Each team may have up to five members, but only three members compete in any
   given round.

Example
=======

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/PvHfJkNoWVY" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Resources
=========

* :download:`An Introduction to World Schools Debate (NSDA) </../files/nsda-world-schools-debate-description.pdf>`
* `World Schools Debate Challenge <https://wsdcdebate.org/>`_

Judging
=======

* :download:`World Schools Debate Ballot (NSDA) </../files/nsda-world-schools-debate-ballot.pdf>`
* :download:`World Schools Debate Ballot with Comments (NSDA) </../files/nsda-world-schools-debate-ballot-comments.pdf>`
