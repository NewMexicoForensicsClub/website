Extemporaneous Debate
---------------------

+--------+-----------------------+
| League | Event Name            |
+========+=======================+
| NCFCA  |                       |
+--------+-----------------------+
| NSDA   | Extemporaneous Debate |
+--------+-----------------------+
| Stoa   |                       |
+--------+-----------------------+

*Extemporaneous Debate* is a one-on-one debate format in which competitors are
presented with the resolution 30 minutes prior to the round.

.. note::
    
   The NSDA's *Extemporaneous Debate* is a supplemental event at national
   competitions, meaning if you qualify in a main event, you may register in
   this one as well.

Example
=======

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/I63EiHaYEJc" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Resources
=========

* :download:`An Introduction to Extemporaneous Debate (NSDA) </../files/nsda-extemporaneous-debate-description.pdf>`
