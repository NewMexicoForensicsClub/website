Big Questions Debate
--------------------

+--------+----------------------+
| League | Event Name           |
+========+======================+
| NCFCA  |                      |
+--------+----------------------+
| NSDA   | Big Questions Debate |
+--------+----------------------+
| Stoa   |                      |
+--------+----------------------+

*Big Questions Debate* is an event that encourages competitors to engage in
discussions on the bigger questions of life, causing them to question, and
requiring them to fully support, their previously held beliefs.  Teams can have
either one or two competitors on either side.

Example
=======

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/e9w7bhapHIE" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Resources
=========

* :download:`Big Questions Debate Format Manual (NSDA) </../files/nsda-big-questions-debate-format-manual.pdf>`

Judging
=======

* :download:`Big Questions Debate Judge Primer (NSDA) </../files/nsda-big-questions-debate-judging.pdf>`
* :download:`Big Questions Debate Ballot (NSDA) </../files/nsda-big-questions-debate-ballot.pdf>`
