Moot Court
----------

+--------+----------------------+
| League | Event Name           |
+========+======================+
| NCFCA  | Moot Court           |
+--------+----------------------+
| NSDA   |                      |
+--------+----------------------+
| Stoa   |                      |
+--------+----------------------+

*Moot Court* is a simulated oral argument, similar to an argument made before
an appellate court.  Students present legal arguments from both sides of a
fictional case as they focus on convincing the judge panel to either uphold or
overturn a lower court’s ruling.

Example
=======

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/SVeIEUAW_8Y?start=135" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Problem
=======

You can access the Moot Court problem via the NCFCA's
`Resource Library <https://portal.ncfca.org/secure/resource-library>`_.  Note
that you will need to have
`affiliated <https://www.ncfca.org/join/affiliation-benefits/league-affiliation/family-affiliation/>`_
in order to gain access to it.
