Parliamentary Debate
--------------------

+--------+----------------------+
| League | Event Name           |
+========+======================+
| NCFCA  |                      |
+--------+----------------------+
| NSDA   |                      |
+--------+----------------------+
| Stoa   | Parliamentary Debate |
+--------+----------------------+

*Parliamentary Debate* is a two-on-two debate in which the team affirming the
resolution represents the government, and the team negating the resolution
represents the opposition.  Resolutions are released 20 minutes prior to a
round of competition.

Example
=======

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/gTJv_8YYa5Q" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Resources
=========

* :download:`Parliamentary Debate Rules (Stoa) </../files/stoa-parliamentary-debate-rules.pdf>`
* :download:`Parliamentary Debate Flow Sheet (Stoa) </../files/stoa-parliamentary-debate-flow-sheet.pdf>`

Judging
=======

* :download:`Parliamentary Debate Ballot (Stoa) </../files/stoa-parliamentary-debate-ballot.pdf>`
