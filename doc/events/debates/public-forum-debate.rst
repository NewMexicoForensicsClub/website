Public Forum Debate
-------------------

+--------+---------------------+
| League | Event Name          |
+========+=====================+
| NCFCA  |                     |
+--------+---------------------+
| NSDA   | Public Forum Debate |
+--------+---------------------+
| Stoa   |                     |
+--------+---------------------+

*Public Forum Debate* is a two-on-two debate that's sort of a hybrid between
*Policy Debate* and *Value Debate* where teams will argue resolutions dealing
with current events.

Example
=======

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/MUnyLbeu7qU?start=40" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Resources
=========

* :download:`An Introduction to Public Forum Debate (NSDA) </../files/nsda-public-forum-debate-description.pdf>`
* :download:`What to Expect Competing in Public Forum Debate (NSDA) </../files/nsda-public-forum-debate-what-to-expect.pdf>`

Judging
=======

* :download:`An Introduction to Evaluating Public Forum Debate (NSDA) </../files/nsda-public-forum-debate-judging.pdf>`
* :download:`Public Forum Debate Ballot (NSDA) </../files/nsda-public-forum-debate-ballot.pdf>`
* :download:`Public Forum Debate Ballot with Comments (NSDA) </../files/nsda-public-forum-debate-ballot-comments.pdf>`
