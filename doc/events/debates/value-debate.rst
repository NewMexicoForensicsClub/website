Value Debate
------------

+--------+------------------------+
| League | Event Name             |
+========+========================+
| NCFCA  | Value Debate           |
+--------+------------------------+
| NSDA   | Lincoln-Douglas Debate |
+--------+------------------------+
| Stoa   | Lincoln-Douglas Debate |
+--------+------------------------+

*Value Debate* is a one-on-one, primarily philosophy-based, debate in which
both teams will put forth a value to uphold and decision-making criterion to
evaluate the round in support of or opposition to the resolution.

Example
=======

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/--ltivnnuGA?start=1805" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Resources
=========

* :download:`General Debate Rules (NCFCA) </../files/ncfca-debate-rules.pdf>`
* :download:`An Introduction to Lincoln-Douglas Debate (NSDA) </../files/nsda-lincoln-douglas-debate-description.pdf>`
* :download:`What to Expect Competing in Lincoln-Douglas Debate (NSDA) </../files/nsda-lincoln-douglas-debate-what-to-expect.pdf>`
* :download:`Lincoln-Douglas Debate Rules (Stoa) </../files/stoa-lincoln-douglas-debate-rules.pdf>`
* :download:`Lincoln-Douglas Debate Flow Sheet (Stoa) </../files/stoa-lincoln-douglas-debate-flow-sheet.pdf>`

Judging
=======

* :download:`An Introduction to Evaluating Lincoln-Douglas Debate (NSDA) </../files/nsda-lincoln-douglas-debate-judging.pdf>`
* :download:`Lincoln-Douglas Debate Ballot (NSDA) </../files/nsda-lincoln-douglas-debate-ballot.pdf>`
* :download:`Lincoln-Douglas Debate Ballot with Comments (NSDA) </../files/nsda-lincoln-douglas-debate-ballot-comments.pdf>`
* :download:`Lincoln-Douglas Debate Ballot (Stoa) </../files/stoa-lincoln-douglas-debate-ballot.pdf>`
