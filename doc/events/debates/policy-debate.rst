Policy Debate
-------------

+--------+--------------------+
| League | Event Name         |
+========+====================+
| NCFCA  | Team Policy Debate |
+--------+--------------------+
| NSDA   | Policy Debate      |
+--------+--------------------+
| Stoa   | Team Policy Debate |
+--------+--------------------+

*Policy Debate* is a two-on-two, primarily evidence-based, debate in which the
affirmative team will support the resolution by putting forward a plan to
change policy and the negative team will argue against that plan and supporting
evidence and analysis.

Example
=======

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/h83q3IGbWN0?start=725" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Resources
=========

* :download:`General Debate Rules (NCFCA) </../files/ncfca-debate-rules.pdf>`
* :download:`An Introduction to Policy Debate (NSDA) </../files/nsda-policy-debate-description.pdf>`
* :download:`What to Expect Competing in Policy Debate (NSDA) </../files/nsda-policy-debate-what-to-expect.pdf>`
* :download:`Team Policy Debate Rules (Stoa) </../files/stoa-team-policy-debate-rules.pdf>`
* :download:`Team Policy Debate Flow Sheet (Stoa) </../files/stoa-team-policy-debate-flow-sheet.pdf>`

Judging
=======

* :download:`An Introduction to Evaluating Policy Debate (NSDA) </../files/nsda-policy-debate-judging.pdf>`
* :download:`Policy Debate Ballot (NSDA) </../files/nsda-policy-debate-ballot.pdf>`
* :download:`Policy Debate Ballot with Comments (NSDA) </../files/nsda-policy-debate-ballot-comments.pdf>`
* :download:`Team Policy Debate Ballot (Stoa) </../files/stoa-team-policy-debate-ballot.pdf>`
