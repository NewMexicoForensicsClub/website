Asking Questions
----------------

:raw-html:`<div align="right">🕑 30 min.</div>`

.. contents::  Contents
   :local:

Motivation
==========

Using questions effectively---in dialogue, debate, writing, etc.---is an art
form.  Most people today never develop their skills past the macaroni noodles
and Elmer's glue stage.  However, being able to communicate your views in an
eloquent, effective, winsome, and gracious manner, which is part of :ref:`our
mission <our_mission>`, requires advancing beyond crayons and construction
paper and working toward the *Mona Lisa*.

Why Does It Matter?
^^^^^^^^^^^^^^^^^^^

But why does effective questioning matter, anyway?  Because there's far too
much confusion in the world, and questions can help you clear things up.
Oftentimes people wind up using the same vocabulary, but different
dictionaries.  This means they're using the same words, so they think they're
talking about the same things, but since they have different definitions in
mind, they wind up talking past each other.  A good example from culture is the
word "evolution", where one person might be talking about seasonal variation
within a species, and another might mean that humans developed over millions of
years from some single-celled organism.  Another example from history is the
German philosopher `Hegel`_, who used such imprecise language in his writing
that often people had no clue what he was talking about.  Shortly after his
death there were two camps of adherents that sprung up:  the `Old Hegelians`_,
who were devoted to maintaining protestant orthodoxy, and the `Young
Hegelians`_, who were supporting revolutionary atheism.  Hard to get further
apart than that, yet both groups pointed back to the same works and the same
words to support their radically different interpretations of what he'd meant.
Imprecise language is dangerous.

.. _Hegel:  https://en.wikipedia.org/wiki/Georg_Wilhelm_Friedrich_Hegel
.. _Old Hegelians:  https://en.wikipedia.org/wiki/Right_Hegelians
.. _Young Hegelians:  https://en.wikipedia.org/wiki/Young_Hegelians

Where Does It Matter?
^^^^^^^^^^^^^^^^^^^^^

Okay, so it might be worthwhile to develop some interrogation skills, but where
am I going to use them?  A first and obvious answer is in academic debate.  It
doesn't matter which :doc:`style of debate </events/debates>` you may be doing,
they all consist of a series of speeches on the affirmative and negative sides,
and between some of those speeches (usually just in the first half of the
debate) there are periods of *cross-examination*.  These are opportunities for
you to question your opponent to clarify the debate in your mind before you
proceed with the next round of argumentation.  Now you could just ask whatever
questions happen to pop into your mind in the moment, or you could train
yourself to really make the most of those few precious minutes.

That said, most of you won't be participating in academic debate most of the
time, but you know what you will be doing?  Interacting with other people, and
that's the second venue in which these skills will benefit you:  in dialogue.
And it doesn't even need to be in the context of an argument.  Chances are
confusion will arise in conversation, and if you don't do anything about it,
you and the other person don't actually have a meeting of the minds.  The
consequences could be minor (Whoops, I showed up at the wrong place.) or major
(You're not coming to my birthday party ever again!), depending on the
circumstances.  However, if you know what to look for, and know how to address
the problems as they arise, you have the potential to have a much more fruitful
conversation.

And finally, this skill set will really help you to level up your game as a
public speaker.  As you're writing your speeches, or speaking extemporaneously,
you can anticipate the questions your audience will have for you.  If you don't
address them, that leaves your audience thinking, "Well, maybe this guy can
talk a good talk, but he didn't think about x, y, and z."  If that happens, it
doesn't matter how persuasive the rest of your speech was, because your
audience thinks they know more than you, and you haven't addressed all their
concerns.  However, if you think through the questions ahead of time, and
address them as they come up, your audience winds up thinking, "Holy cow, I was
just going to ask that question!  And that was a really good answer!"  You've
alleviated their concerns and they walk away much better persuaded.

Who Are Questions For?
^^^^^^^^^^^^^^^^^^^^^^

Before we get to the questions themselves and how you use them, we need to talk
briefly about the two different targets for your questions:  who are they for?
Oftentimes they'll simply be directed at your interlocutor (that just a fancy
word for the person you're speaking to).  You're having a conversation with
someone and they're not making sense, so you're directing questions at them to
try to improve the quality of the conversation.  Makes sense.

Other times, though, you'll be directing questions *to* your interlocutor, but
they're not actually *for* them.  Rather, they're *for* anyone else who happens
to be within earshot---whatever audience you have listening to your
conversation.  What do I mean by this?  Oftentimes you'll be able to discern
that the person you're speaking to isn't open to the possibility that their
ideas are wrong, isn't interested in examining the evidence, isn't about to
realize their viewpoint rests on logical fallacies, etc.  In such cases the
likelihood that you'll wind up convincing them of something other than what
they already believe is slim to nil.  However, the same can't be said for all
the people listening in, so you're asking questions to encourage *them* to
ponder the situation further.  You want them to leave motivated to do some
research and further thinking to see who was right and who was wrong.  This
scenario can be hard to get used to, but the purpose isn't to win an argument,
it's to help people find truth.

How Do You Use Questions?
^^^^^^^^^^^^^^^^^^^^^^^^^

At this point we understand that interrogation is a useful skill, and we're
aware of the different contexts we'll use it in and the different audiences we
may target with them.  The next step before we really dive in is to realize
that there are three different purposes for asking questions:

1. **Gathering information:**  I need more details in order to better
   understand my opponent's side, so I need to ask for them.
2. **Making a point:**  I want to point something out, but rather than state it
   directly, I'll phrase it as a question.
3. **Redirecting the conversation:**  The conversation has taken a turn for the
   worse, but I can nudge it in a different direction with a question.

Gathering information
=====================

The vast majority of the time, you'll probably wind up using questions to
gather more information.  But what kind of information?  There are generally
four different things that you might need to clarify in conversation:

1. terms,
2. analysis,
3. sources, and
4. motives.

.. _clarifying_terms:

Clarifying Terms
^^^^^^^^^^^^^^^^

First and foremost, you must understand what is meant by the terms your
interlocutor is using.  As mentioned before, if you're using the same
vocabulary, but different dictionaries, you just wind up talking past each
other (and thoroughly confusing your audience).  To get a peek inside their
dictionary, though, you'll need to develop the habit of asking, "What do you
mean by that?"

.. note::

   Brownie points if you caught when I used this up above.

You don't want to be a broken record, though.  Normally, when I first teach
kids about this question, they decide to have too much fun responding to
absolutely everything with, "What do you mean by that?"  But if you're
annoying, people won't want to talk with you, so don't overdo it.  Only ask
when you genuinely need to understand some word or phrase they've used, and
change it up each time.  For example, "Can you rephrase that for me?" is
another good way to ask.  Or a phrase I use often enough with my wife is, "Hang
on, say the same thing again, but with different words."  Or if you're a
*Different Strokes* fan, there's always, |what_choo_talkin_bout_willis|_

.. _what_choo_talkin_bout_willis:  https://www.youtube.com/watch?v=Qw9oX-kZ_9k
.. |what_choo_talkin_bout_willis| replace::  "What'choo talkin' 'bout, Willis?"

.. _clarifying_analysis:

Clarifying Analysis
^^^^^^^^^^^^^^^^^^^

Once you're clear on the meanings of the words you're using to communicate, the
next thing to clarify will be the other person's analysis, so the next question
to learn is, "How did you come to that conclusion?"  You understand what they
mean, but not why it makes sense to them, so you'd like them to walk you
through their thinking to see how they decided to believe what they do.  It may
be the case, as they explain their thoughts, that they have a number of good
points that just hadn't occurred to you yet.  On the other hand, there may be
one or more problems with their analysis that haven't occurred to them yet, and
discovering them could help in changing their opinion.

Again, there are multiple ways to try to get at the same information.  Consider
asking, "Why do you think that?" or "What led you to that belief?"  Another
option, if you have time for more of a backstory, would be, "Did you ever think
differently in the past?  What changed your mind?"

.. warning::

   Before we move on, it's worth pausing for a moment to note that you have to
   enter these conversations knowing that it's possible both that you're
   mistaken in one or more areas of your understanding, and that your opponent
   is right about a number of things as well.  If you walk into a discourse
   assuming you're right about everything and your opponent is a blithering
   idiot, that's a recipe for disaster.  Remember, the point in all of this
   isn't winning, isn't one-upping your opponent, isn't showing yourself to be
   the superior rhetoritician; rather, the goal is finding truth.  Check your
   pride at the door, and don't allow yourself to get distracted.

.. _clarifying_sources:

Clarifying Sources
^^^^^^^^^^^^^^^^^^

Now that you understand the trail of thought that brought the other person to
their way of thinking, the next thing you'll need to clarify is their sources.
All the information that led them to their conclusions didn't just materialize
out of thin air, so where'd it come from?  For that, the third fundamental
question you'll want to train yourself to ask regularly is, "Where did you get
your information?"

When you request sources, there are generally three possible responses:

1. **They're able to provide them on the spot:**  This will usually only be
   true in academic debate, or if someone's done a ton of research and keeps it
   handy at all times, which is pretty rare.
2. **They're not able to provide them because they don't actually know where
   the information comes from:**  This isn't something to berate them about;
   rather, use questions to help them realize the problem with not having any
   evidence to back up their thinking.
3. **They do have sources for their information, but they don't have them on
   hand at the moment:**  In this case, don't be a jerk and chastise them for
   not carrying all their evidence around at all times; rather, give them time
   to gather the information and continue the conversation later.

Clarifying Motives
^^^^^^^^^^^^^^^^^^

So far the questions we've asked to clarify any holes in our communication have
proceeded in a particular order:  first clarify the terminology being used,
then make sure you understand your opponent's analysis, then examine their
sources of information.  This final question we'll consider isn't one that you
use last in the progression; rather, you should insert it into the conversation
whenever you suspect the other person might have ulterior motives in place.  In
order to clarify your opponent's motives, ask something like "What are you
getting at?" or "What is it you’re trying to say, exactly?"

The ideal scenario is when you both approach the conversation with the purpose
of trying to understand each other well and arrive at truth.  However, either
of you may have particular attachments to your beliefs that may skew your
motivation in one direction or another.  If it feels like they're fishing for
something, try asking, "Are you looking for a particular answer here?"  Or if
it feels like they're pushing you in a particular direction, you might try,
"Are you hoping for a particular outcome?"  If all else fails, you can resort
to simply asking, "Why are you doing [fill in the blank]?  What's your
motivation?"

If it becomes apparent that the other person is not truly seeking a meeting of
the minds in search of truth, their arguments become less trustworthy because
of the inherent bias.  The point in asking these questions is either

1. to clear things up and get things back on the right track, or
2. to indicate to the wider audience that everything your opponent says should
   be taken with a grain of salt.

Making a Point
==============

At this point we've clarified everything that might need clarifying, and it's
time to move on to the second purpose of asking questions in conversation:
making a point.  But why would we use questions to make a point when we could
just state our point directly?  Good question; it feels like it's inefficient,
right?  Here's the thing:  if you make a statement that you know your opponent
doesn't agree with---pick whatever charged topic you like:  the president is
trying to destroy the country; electric vehicles do more harm than good; the
virus isn't as dangerous as is being claimed; red chile is better than green;
whatever---your opponent will likely have a gut response that leads them to
reject whatever you say outright, without thinking about it, because you're the
bad guy.  However, if you use questions to lead them in the right direction,
and they come to your conclusion via their own logical thought processes in
their own head, then they're the one making the statement, and that's much
harder to disagree with, because they're the good guy.

You can use questions to make pretty much whatever point you'd like, but you'll
often find this skill helpful in identifying:

1. logical errors,
2. poor sources,
3. hypocrisy, and
4. fact vs opinion.

Identifying Logical Errors
^^^^^^^^^^^^^^^^^^^^^^^^^^

In the midst of your conversation, you may detect one or more logical
inconsistencies in the other person's thinking.  When that happens, the last
thing you want to do is say something like, "That's illogical," because you'll
trigger that fight or flight gut response that will cause them to dig in their
heels more rather than try to understand what you're saying.  Instead, you can
ask questions to help them see the disconnect.  Consider the following
scenarios:

Self-contradictory views
  Say you notice your opponent seems to hold some viewpoint that is logically
  inconsistent with itself.  The classic example is when someone claims there's
  no such thing as absolute truth.  Rather than responding with something that
  amounts to, "You dummy," try, "Hang on, if what you said is true, then how
  can it be that [insert conclusion that is at odds with the premise]?"  (E.g.,
  "Is that statement true absolutely?")
Mutually exclusive views
  This one is particularly prevalent with `counterfeit worldviews invading the
  church`_.  You have someone holding to two beliefs simultaneously, though
  they're mutually exclusive.  E.g., do you know truth via :doc:`revelation,
  reason, and reality <worldview-analysis/revelation-reason-and-reality>`, or
  through your emotions?  Is scripture to be interpreted through a
  :doc:`historical, grammatical hermeneutic <biblical-hermeneutics>`, or via
  the lived experience of a plurality of voices?  When you sense incompatible
  views being held simultaneously, use questions to draw out how the two cannot
  possibly both be true together.
Red herrings
  This is a fallacy of distraction, where instead of attacking your argument,
  your opponent will throw out something vaguely related for the sake of
  side-tracking the conversation.  E.g., we started off discussing climate
  change, but then we somehow took a left turn into child labor.  Your opponent
  may not even realize what they've done.  Use something like, "Hang on, I
  thought we were talking about [the first topic], but now it seems like we've
  changed to [the second topic].  Did you mean to switch gears?  Can we finish
  the first conversation before we dig into the second one?"  If yes, you've
  improved the quality of the discourse.  If no, you've showcased to your
  audience that they're just throwing stuff out there and seeing what sticks,
  as opposed to actually working through an issue with you.
Absurdity
  In this last case, your opponent likely hasn't followed their train of
  thought to its logical conclusion, so you want to help them do that for the
  sake of showcasing how absurd their viewpoint is.  For instance, many
  organizations these days are going out of their way to be "affirming" of
  however people choose to "identify" at the moment.  So take that to a
  logical, ridiculous conclusion:  Does that mean we'll start allowing
  50-year-old men compete on a swim team for 12- and 13-year-old girls?  `Yes,
  apparently it does`_.

.. _counterfeit worldviews invading the church:  https://jmgate.readthedocs.io/en/latest/thoughts/counterfeit-worldviews.html
.. _Yes, apparently it does:  https://www.rebelnews.com/swim_competition_allows_a_50_year_old_biological_male_to_swim_with_13_year_old_girls

.. note::

   A comprehensive review of :doc:`logical fallacies <logical-fallacies>` is
   beyond the scope of our current discussion, but there are :ref:`a number of
   books we recommend <logic_books>` for further study.

Keep in mind that "you can lead a horse to water, but you can't make him
drink."  You may not get your opponent to realize the flaws in their reasoning
in the moment---that's hard to do, and even harder to admit once you have
realized it.  Hopefully, you'll at least have planted a seed of nagging doubt
that may bear fruit later on down the line.

Identifying Poor Sources
^^^^^^^^^^^^^^^^^^^^^^^^

Up above we talked through how to use questions to determine what sources of
information contributed to the other person's way of thinking.  Once you do
have your opponent's sources on hand, it's time to :ref:`evaluate their
credibility <craap_test>`.  After all, just because I have a source doesn't
mean it's a good one.  It may be outdated, or biased, or sketchy, etc.  If you
find the quality of the source lacking in any way, again, don't be a jerk, but
gently use questions to help the other person realize the weaknesses of their
source of information.  Consider the following areas of examination:

**Currency**

  * This article looks like it was written five years ago.  Are you sure the
    author's analysis is still accurate, given what we know today?
  * It looks like some of these results were later retracted due to a problem
    with the underlying simulation.  Can we be sure there aren't any more
    problems in their methodology that haven't been noticed yet?

**Relevance**

  * This information looks like it was intended for a general audience, but
    we're hoping to dig into the details.  Are you sure this is sufficient for
    us to build a well-informed understanding?
  * It seems like this piece is only tangentially related to what we're
    discussing.  Can we find some evidence that speaks more directly to the
    question at hand?

**Authority**

  * The doctor you reference is just a general practitioner.  Are you sure
    they're qualified to speak in the realm of global infectious disease
    transmissibility?  (That is, is this a :ref:`fallacious appeal to authority
    <fallacious_appeal_to_authority>`?)
  * It looks like this particular scientist has a significant history of
    issuing remarkably dire predictions, and reality has never come close to
    them.  Are you sure they're trustworthy in this instance?

**Accuracy**

  * I couldn't find any citations or links to more information.  Do you know if
    this source agrees with other reporting on this subject?
  * The quality of this piece looks a little iffy.  Do you happen to know if it
    went through any kind of peer review process before it was published?

**Purpose**

  * Does this article seem like an unbiased presentation of the facts to you,
    or does it feel like it's slanted in a particular direction?
  * The author makes some good arguments, but it seems like they all hang on
    this fundamental assumption that [insert assumption here].  Is that
    assumption valid?

Identifying Fact vs Opinion
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Even if the source is credible, the next thing you'll want to discern is
whether they're presenting you with facts or just their opinion.  It's worth
following up with something like, "Can you show me the data?"  I understand
your expert source believes what you're telling me, but why?  What data were
they examining, and what analysis did they perform to lead them to this
conclusion?  If we're looking at raw data, then you have the ability to draw
your own conclusions and see whether they align with your opponent's.

However, if your probes for more information end in nothing but dead ends, it's
likely the case that you have an instance of `the worldview of
authoritarianism`_ playing itself out.  If you're not afforded the opportunity
to examine the facts themselves, then you're effectively not being trusted to
think for yourself.  This may be worth pointing out with something like, "Why
do you suppose we have to take this fellow's opinion on faith rather than
evaluate the evidence for ourselves?"

.. _the worldview of authoritarianism:  https://jmgate.readthedocs.io/en/latest/thoughts/counterfeit-worldviews.html#authoritarianism

.. note::

   In certain circumstances, it may be the case that you lack the subject
   matter expertise to evaluate the data personally, in which case you'll have
   to rely on some expert in one way or another.  However, whether a source
   transparently shared all their data with you is a pretty strong indicator of
   their trustworthiness.  In practice, it's much more common to see the
   continual dissemination of opinions without any facts in sight.

Identifying Hypocrisy
^^^^^^^^^^^^^^^^^^^^^

A final instance where questions can help you make a point is when you run into
hypocrisy.  If, in the course of conversation, you note that someone's actions
don't align with their thoughts, it's worth calling attention to that.  After
all, if they really believe what they're saying, you'd expect the way they live
their lives to align with that.  If that's not the case, then perhaps they're
not as convinced as they think they are, and if that's true, then why should
you be?

For instance, say you run into someone who's arguing that we need to save the
planet by doing away with plastics.

* **You:**  So you want me to get rid of all the plastics in my life?
* **Them:**  Yes, absolutely.
* **You:**  Are you willing to do the same?
* **Them:**  I've already done so.  I bring reusable bags to the grocery store,
  and buy all my food in bulk to avoid the packaging.
* **You:**  Okay, but what about your glasses?
* **Them:**  What?  I hadn't thought about that.  But I need them to see.
* **You:**  Okay, so how about your clothing?  That polyester's a plastic
  after all.
* **Them:**  Really?  Huh, I didn't realize that.
* **You:**  Have you gotten rid of your cell phone already?  Your laptop?
* **Them:**  Ummm...
* **You:**  The coffee maker?
* **Them:**  NOT MY COFFEE MAKER!!!

A little exaggerated at the end there, just for fun, but you get the idea:  You
can gently ask a series of questions to help them realize what hasn't occurred
to them yet.

A Word of Caution
^^^^^^^^^^^^^^^^^

In all of the above, keep in mind that you must never come across as having
said or implied, "My opponent is nothing but an illogical hypocrite with a
bunch of crazy opinions and no evidence to back them up!  Clearly I'm right,
because they're so obviously incompetent!"  Your goal in all of this is to seek
and find truth for yourself, and to help others do the same.  If it wasn't for
God's grace, you would be just as blind as they are.  There's no room for pride
here, and there are devastating consequences if you fall victim to it.  Do what
you can to help them find truth, and trust the Holy Spirit to handle the rest,
in his timing, not yours.

Redirecting the Conversation
============================

At this point we've become masters of clarification and have learned to make a
point without saying it.  The last tool to add to our belt is the ability to
redirect a conversation with a question.  At some point in your communication,
you may sense that the discussion is taking a turn for the worse.  When you
realize this, you have two options:

1. You can attempt to bring the conversation back in a healthy direction.  If
   you succeed, then you and your opponent can continue working through the
   issues until you reach a point of understanding---not necessarily agreement,
   but at least understanding one another well.
2. If it appears that won't be possible, you can simply bring the conversation
   to a close.  There's no sense wasting any more breath in unproductive
   argument, and your time will be better spent elsewhere.

There are a variety of scenarios in which this attempted redirection will be
beneficial, but the most common tend to be when you need to:

1. parry an *ad hominem* attack,
2. address rudeness,
3. narrate the debate, or
4. request time to process.

Parrying *ad hominem* Attacks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

When your opponent decides to attack you rather than your argument, it can be
quite the jarring experience and throw you off your feet.  Don't take the bait
and start defending yourself---to do so would be to follow your opponent away
from the actual issue being argued.  When you get hit with one of these,
deflect it with something like, "I thought we were arguing about [insert topic
here].  Why are you attacking me personally?"

**Best case scenario**

  They'll be shocked and horrified by what they did, apologize promptly, and
  the two of you can get back to tackling the issue at hand.

**More likely scenario**

  They're not engaged in this debate in good faith, and are trying whatever
  underhanded tactics they can to score some points and/or save face.  You can
  probably gauge whether this is the case beforehand by the tone and
  progression of the conversation up till this point.  In this case, the
  purpose of your question is to (1) leave no doubt in their mind as to what
  they're doing, and (2) showcase to the wider audience that you're attempting
  to get at the truth while they most decidedly are not.

Addressing Rudeness
^^^^^^^^^^^^^^^^^^^

On occasion you might find yourself dealing with someone who has a tendency to
just be downright rude.  The cause may be innocent enough---perhaps their
parents simply never taught them social graces---or it may be that they're
using rudeness as a weapon to distract from the weakness of their
argumentation.  Consider the following scenarios:

* You're dealing with a prolific talker who just won't let you get a word in
  edgewise.  A simple, "Do you mind if I interject something here?" may help,
  in which case you give them the opportunity to realize their error and
  correct.  However, if you sense they're just being a jerk, you might follow
  that up with, "Or would you like to just keep on talking?", which points out
  their dirty tactic to the wider audience.
* The other person resorts to shouting to try to get you to acquiesce to their
  position.  It may be they've just gotten carried away in their excitement, in
  which case a simple, "Can we take a moment to cool down a bit?" may help.
  However, if you sense that perhaps they have a tendency to bouts of anger and
  are attempting to berate you into submission, then something more like, "Do
  you think whoever's loudest is right?  Or that you can scare me into agreeing
  with you?" might be more appropriate.
* Your opponent just keeps throwing questions at you, one after the other.  A
  simple, "Would you like me to answer that one?" may help, or something like,
  "Would you mind if we wrote down all your questions first, so we could
  prioritize them and figure out what order to tackle them in?"  If they're
  actually interested in a search for truth, this should help them be more
  productive in their conversation with you.  However, if you sense that's not
  the case, you may want to use something like, "Do you actually want answers
  to any of these questions, or do you just want to keep up the barrage till I
  go away?"

In all of the above, the recommendation would be to not assume ill intent at
first.  Start with a gentle course correction, and if it works, then the
dialogue can continue productively.  When a subtle redirect fails, and when you
sense obstinacy on their end, that's when you move on to the more pointed
questions to put a spotlight on their bad behavior.  In that case, you're
likely no longer expecting them to change, and you expect the conversation's
pretty much over, but you want to make sure nobody leaves without realizing
that the breakdown of communication was on their end, not yours.  You're still
genuinely searching for truth; they decided they didn't want any part of that.

Narrating the Debate
^^^^^^^^^^^^^^^^^^^^

We discussed up above what to do when your interlocutor tries to take a hard
left turn somewhere with a red herring, but you may also find yourself in
situations where there are no abrupt shifts in topic, but you realize the
conversation has drifted over its course.  Maybe you started with the southern
border crises, eventually wound up talking migrant workers' willingness to work
jobs Americans simply won't, moved on from there to the failing educational
system, etc.  Any of these topics would be worth diving into further, but you
haven't spent enough time on any of them to learn anything in any depth or hash
out any disagreements.  You can tell you and your opponent are on opposite
sides of the issues, but you've been unable to actually work through any
differences because the conversation has taken such a circuitous route.

Whenever this is the case, it's worth taking a step back and narrating the
debate, so your opponent and all your listeners are clear as to the journey
you've taken.  Try something like, "Hold up, can we pause for a moment and
review where we've been?  We started off at [point A], then transitioned to
[point B].  After that we [fill in the blank].  Do you mind if we focus on one
of these topics so we can actually dive into it and hash it out?"  As you're
recapping each of the points, it'll be worthwhile to state what the main
contentions were, and where you agreed and disagreed.  It may even be helpful
to sketch out a little map of the conversation, so you both have a visual
representation in front of you for reference.  The goal here is to both put a
stop to the meandering and to focus the conversation so it can be more
productive.  At the very least, you both should leave with a clearer
understanding of the discussion that was had.

Requesting Time to Process
^^^^^^^^^^^^^^^^^^^^^^^^^^

The final situation in which you may want to use questions to change the course
of the conversation is when you need some time to think.  Remember, the
assumption going into the conversation is that you don't know everything and
you may be mistaken in one or more of your beliefs.  If the conversation has
been even somewhat productive, you should now have more information than when
you started.  Sometimes that new information can't be evaluated on the spot.
Either you need some time to look into the source material, or you'll need to
think through how it fits with your mental model of the issue at hand.  Perhaps
it doesn't fit, which means some change in your understanding is likely
required.  Whatever the scenario, you may have reached the limits of thinking
and processing on your feet, and you need some time away to ponder things.  In
that case, a simple, "I think I'm going to need some time to process this.  Do
you mind if we pick up the conversation again on [insert date/time]?"  That
should be totally fine, and always be willing to extend to them the same
courtesy.  Remember that you don't have to work through absolutely everything
in a single conversation with each other.  The hope is that you both grow in
your understanding with each interaction.

Now there may be times when you sense your opponent is pressuring you to come
up with an answer or make a decision on the spot.  For some reason they think
you must be ready with a rapid response or admit defeat.  In that case your
question is more along the lines of, "Do you want me to actually think before I
respond, or do you just want the first thought that pops into my head?"  Or
maybe, "Why do I have to make a decision right this moment?  Can I have some
time to process first?"  You're making sure they realize that they're being
unfair in how they're engaging you, and you force the audience to think about
whether they want to see well-reasoned argumentation or just a bunch of
bickering back and forth.

Next Steps
==========

At this point you've learned the three fundamental purposes for using questions
in dialogue:  gathering information, making a point, and redirecting a
conversation.  You should be sufficiently better equipped than many of your
peers to improve the quality of conversations you find yourself in.  Now it's
time to start putting it all into practice.  Go ask questions to clarify terms,
analysis, sources, and motives.  Use questions to point out logical fallacies,
weak sources, unsubstantiated opinions, and hypocrisy.  And when needed, use
them to defend against attacks and rudeness, to recapitulate the course of the
debate, and to arrange a time to reconvene and pick things back up later.

In addition, train your friends in all these same skills, and hold one another
accountable to using them frequently.  Always be on the lookout for how you can
improve your tact in phrasing, tone, and delivery.  Debrief with a friend after
an encounter to talk through what worked well that you should keep doing, what
went poorly that you need to improve, and what you forgot about that would've
been beneficial, so you can remember to do that next time.

And finally, if you're looking for further study in this arena, I highly
recommend you pick up a copy of |tactics|_, by Greg Koukl.  He expertly takes
this same skill set, explains things in slightly different ways, and applies it
specifically to the realm of sharing and defending your faith.  It was easily
one of the best books I read in 2021.

.. _tactics:  https://www.amazon.com/Tactics-10th-Anniversary-Discussing-Convictions/dp/0310101468/ref=sr_1_1?keywords=tactics+greg+koul&sr=8-1
.. |tactics| replace::  *Tactics:  A Game Plan for Discussing Your Christian Convictions*

