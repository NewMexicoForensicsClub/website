Why I Choose to Believe the Bible
---------------------------------

.. role:: raw-html(raw)
   :format: html

:raw-html:`<div align="right">🕑 22 min.</div>`

Here we get into some of the specifics behind the phrase, "I choose to believe
the Bible because it is a reliable collection of historical documents, written
down by eyewitnesses during the lifetime of other eyewitnesses; they report
supernatural events that took place in fulfillment of specific prophecies, and
claim that their writings are divine rather than human in origin."

.. contents:: Contents
   :local:

This sentence comes from the following lecture by `Voddie Baucham Jr.
<https://www.voddiebaucham.org/>`_

.. raw:: html

    <div style="position: relative;
                padding-bottom: 56.25%;
                height: 0;
                overflow: hidden;
                max-width: 100%;
                height: auto;">
        <iframe
         src="https://www.youtube.com/embed/1IxUJyTrUnY?start=180"
         frameborder="0"
         allowfullscreen
         style="position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                padding: 10px;">
        </iframe>
    </div>

The sentence is a summary and consolidation of the following passage of
scripture.

.. raw:: html

   <div style="border: 2px solid ForestGreen;
               border-radius: 5px;
               padding: 10px;
               background-color: HoneyDew;">
       <div data-gb-widget="passage" data-passage="2PE.1.16-2PE.1.21">
       </div>
   </div>
   <br>

Let's walk through the different components of it.

A Reliable Collection of Historical Documents
=============================================

There are actually three points being made in this brief phrase, and we'll walk
through them in reverse order.

Historical
~~~~~~~~~~

First the Bible is a *historical* document, being written over a period of
about 1500 years.  The first five books of the Bible were written down around
1400 BC,\ [#did_moses_write_genesis]_ and the New Testament was completed
toward the end of the first century
AD.\ [#was_the_new_testament_written_hundreds_of_years_after_christ]_  While
the Bible contains a variety of literary forms, it is intended to provide a
historically accurate account of God's dealings with his people and the world.
This is particularly evident with the gospel of Luke.

.. raw:: html

   <div style="border: 2px solid ForestGreen;
               border-radius: 5px;
               padding: 10px;
               background-color: HoneyDew;">
       <div data-gb-widget="passage" data-passage="LUK.1.1-LUK.1.4">
       </div>
   </div>
   <br>

Luke was a physician and historian who set out to compile a detailed and
orderly historical account of all that had happened in the life of
Jesus.\ [#who_was_luke_in_the_bible]_  This wasn't just mythology, as mentioned
in |2PE.1.16|, or embellished storytelling; rather, it was first-century
investigative journalism reporting on events as they actually happened.
Scholars have long debated the historicity of the biblical account, but
archaeological discoveries continue to support scripture, and as of yet no
archaeological discovery has ever contradicted
it.\ [#why_you_can_believe_the_bible]_

.. |2PE.1.16| replace:: :raw-html:`<a data-gb-link="2PE.1.16">2 Peter 1:16</a>`

Collection
~~~~~~~~~~

As can be expected, having been written over ~1500 years, the Bible is not a
single document, but rather a *collection* of documents.  It's comprised of 66
books, written by over 40 different authors from all walks of
life.\ [#how_many_people_wrote_the_bible]_  These authors wrote in three
different languages:  Hebrew, Aramaic, and
Greek.\ [#in_what_language_was_the_bible_first_written]_  The books were
written on three different continents:  Asia, Africa, and Europe.  The Bible
isn't a book that any one person generated on their own, as a number of other
religious texts are.  Rather, dozens of authors, spread across a variety of
cultures, locations, time periods, and languages, deliver a historically
accurate and self-consistent testimony of God's interactions with mankind.

Reliable
~~~~~~~~

Even if we have a collection of historical documents, how do we know those
documents are *reliable*?  There are actually two questions to tackle here:

1. How do we know what was originally written was accurate?
2. How do we know what we have today matches what was originally written?

The first question we'll get to momentarily, so let's tackle the second, and
we'll do so by examining a few different areas of analysis.

Bible Translation
^^^^^^^^^^^^^^^^^

You may hear that the translation of scripture has played out like a game of
telephone over the last few millenia.  First it was written in Hebrew, then
translated to Greek, then into Latin, then old English, then eventually into
modern English.  Naturally with that many layers of translation, there are
bound to be errors that creep in that get propagated and then exaggerated over
time.  At face value, that may seem like a legitimate concern, however the
story is fictitious.  The Bible isn't translated from translations; rather,
Bible translaters go back to the ancient manuscripts written in Greek, Hebrew,
and Aramaic.  Not only that, but if you were to take the time to learn those
languages, you could go back to those same documents yourself (or at least
digital versions of them) and verify that the translations we have today line
up with what was originally written.\ [#evidence_that_demands_a_verdict]_

Early Copies
^^^^^^^^^^^^

Unfortunately you can't go back to the *original* documents, as the media on
which they were written (papyrus and the like) were highly susceptible to
degradation over time.  However, we do have a remarkable number of ancient
copies of the original documents.  The numbers will vary, depending on how you
count, but one number is we have 5,856 manuscripts of the New Testament in
Greek, with the earliest ones dating to 130 A.D. or earlier, which puts them
within a few decades of the New Testament having been completed.  If you then
consider the translations of the New Testament into other languages of the
people (e.g., Syriac, Coptic, Latin, etc.), the number of manuscripts jumps up
to about 24,000.  If you want to consider scrolls and codices containing
sections of the Old Testament, that adds another ~42,000 manuscripts, bringing
the total to roughly 66,000 ancient manuscripts against which we can compare
modern Bible translations.\ [#evidence_that_demands_a_verdict]_

Early Church Fathers
^^^^^^^^^^^^^^^^^^^^

In addition to all the ancient manuscripts in the original languages, plus the
number of early translations, we also have the evidence of the early church
fathers.

    "... the textual critic has available the numerous scriptural quotations
    included in the commentaries, sermons, and other treatises written by the
    early Church fathers.  Indeed, so extensive are these citations that if all
    other sources for our knowledge of the text of the New Testament were
    destroyed, they would be sufficient alone for the reconstruction of
    practically the entire New Testament."\ [#metzger_and_ehrman]_

If we just consider what was written before the Council of Nicea in 325 A.D.,
we already have over 36,000 citations of
scripture.\ [#evidence_that_demands_a_verdict]_

Other Ancient Works
^^^^^^^^^^^^^^^^^^^

For the sake of comparison, let's see how other ancient works stack up against
the Bible.  Homer's *Iliad*, for instance, was written around 1200 BC and we
have about 1900 manuscripts of it, with the earliest one dating to around 415
BC (an ~800-year gap).  Caesar's *Gallic Wars* was written around 50 BC, but
the earliest manuscript we have is from around 850 AD (a ~900-year gap), and we
only have about 250 manuscripts total.  Livy's *History of Rome* was written
around 10 BC, the earliest manuscript we have is from ~350 years later, and at
this point we have about 500 manuscripts.  We could examine more examples, but
in terms of surviving source material, the Bible is the most well-supported
ancient work; other books, the reliability of which we don't think twice about,
don't even come close.\ [#evidence_that_demands_a_verdict]_

Written Down by Eyewitnesses
============================

Now that we've established the accurate transmission of scripture from when it
was originally written to today, let's return to the question of whether or not
what was originally written was accurate.  We saw earlier in |2PE.1.16| that
Peter mentions they were *eyewitnesses* of the majesty of Christ.  Let's see
what John has to say about it.

.. raw:: html

   <div style="border: 2px solid ForestGreen;
               border-radius: 5px;
               padding: 10px;
               background-color: HoneyDew;">
       <div data-gb-widget="passage" data-passage="1JN.1.1-1JN.1.3">
       </div>
   </div>
   <br>

John also hammers home that they saw, heard, and touched everything that
they're writing about.  These aren't just fanciful tales; rather, the authors
wrote about the events they actually experienced, or wrote their works after
compiling such eyewitness testimony.  Such accounts are crucial to, for
instance, criminal and legal proceedings, as the presence of eyewitness
testimony adds substantial credibility to any claims being made, as you're
getting the information straight from the horse's mouth, so to
speak.\ [#how_ancient_eyewitness_testimony_became_the_new_testament_gospel_record]_

Well hang on a second, what if the eyewitness happens to be lying?  That's why
this next phrase is so important.

During the Lifetime of Other Eyewitnesses
=========================================

In a legal context, if an eyewitness claims one thing, and it's possible for
another eyewitness to turn up and claim the opposite, the initial claim is what
we call *falsifiable*.\ [#basic_concepts_falsifiable_claims]_  Put simply, that
means that people can prove that you're lying.  If you're an eyewitness making
a claim, knowing that there are other eyewitnesses to the same event that can
speak up and falsify whatever you say, you're going to be very careful about
what you say or write down.

.. note::

   When googling the term "falsifiable", you often find results pertaining to
   the context of the scientific method, but it also pertains to matters that
   can be falsified by other means such as historical evidence.

It's worthwhile here to focus in on one event in scripture rather than trying
to tackle all of it at once:  the resurrection of Jesus.  Eyewitnesses claim
that Jesus was crucified, was very much dead (not just `mostly dead
<https://www.youtube.com/watch?v=xbE8E1ez97M>`_), was raised to life again, and
interacted with hundreds of people before ascending to heaven.  Such
eyewitnesses testimony could easily be falsified by, for instance, producing
the dead body, or proving that the man claiming to be the resurrected Jesus was
not the same Jesus that was crucified.  Why should we just focus on the
resurrection?  Let's see what Paul has to say about that.

.. raw:: html

   <div style="border: 2px solid ForestGreen;
               border-radius: 5px;
               padding: 10px;
               background-color: HoneyDew;">
       <div data-gb-widget="passage" data-passage="1CO.15.1-1CO.15.19">
       </div>
   </div>
   <br>

If the story of Jesus' resurrection is a lie, all the claims of Christianity
fall apart; however, if it's true, then all those claims hold
together.\ [#why_is_the_resurrection_of_jesus_christ_important]_ Paul was
writing 1 Corinthians only about 20 years after the resurrection took place, so
many of the original eyewitnesses to it were still available to corroborate or
contradict what he says here.\ [#whats_up_with_those_500_witnesses]_ However,
the claims of the resurrection were not falsified,\ [#is_it_falsifiable]_ and
there were a number of people who would have gladly done so if they could
have.\ [#is_the_bible_written_by_eyewitnesses_of_the_events]_

They Report Supernatural Events
===============================

Now if we were to stop after, "I choose to believe the Bible because it is a
reliable collection of historical documents, written down by eyewitnesses
during the lifetime of other eyewitnesses," all we would have is a really good
history book.  What sets it apart from other really good history books is that
the authors record *supernatural events*, and what's meant by that term isn't
merely *super-human* feats, but things that are completely outside the laws of
nature:  speaking the universe into existence out of nothing (|GEN.1|), the
parting of the Red Sea (|EXO.14|), making the sun stand still for a day
(|JOS.10|), the virgin birth (|MAT.1|), walking on water (|MRK.6|), rising from
the dead (|LUK.24|), and the list goes on.  Up above in |2PE.1.17-2PE.1.18|,
Peter recounts the story of the transfiguration, where he, James, and John
witnessed Jesus being revealed in his glory, conversing with Moses and Elijah
in bodily form, along with the voice of God speaking from heaven.  Why is the
supernatural significant?  Because this really good history book doesn't just
contain theology, or religious or political philosophy.  It's an account of how
the supernatural creator and sustainer of all existence interacts with both his
creation in general, and you, the reader,
specifically.\ [#the_importance_of_the_supernatural]_

.. |GEN.1| replace:: :raw-html:`<a data-gb-link="GEN.1.1-GEN.1.31">Genesis 1</a>`
.. |EXO.14| replace:: :raw-html:`<a data-gb-link="EXO.14.1-EXO.14.31">Exodus 14</a>`
.. |JOS.10| replace:: :raw-html:`<a data-gb-link="JOS.10.1-JOS.10.43">Joshua 10</a>`
.. |MAT.1| replace:: :raw-html:`<a data-gb-link="MAT.1.1-MAT.1.25">Matthew 1</a>`
.. |MRK.6| replace:: :raw-html:`<a data-gb-link="MRK.6.1-MRK.6.56">Mark 6</a>`
.. |LUK.24| replace:: :raw-html:`<a data-gb-link="LUK.24.1-LUK.24.53">Luke 24</a>`
.. |2PE.1.17-2PE.1.18| replace:: :raw-html:`<a data-gb-link="2PE.1.17-2PE.1.18">2 Peter 1:17&ndash;18</a>`

That Took Place in Fulfillment of Specific Prophecies
=====================================================

Above in |2PE.1.19|, Peter speaks of the confirmation of the prophetic word,
where he's talking about the prophecies from scripture that were fulfilled in
the life, death, and resurrection of Jesus Christ.  It's important to note that
these foretellings of future events in the Bible aren't what you get from the
likes of `Nostradamus <https://en.wikipedia.org/wiki/Nostradamus#In_popular_culture>`_
or the horoscope section of the paper.  Here we're talking about specific
predictions that were fulfilled in very specific ways.  How many are we talking
about?  It depends on how you count them, but a safe estimate is at least
300.\ [#how_many_prophecies_did_jesus_fulfill]_  We won't dive into all of them
here (though you're encouraged to do so); instead we'll just focus on two.

.. |2PE.1.19| replace:: :raw-html:`<a data-gb-link="2PE.1.19">2 Peter 1:19</a>`

Apparently it's common in the regular readings within the Jewish community to
read the first part of Isaiah 52, then skip over chapter 53, and pick back up
with chapter 54.\ [#isaiah_53_the_forbidden_chapter]_

.. raw:: html

   <div style="border: 2px solid ForestGreen;
               border-radius: 5px;
               padding: 10px;
               background-color: HoneyDew;">
       <div data-gb-widget="passage" data-passage="ISA.53.1-ISA.53.12">
       </div>
   </div>
   <br>

Now if you were to read that chapter to someone but not tell them where it
comes from, chances are they could tell you it was about Jesus, and they might
assume it comes from somewhere in the New Testament.  In actuality, this vivid
depiction of the week leading up to Jesus' death was written by Isaiah roughly
700 years before Jesus walked the face of the earth.

For this next example, it's important to realize that the subdivision of our
Bibles into chapters and verses is a relatively recent development.  If you
wanted to direct people to a particular passage of scripture, you wouldn't say,
"Turn in your Bibles to [book, chapter, and verse]," but rather, "Open the
scroll to [first line of the passage]," and you would have an idea of where
that was and what comes next.  Using that kind of memory technique actually
works rather well.  For instance, if I were to say, "Yesterday, all my troubles
seemed so far away," you instantly start thinking of `the rest of the Beatles'
song <https://youtu.be/wXTJBr9tt8Q>`_.  Or when I say, "Somebody once told me,"
the rest of Smash Mouth's `All Star <https://youtu.be/L_jWHffIx5E>`_ is already
playing in your head.

Why does this matter?  If you've heard the story of Jesus' crucifixion, you
probably remember in |MAT.27.46| where Jesus, on the cross, cries out, "Eloi,
eloi, lama sabacthani," and then the text goes on to say that the translation
of that is, "My God, my God, why have you forsaken me?"  Had you ever wondered
why the text was phrased that way?  It turns out the words Jesus cries out are
the opening words to Psalm 22, which for those hearing him would have called to
mind the rest of the passage.

.. |MAT.27.46| replace:: :raw-html:`<a data-gb-link="MAT.27.46">Matthew 27:46</a>`

.. raw:: html

   <div style="border: 2px solid ForestGreen;
               border-radius: 5px;
               padding: 10px;
               background-color: HoneyDew;">
       <div data-gb-widget="passage" data-passage="PSA.22.1-PSA.22.18">
       </div>
   </div>
   <br>

This description of Jesus' death was written roughly 1000 years before it
happened.  Why do such fulfillments of prophecy matter?  Because they are
substantial evidence in support of our final point, to which we'll now
turn.\ [#why_is_prophecy_important]_

And Claim That Their Writings Are Divine Rather than Human in Origin
====================================================================

Back up in |2PE.1.20-2PE.1.21|, Peter tells us that "men spoke from God as they
were carried along by the Holy Spirit."  Similarly:

.. |2PE.1.20-2PE.1.21| replace:: :raw-html:`<a data-gb-link="2PE.1.20-2PE.1.21">2 Peter 1:20&ndash;21</a>`

.. raw:: html

   <div style="border: 2px solid ForestGreen;
               border-radius: 5px;
               padding: 10px;
               background-color: HoneyDew;">
       <div data-gb-widget="passage" data-passage="2TI.3.16">
       </div>
   </div>
   <br>

We use the terms *inspired* or *inspiration* to refer to the process by which
God caused human authors to record his words for the sake of communicating them
to the world.  God is the ultimate author of the Bible.

    "[T]he technical theological sense of inspiration, largely lost in the
    secular atmosphere of our time, is clearly asserted by the Scriptures with
    a special view to the sacred writers and their writings.  Defined in this
    sense, inspiration is a supernatural influence of the Holy Spirit upon
    divinely chosen agents in consequence of which their writings become
    trustworthy and authoritative."\ [#bible_inspiration_of]_

Now there are some who either knowingly or unknowingly hold to the opinion that
we really can't trust the Bible to accurately convey God's truth to us because
ultimately it was written by fallible human beings.  Following that line of
reasoning to its logical conclusion means we really can't trust any book (as
they were all written by humans) and so we really can't know anything with
certainty.  There are some who adamantly hold to such an `epistemology
<https://en.wikipedia.org/wiki/Epistemology>`_ (a view of what is knowable and
how you can know it), but most will see such a view as being out of touch with
reality.  All truth is God's truth.\ [#all_truth_is_gods_truth]_  Rather than
approach a text with the question of who wrote it, we should first approach it
with the question of whether or not it's
true.\ [#you_cant_trust_the_bible_because_it_was_written_by_humans]_  The
source of the information *can* contribute to your assessment of the
trustworthiness of the information, but to say, "this information came from
[source], therefore it must be false," is an example of :doc:`the genetic
fallacy </training/logical-fallacies>`.

Others will argue that we can't believe such an audacious claim (that the Bible
is divine in origin) unless we can prove it to be true scientifically.  This
reveals another epistemological flaw:  thinking that the only things that are
knowable are those that can be proven by the `scientific method
<https://en.wikipedia.org/wiki/Scientific_method>`_, meaning they must be
observable, measurable, and repeatable.  However, that methodology does not
apply when trying to ascertain the veracity of historical matters, such as the
inspiration of scripture or the details of the life, death, and resurrection of
Jesus, as history meets none of the criteria for operational scientific
inquiry.  Instead you use the `historical method
<https://en.wikipedia.org/wiki/Historical_method>`_, which involves examining
the provenance of source material, determining its credibility, seeking out
eyewitness testimony, and the like.  If you're familiar with crime procedurals
on television, this is how they go about etablishing the facts of the case.

So What?
========

Whether or not you believe any of the claims mentioned above, why does any of
this matter?  Well consider this for a moment:  If we have a reliable
collection of historical documents, arguably the most verifiably accurate
ancient history book the world has ever known...  If those documents contain
eyewitness testimony making claims that were never falsified by the other
eyewitnesses to the same events...  If that testimony recounts events
comepletely outside the realm of natural phenomena, which were predicted in
astounding detail, sometimes hundreds of years in advance, and if the book
claims its ultimate author is the all-powerful creator and sustainer of
absolutely everything in existence, including me, then I really need to take
some time to figure out what it says about

* who I am,
* why I'm here,
* what's wrong with the world, and
* how what's wrong can be made right.

If you're skeptical of any of this, it can be tempting to fall victim to what I
call Scarlett O'Hara syndrome:

    "I can't think about that right now.  If I do, I'll go crazy.  I'll think
    about that tomorrow."

It's also far too easy to think, "Gosh, this is just so much to take in, I
don't think I buy it," (an example of the :doc:`fallacy of personal incredulity
</training/logical-fallacies>`).  Rather than letting your skepticism end where
it begins, I encourage you to engage with the material and wrestle with the
truth claims.  Look into the references below to see what they have to say.  If
you're not sure about certain points, talk them through with someone.  This
brief write-up isn't meant to answer all your questions; it merely scratches
the surface.  Countless individuals have devoted their lives to backing up
these claims with far more proficiency than I ever could.

For the Christian, we point to scripture because it is *the* trustworthy source
with which all our answers must agree.  If at any point our thinking doesn't
line up with the truths God's revealed to us in the Bible, that's a problem.

.. raw:: html

   <script src="https://bibles.org/static/widget/v2/widget.js"></script>
   <script>
       GLOBALBIBLE.init({
           url: "https://bibles.org",
           bible: "f421fe261da7624f-01",
           autolink: false,
       });
   </script>

.. rubric:: References

.. [#did_moses_write_genesis]
   Bodie Hodge and Dr. Terry Mortenson,
   "Did Moses Write Genesis?"
   June 28, 2011,
   Answers in Genesis,
   https://answersingenesis.org/bible-characters/moses/did-moses-write-genesis/,
   retrieved March 14, 2021.

.. [#was_the_new_testament_written_hundreds_of_years_after_christ]
   Matt Slick,
   "Was the New Testament written hundreds of years after Christ?"
   November 22, 2008,
   Christian Apologetics & Research Ministry,
   https://carm.org/the-bible/was-the-new-testament-written-hundreds-of-years-after-christ/,
   retrieved March 19, 2021.

.. [#who_was_luke_in_the_bible]
   "Who was Luke in the Bible?"
   Got Questions,
   https://www.gotquestions.org/Luke-in-the-Bible.html,
   retrieved March 19, 2021.

.. [#why_you_can_believe_the_bible]
   "Why You Can Believe the Bible,"
   EveryStudent.com,
   https://www.everystudent.com/features/bible.html,
   retrieved March 19, 2021.

.. [#how_many_people_wrote_the_bible]
   "How Many People Wrote the Bible?"
   All About Truth,
   https://www.allabouttruth.org/how-many-people-wrote-the-bible-faq.htm,
   retrieved March 19, 2021.

.. [#in_what_language_was_the_bible_first_written]
   "In what language was the Bible first written?"
   Biblica:  The International Bible Society,
   https://www.biblica.com/resources/bible-faqs/in-what-language-was-the-bible-first-written/,
   retrieved March 19, 2021.

.. [#evidence_that_demands_a_verdict]
   Josh McDowell and Sean McDowell, PhD,
   *Evidence that Demands a Verdict:  Life-Changing Truth for a Skeptical World*,
   Harper Collins Publishers,
   2017, orig. pub. 1984,
   chs. 3--4.

.. [#metzger_and_ehrman]
   Bruce M. Metzger and Bart D. Ehrman,
   *The Text of the New Testament:  Its Transmission, Corruption, and Restoration*,
   University Press,
   2005, orig. pub. 1992,
   p. 126.

.. [#how_ancient_eyewitness_testimony_became_the_new_testament_gospel_record]
   J. Warner Wallace,
   "How Ancient Eyewitness Testimony Became The New Testament Gospel Record,"
   May 30, 2018,
   Cold-Case Christianity,
   https://coldcasechristianity.com/writings/how-ancient-eyewitness-testimony-became-the-new-testament-gospel-record/,
   retrieved April 16, 2021.

.. [#basic_concepts_falsifiable_claims]
   Janet D. Stemwedel,
   "Basic concepts:  falsifiable claims,"
   January 31, 2007,
   ScienceBlogs,
   https://scienceblogs.com/ethicsandscience/2007/01/31/basic-concepts-falsifiable,
   retrieved April 23, 2021.

.. [#why_is_the_resurrection_of_jesus_christ_important]
   "Why is the resurrection of Jesus Christ important?"
   Got Questions,
   https://www.gotquestions.org/resurrection-Christ-important.html,
   retrieved April 23, 2021.

.. [#whats_up_with_those_500_witnesses]
   Demer Webb,
   "What's up with those 500 witnesses?"
   April 14, 2016,
   Explore the Story,
   https://explorethestory.wordpress.com/2016/04/14/whats-up-with-those-500-witnesses/,
   retrieved April 23, 2021.

.. [#is_it_falsifiable]
   Dr Michael G. Strauss,
   "Is it Falsifiable?"
   October 20, 2019,
   Experimental particle physicist Dr Michael G Strauss discusses the relationship between science, God, Christianity, and reason,
   https://www.michaelgstrauss.com/2019/10/is-it-falsifiable.html,
   retrieved April 23, 2021.

.. [#is_the_bible_written_by_eyewitnesses_of_the_events]
   Teri Dugan,
   "Case-Making 101:  Is the Bible written by eyewitnesses of the events?"
   December 10, 2016,
   Truth, Faith and Reason,
   https://truthfaithandreason.com/case-making-101-is-the-bible-written-by-eyewitnesses-of-the-events/,
   retrieved April 23, 2021.

.. [#the_importance_of_the_supernatural]
   Dr. Douglas Stuart,
   "The Importance of the Supernatural:  Why is belief in supernatural events so important for a proper understanding of the Bible?"
   Thirdmill,
   https://thirdmill.org/answers/answer.asp?file=43334,
   retrieved April 25, 2021.

.. [#how_many_prophecies_did_jesus_fulfill]
   "How many prophecies did Jesus fulfill?"
   Got Questions,
   https://www.gotquestions.org/prophecies-of-Jesus.html,
   retrieved April 25, 2021.

.. [#isaiah_53_the_forbidden_chapter]
   Dr. Eitan Bar,
   "Isaiah 53 - The forbidden chapter,"
   October 1, 2017,
   One For Israel,
   https://www.oneforisrael.org/bible-based-teaching-from-israel/inescapable-truth-isaiah-53/,
   retrieved April 25, 2021.

.. [#why_is_prophecy_important]
   Todd Hampson,
   "Bible Prophecy 101:  Why is Prophecy Important?"
   ToddHampson.com,
   https://toddhampson.com/bible-prophecy-101-why-is-prophecy-important/,
   retrieved April 25, 2021.

.. [#bible_inspiration_of]
   C. F. H. Henry,
   "Bible, Inspiration of,"
   *Evangelical Dictionary of Theology, Second Edition*,
   edited by Walter A. Elwell,
   Baker Book House Company,
   2001, orig. pub. 1984,
   pp. 159-163.

.. [#all_truth_is_gods_truth]
   R.C. Sproul,
   "All Truth Is God's Truth,"
   Ligonier Ministries,
   https://www.ligonier.org/learn/articles/all-truth-gods-truth-sproul/,
   retrieved May 5, 2021.

.. [#you_cant_trust_the_bible_because_it_was_written_by_humans]
   J. Warner Wallace
   "Quick Shot:  You Can't Trust The Bible Because It Was Written By Humans,"
   May 13, 2019,
   Cold-Case Christianity,
   https://coldcasechristianity.com/writings/quick-shot-you-cant-trust-the-bible-because-it-was-written-by-humans/,
   retrieved May 5, 2021.
