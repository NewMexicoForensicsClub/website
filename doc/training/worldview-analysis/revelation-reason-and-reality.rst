Revelation, Reason, and Reality
-------------------------------

.. role:: raw-html(raw)
   :format: html

:raw-html:`<div align="right">🕑 6 min.</div>`

.. contents:: Contents
   :local:

.. sidebar::  Fun Fact

   The English word *science* comes from the Latin word *scientia*, which
   simply means *knowledge*.  "The sciences," then, aren't just the subjects we
   typically think of today as being scientific (chemistry, biology, etc.), but
   are rather any means of gaining knowledge.

When it comes to the question of how we can know truth, theologians typically
break it down into two categories:  `special and general revelation`_.  Special
revelation is basically how God communicates directly to us, and general
revelation is how we're able to learn from his creation; that is, via the
sciences.  I prefer to break general revelation down into two separate
categories:  one dealing with sciences that have something concrete (e.g.,
history, physics, etc.) as their object of study, and the other having how
minds think as the object (that is, the science of reason).  We therefore have
the following "three Rs" (yay for alliteration), which are umbrella terms for
how God communicates truth to us:

.. _special and general revelation:  https://www.gotquestions.org/general-special-revelation.html

Revelation
  God's direct communication to us through his written word, through the
  indwelling Holy Spirit, and through his miraculous intervention in the world
  he created and superintends.
Reason
  God's way of thinking via rational thought processes, which we think after
  him by virtue of being made in his image.
Reality
  God's indirect communication to us through his created order, which we can
  study through the observational and historical sciences.

Under each of these umbrellas, then, are a number of skills in which we can
develop our competency over time.

.. note::

   Don't think of these three categories as hard and fast subdivisions.
   There's some overlap, and developing skills in one area improves your skills
   in another.

Revelation
^^^^^^^^^^

A primary habit to be cultivated under revelation is that of reading the
scriptures.  There are some who contend that we are where we are today simply
because Christians are largely ignorant of what the Bible says.  If the only
Bible reading we do is in conjunction with the sermons on Sunday, and if those
slowly work their way through scripture a few verses at a time, it can take a
lifetime to finally make it through the whole counsel of God.  That's simply
too slow a pace, and it means we're just not familiar enough with everything
God has to say.  Pick one of the many "read the Bible in a year" plans and go
with it.  Don't get discouraged if you get behind a few days---if it takes you
two years to make it through, that's better than twenty.  When you make it
through, celebrate, and then start back in again. |the_daily_bible|_ is a good
resource in this endeavor.

.. _the_daily_bible:  https://smile.amazon.com/Daily-Bible%C2%AE-NIV-LaGard-Smith/dp/0736980296/ref=sr_1_1?keywords=the+daily+bible+in+chronological+order+365+daily+readings&qid=1659354637&sprefix=the+daily+bible%2Caps%2C165&sr=8-1
.. |the_daily_bible| replace::  *The Daily Bible*

Beyond simply reading scripture every day, we must commit ourselves to
memorization, that we might hide truth in our heart (|PSA.119.11|).  Part of
this is memorizing scripture itself, whether that means individual verses, or
whole books of the Bible (start with the shortest books to build your
confidence).  A program like `Awana`_ can be a big help here.  In addition to
memorizing scripture, consider also memorizing one or more of the early
Christian statements of faith, such as the `Apostles'`_ or `Nicene`_ creeds.  A
final practice in the realm of memorization that has been largely lost in
evangelicalism is that of *catechism*, which is just a fancy word for a summary
of doctrine compiled in a question and answer format.  Consider using
|a_catechism_for_girls_and_boys|_, though there are a number of others to
choose from.

.. |PSA.119.11| replace:: :raw-html:`<a data-gb-link="PSA.119.11">Psalm 119:11</a>`
.. _Awana:  https://www.awana.org/about/
.. _Apostles':  https://www.crcna.org/welcome/beliefs/creeds/apostles-creed
.. _Nicene:  https://www.crcna.org/welcome/beliefs/creeds/nicene-creed
.. _a_catechism_for_girls_and_boys:  http://www.reformedreader.org/ccc/acbg.htm
.. |a_catechism_for_girls_and_boys| replace::  *A Catechism for Girls and Boys*

.. note::

   As you're memorizing these extra-biblical resources, keep in mind that they
   are not infallible as the Bible itself is.

In addition to knowing what's in the Bible, we also need to hone our skills
when it comes to interpreting what it says.  The practice of understanding the
meaning of scripture is known as :doc:`biblical hermeneutics
<../biblical-hermeneutics>`.  To an extent, it's simply an application of `how
to analyze literature`_, where the literature in question is the word of God.
If you get good at one, you get good at the other, so a resource like
|how_to_read_a_book|_, by `Mortimer J. Adler`_, can be beneficial.  More
specific to scripture, though, you might consider learning and practicing the
`inductive Bible study method`_, though other methods abound.  A significant
difference between interpreting scripture versus other works is you have the
author on hand to help you.  As such, books like `Richard Foster`_'s
|celebration_of_discipline|_ can be helpful, in that spiritual disciplines like
prayer and fasting help to put us in a place where God can better inform and
correct us.

.. _how to analyze literature:  https://www.youtube.com/watch?v=zY3A5LiW5bk
.. _how_to_read_a_book:  https://smile.amazon.com/How-Read-Book-Classic-Intelligent/dp/0671212095/ref=sr_1_1?crid=32L4Y35SEN3W2&keywords=how+to+read+a+book&qid=1659390686&sprefix=how+to+read+a+boo%2Caps%2C272&sr=8-1
.. |how_to_read_a_book| replace::  *How to Read a Book*
.. _Mortimer J. Adler:  https://en.wikipedia.org/wiki/Mortimer_J._Adler
.. _inductive Bible study method:  https://biblestudy.tips/inductive-bible-study/
.. _Richard Foster:  https://renovare.org/people/richard-foster/bio
.. _celebration_of_discipline:  https://smile.amazon.com/Celebration-Discipline-Special-Anniversary-Spiritual/dp/0062803883/ref=sr_1_1?crid=3LE8K4IBA4D9R&keywords=celebration+of+discipline&qid=1659392012&sprefix=celebration+of+discipline%2Caps%2C282&sr=8-1
.. |celebration_of_discipline| replace::  *Celebration of Discipline*

Reason
^^^^^^

Another way we can discern between that which is true and that which is
*almost* true is by using the rules of logic.  This is a practice that is often
overlooked these days, so training ourselves in the `fundamentals of logic`_,
and in `more advanced logical argumentation`_, can go a long way in improving
our ability to ascertain truth.  Once you have the fundamentals down, a fun way
to continue to sharpen your skills is to keep your eyes peeled for
:doc:`logical fallacies <../logical-fallacies>` throughout the day and talk
through them come dinner time.

.. _fundamentals of logic:  https://canonpress.com/products/intrologic/
.. _more advanced logical argumentation:  https://canonpress.com/products/new-intermediate-logic-complete-program-with-dvd-course/

In addition to thinking logically, we can also improve our skills in the realm
of communicating effectively, known as |rhetoric|_.  While you speaking
persuasively doesn't directly improve your ability to determine truth, knowing
how people communicate effectively, in terms of both honest and deceitful
tactics, can help you sense when something's amiss.  `Aristotle`_'s
|aristotles_rhetoric|_ is the classic text on the subject, but
|how_to_speak_how_to_listen|_, by `Mortimer J. Adler`_, is a good one as well.

.. _rhetoric:  https://en.wikipedia.org/wiki/Rhetoric
.. |rhetoric| replace::  *rhetoric*
.. _Aristotle:  https://en.wikipedia.org/wiki/Aristotle
.. _aristotles_rhetoric:  https://smile.amazon.com/Rhetoric-Dover-Thrift-Editions-Aristotle/dp/0486437930/ref=sr_1_4?crid=3KROPNSO7BJXS&keywords=rhetoric+aristotle&qid=1659394008&sprefix=rhetoric+aristotle%2Caps%2C179&sr=8-4
.. |aristotles_rhetoric| replace::  *Rhetoric*
.. _how_to_speak_how_to_listen:  https://smile.amazon.com/How-Speak-Listen-Mortimer-Adler/dp/0684846470/ref=sr_1_1?crid=2YCJRWMR1411G&keywords=how+to+speak+how+to+listen&qid=1659394100&sprefix=how+to+speak+how+to+listen%2Caps%2C181&sr=8-1
.. |how_to_speak_how_to_listen| replace::  *How to Speak, How to Listen*

Reality
^^^^^^^

A first arena under the umbrella of reality is that of empirical science, which
consists of using our senses to better understand the world around us.  We can
`improve our observational abilities`_ over time with practice.  Such skills
are foundational to the `scientific method`_, which allows us to verify whether
our current understanding of how the natural world works is correct, and adjust
our thinking if not.  Additional skills that play into the process are the
practices of `deductive and inductive reasoning`_, which come from logic,
mentioned earlier.  "Well hang on now.  Are you saying I need to switch careers
and become a scientist?"  No, but I am saying you need to be able to think
critically about what you see in the world around you.

.. _improve our observational abilities:  https://www.sciencelearn.org.nz/resources/605-observation-and-science
.. _scientific method:  https://en.wikipedia.org/wiki/Scientific_method
.. _deductive and inductive reasoning:  https://www.masterclass.com/articles/what-is-deductive-reasoning

A second arena is that of `historical science`_.  This one is often overlooked
these days, because the prevalence of naturalism has duped us into thinking the
only things we can know are those we can know "scientifically," by which is
meant "by the methods of empirical science."  Consider this question, though:
What did you have for lunch last Tuesday?  I'm afraid no amount of
observational work is going to answer that question for you.  Instead you need
`historical thinking skills`_ like examining sources, determining context,
finding corroboration, and careful reading, to name a few.  A great resource
for training you in these techniques is |cold_case_christianity|_, by `J.
Warner Wallace`_.

.. _historical science:  https://answersingenesis.org/what-is-science/historical-science-useful/
.. _historical thinking skills:  https://www.waterford.org/education/historical-thinking-skills-for-students/
.. _cold_case_christianity:  https://smile.amazon.com/Cold-Case-Christianity-Homicide-Detective-Investigates/dp/1434704696/ref=sr_1_1?keywords=cold+case+christianity&qid=1659442454&sprefix=cold+case+chri%2Caps%2C179&sr=8-1
.. |cold_case_christianity| replace::  *Cold Case Christianity*
.. _J. Warner Wallace:  https://coldcasechristianity.com/j-warner-wallace-christian-apologist-and-author/

Whether you're dealing with empirical or historical sciences, a skill set
useful to both is that of `evaluating evidence`_.  This involves evaluating the
`credibility of sources`_:  how close they are to the information they're
reporting, whether or not they're biased, how reliable they've been in the
past, etc.  Instead of simply accepting information because it comes from an
expert, you need to be able to determine the likelihood that the expert is
giving you accurate information in a particular situation.

.. _evaluating evidence:  https://courses.lumenlearning.com/olemiss-writ250/chapter/evaluating-evidence/
.. _credibility of sources:  https://www.scribbr.com/working-with-sources/credible-sources/

.. |EPH.4.11-EPH.4.16| replace:: :raw-html:`<a data-gb-link="EPH.4.11-EPH.4.16">Ephesians 4:11&ndash;16</a>`

.. raw:: html

   <script src="https://bibles.org/static/widget/v2/widget.js"></script>
   <script>
       GLOBALBIBLE.init({
           url: "https://bibles.org",
           bible: "f421fe261da7624f-01",
           autolink: false,
       });
   </script>

