Biblical Hermeneutics
---------------------

.. role:: raw-html(raw)
   :format: html

:raw-html:`<div align="right">🕑 42 min.</div>`

We've established elsewhere that the Bible is a :doc:`reliable source of truth
<worldview-analysis/the-bible>`, but believers and unbelievers alike often wind
up arguing about what it says.  It's not sufficient for us to simply point
someone to a passage of scripture, as they could easily retort, "I don't think
that's what it means."  If two people are looking at the same text and
disagreeing about what's meant by it, what are we to do?  This is where the
practice of *biblical hermeneutics* comes into play.

|hermeneutics|_ is just a fancy word for the practice of interpreting certain
types of literature.  Indeed it could be considered a subset of `literary
criticism <https://en.wikipedia.org/wiki/Literary_criticism>`_, so whatever you
already know when it comes to understanding literature likely comes into play
here as well (see, e.g., |how_to_read_a_book|_, by Mortimer J. Adler and
Charles Van Doren, or |how_to_read_the_infallible_word|_, by Rebekah Merkle).
*Biblical* hermeneutics, then, is the process as applied to scripture.

.. _hermeneutics:  https://en.wikipedia.org/wiki/Hermeneutics
.. |hermeneutics| replace::  *Hermeneutics*
.. _how_to_read_a_book:  https://smile.amazon.com/How-Read-Book-Classic-Intelligent/dp/0671212095/ref=sr_1_1?dchild=1&keywords=how+to+read+a+book&sr=8-1
.. |how_to_read_a_book| replace::  *How to Read a Book*
.. _how_to_read_the_infallible_word:  https://www.youtube.com/watch?v=zY3A5LiW5bk
.. |how_to_read_the_infallible_word| replace::  *How to Read the Infallible Word*

At a high level, the practice can be broken down into two different
disciplines:  |exegesis|_ and *contextualization*.  The first answers the
question, "What does the text actually say?" while the second deals with, "What
does that mean to us today?"  Let's dive into these in more detail.

.. _exegesis:  https://en.wikipedia.org/wiki/Exegesis
.. |exegesis| replace::  *exegesis*

.. contents::  Contents
   :local:

Exegesis
========

Does this scenario sound familiar?  You gather together with a handful of
believers in some sort of small group setting, you crack open the Bible and
read a passage of scripture together, and then someone (often the group leader)
asks the question:  "What does this mean to you?"  What's wrong with this
picture?

The problem is we're asking the wrong question; or rather, to be more accurate,
we're asking a fine question, but asking it at the wrong time.  If we start
with the question, "What does this mean to you?", we're placing the interpreter
in the position of being both the source and ultimate arbiter of truth.  If we
do that, there's not really much sense in attempting to analyze the text,
because your interpretation will ultimately be based on an unstable and
shifting foundation (you, your emotions, whatever thoughts you deem fashionable
at the time, etc.).  Instead we must first start with the question, "What does
the text actually say?"

This is what the discipline of exegesis is all about.  Before you determine
what a passage means *to you*, you first need to figure out what it means.  But
how do you go about doing that?  Generally the process breaks down into two
phases:  studying the overall context of the passage, and then analyzing the
text of the passage itself.

The Surrounding Context
^^^^^^^^^^^^^^^^^^^^^^^

When jumping into the context for a passage of scripture, there are actually
two different types of contexts to consider:  the literary context and the
historical context.

Literary Context
~~~~~~~~~~~~~~~~

The literary context deals with how the passage you're examining fits in with
the overall work of literature it's contained in.  There are two primary
questions to ask in this arena.

What literary genre does the book fit into?
+++++++++++++++++++++++++++++++++++++++++++

Literary works can be categorized into a variety of different genres, depending
on their style, form, content, etc.  Different genres of literature are meant
to be read and interpreted differently.  For instance, you'll read a work of
fiction through different lenses than you would a piece of non-fiction.
Depending on who you're talking to, they might have different ways to
categorize a piece, and it is possible for a piece to span multiple categories,
but some standard literary genres are the following:

.. raw:: html

   <div style="display: flex;">
     <div style="flex: 33%;">
       <ul>
         <li>Academic</li>
         <li>Adventure</li>
         <li>Biography</li>
         <li>Comedy</li>
         <li>Epic</li>
         <li>Essay</li>
         <li>Fantasy</li>
       </ul>
     </div>
     <div style="flex: 33%;">
       <ul>
         <li>Folklore</li>
         <li>History</li>
         <li>Horror</li>
         <li>Journalism</li>
         <li>Mystery</li>
         <li>Narrative</li>
         <li>Philosophy</li>
       </ul>
     </div>
     <div style="flex: 33%;">
       <ul>
         <li>Play</li>
         <li>Poetry</li>
         <li>Reference</li>
         <li>Religious</li>
         <li>Romance</li>
         <li>Satire</li>
         <li>Science fiction</li>
       </ul>
     </div>
   </div>

In addition to these commonly recognized genres, there are also some others
that are `specific to scripture
<https://www.gotquestions.org/Bible-genres.html>`_:

* Epistle
* Law
* Prophecy
* Wisdom

Why does all this matter?  Because if you fail to keep a book's genre(s) in
mind, you can very easily misinterpret what's said in it.

Take, for instance, the book of Genesis.  What's in it?  It starts with
creation and Adam and Eve, then you've got the fall, Cain and Abel, Noah and
the flood, the tower of Babel, and the stories of Abraham, Isaac, and Jacob,
and then it ends with Joseph and the move of Israel and his family to Egypt.
With that lightning-fast recap in mind, what genre(s) does Genesis fit into?
For one, it's historical, in that it's ultimately a history of God's chosen
people.  On top of that, it's true; that is, it's not put forth as historical
fiction, along the lines of something you might pick up on a Barnes and Noble
new best-sellers shelf.  Additionally, it's narrative, in that it's telling the
stories of a handful of main (actual, historical) characters.

What's the significance in all of this?  These days it's common enough for
people in the church to think that the genre of Genesis shifts at the boundary
between chapters 11 and 12.  They'll contend that the first 11 chapters,
containing creation through the tower of Babel, fall into the genres of myth or
allegory, meaning they contain stories that help us to understand certain
truths about the way the world works, but they aren't actually true.  And then
suddenly the genre shifts and chapters 12 through the end of the book are true
historical narrative.  However, there is nothing whatsoever in the text to
indicate a genre shift.  To the extent that people hold to this genre shift,
they're letting worldview assumptions (e.g., regarding the age of the earth,
the development of languages, etc.) override what the text actually indicates.
If we don't understand the genre, we can very easily manipulate the text to
support what we already believe, which is |eisegesis|_, the opposite of
exegesis.

.. _eisegesis:  https://en.wikipedia.org/wiki/Eisegesis
.. |eisegesis| replace::  *eisegesis*

How does the passage fit into the larger literary context?
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Once you have the genre figured out, the next bit to determine is how the
passage you're looking at fits into the larger literary context.  We have a bad
habit of treating the Bible like it's a big collection of random verses that
just happened to be collected into a particular order.  If we think about it,
though, we know that doesn't make sense.  It's a collection of books, written
by particular authors, and each book was written in a particular order, and for
a particular purpose.  Oh, and by the way, the whole process of writing and
compiling scripture was inspired and superintended by the all-powerful creator
and sustainer of the universe.  With all that in mind, we need to understand
the overall structure to ensure we're not misinterpreting a passage by taking
it out of context.

So what is that literary context, then?  If we start with the overall context
of the Bible as a whole, we can break it down as follows:

* Old Testament

  * The Pentateuch:  Genesis through Deuteronomy
  * Historical Books:  Joshua through Esther
  * Wisdom Literature:  Job through the Song of Solomon
  * The Prophets:  Isaiah through Malachi

* New Testament

  * The Gospels:  Matthew through John
  * Historical Books:  Acts
  * Epistles:  Romans through Jude
  * Apocalyptic Literature:  Revelation

.. sidebar::  Recommended Study Bibles

   * `NASB Study Bible`_
   * `NIV Life Application Study Bible`_
   * `NIV Archaeological Study Bible`_
   * `CSB Apologetics Study Bible`_

.. _NASB Study Bible:  https://www.amazon.com/NASB-Zondervan-Study-Bible/dp/0310910927/ref=sr_1_7?keywords=nasb+study+bible&sr=8-7
.. _NIV Life Application Study Bible:  https://www.amazon.com/Application-Study-Bonded-Leather-Letter/dp/0310452775/ref=sr_1_1_sspa?keywords=niv+life+application+study+bible&sr=8-1-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUExUVdWTzVZMEhQNDhWJmVuY3J5cHRlZElkPUEwOTQ4MzAyMVZJTkhCV0xTU1RETiZlbmNyeXB0ZWRBZElkPUEwNjI0NDY1MlI3NElYWUhFT1cxSiZ3aWRnZXROYW1lPXNwX2F0ZiZhY3Rpb249Y2xpY2tSZWRpcmVjdCZkb05vdExvZ0NsaWNrPXRydWU=
.. _NIV Archaeological Study Bible:  https://www.amazon.com/NIV-Archaeological-Study-Bible-Hardcover/dp/031092605X/ref=sr_1_1?keywords=niv+archaeological+study+bible&sr=8-1
.. _CSB Apologetics Study Bible:  https://www.amazon.com/CSB-Apologetics-Study-Bible-Hardcover/dp/1433644096/ref=sr_1_1?keywords=csb+apologetics+study+bible&sr=8-1

You could further subdivide things if you wanted---e.g., major vs minor
prophets, or Paul's letters vs those from Peter or John, etc.---but this is
sufficient for a high-level understanding.  So given the passage you're
examining, once you've figured out where the book fits in the structure of the
Bible as a whole, the next step is to figure out where the passage fits in the
structure of the book itself.  It'll be worthwhile to have a study Bible on
hand, as the introductory material at the beggining of each book will often
include an outline to help you grasp the high-level organization of the book.

Let's return to our example of Genesis earlier.  Say the passage in question is
the creation narrative contained in the first three chapters.  Genesis is found
in the Pentateuch, which contains the history of God's chosen people, along
with the law he established for them, and it forms the theological foundation
for absolutely everything that follows.  The book itself is `subdivided into 10
sections`_, each of which begins with "These are the
generations of [insert dude here]".  Your particular translation may render the
word "generations" as "descendants" or perhaps "records", but the point is that
Genesis is subdivided into sections, each of which records the accurate history
of a particular person and those begotten from him.  The one exception to this
pattern is the very first section:

.. _subdivided into 10 sections:  https://www.esv.org/resources/esv-global-study-bible/chart-01-01/

.. raw:: html

   <div style="border: 2px solid ForestGreen;
               border-radius: 5px;
               padding: 10px;
               background-color: HoneyDew;">
       <div data-gb-widget="passage" data-passage="GEN.2.4">
       </div>
   </div>
   <br>

Genesis begins with a prelude, and then in 2:4 the structure of the book kicks
into gear with the accurate historical records of the heavens and the earth,
and all that proceeds from them.  Earlier we saw that interpreting this passage
as mythology is inconsistent with the book's genre, but now to that we can add
that such an interpretation would be incompatible with the overall literary
context of the book as well.

Historical Context
~~~~~~~~~~~~~~~~~~

Now that we've talked through the literary context, we can move on to the
historical context.  Have you ever read an old book---say Homer's *Iliad*, or
Shakespeare's *Much Ado About Nothing*---and had a hard time understanding
everything that was going on because the setting is so far removed from our
own?  The same thing can happen when reading scripture, because the Bible was
written over a period of about 1,500 years and was finished almost 2,000 years
ago.  Safe to say the historical context is slightly different from 21st
century America.  But what's the difference?  To answer that question, there
are three primary questions you'll need to ask and answer.

Who wrote the book?
+++++++++++++++++++

First up:  books don't simply appear out of thin air---they have to be written
by someone.  Now on the one hand, all scripture is God-breathed (|2TI.3.16|),
but when God, through the Holy Spirit, inspired the authors of the books of
scripture to write, it's not like he completely took over and they became
robots devoid of all personality.  They were still the same people, with their
own particular backstories, skills, passions, callings, etc.  If we're going to
accurately understand the words they put to the page, that's going to involve
getting to know them a bit.

.. |2TI.3.16| replace:: :raw-html:`<a data-gb-link="2TI.3.16">2 Timothy 3:16</a>`

Again, it'll be worthwhile to have a good study Bible handy, as they often have
author profiles included at the start of each book, but that'll just be a
starting point.  You'll also want to do some good, old-fashioned :doc:`research
<conducting-research>`.  Here are some questions you'll want to dig into:

* Who wrote the book?
* Where did he live?
* What was his background?
* What was going on in the world during his lifetime and while he was writing?
* What was the nature of his ministry?
* Why was he writing?

Let's return to our Genesis example to try to answer some of these questions,
and see if they lend additional insight to our understanding of the first 11
chapters.

The first five books of the Bible were written by Moses, who was a Hebrew
living in ancient Egypt many generations after Israel and his family settled
there (in the 1400s BC).  While he spent his first few years being
nursed/raised by his mother at the behest of Pharaoh's daughter
(|EXO.2.7-EXO.2.10|), he ultimately grew up as Egyptian royalty.  As such, he
was both highly educated and intimately familiar with the intricacies of
ancient Egyptian mythology and religion, since the pharaohs were thought to be
gods (or descended from them), and Moses had been adopted into that family.
Now what bearing does any of this have on our interpretation of Genesis,
though?

.. |EXO.2.7-EXO.2.10| replace:: :raw-html:`<a data-gb-link="EXO.2.7-EXO.2.10">Exodus 2:7&ndash;10</a>`

Recall the common claim that Genesis 1--11, and particularly the first three
chapters, are myth and not true historical narrative.  Does such a claim make
sense in light of what we know of the author?  `Ancient Egyptian mythology`_ is
quite involved, and shares a good deal in common with the mythologies of other
ancient civilizations that were around at the time or would pop up in the
coming centuries and millennia:  a pantheon of gods; gods who tend to be
personifications and exaggerations of particular human character traits; gods
who are essentially super-human, but susceptible to all the same character
flaws and failures we are; mystical transformations of gods into various forms
(human, animal, and inanimate); etc.  If Moses wanted to sit down and write
a mythology for his new people as they were setting out on their own after
leaving Egypt, he had plenty of experience and source material to draw from.

.. _Ancient Egyptian mythology:  https://www.youtube.com/watch?v=uZe49S1Q8b8

In contrast, however, what we have in the opening chapters of Genesis is
distinctly unlike any mythology Moses would have been familiar with.  Instead
of a plurality of gods, you have one God.  Instead of gods who somehow come
from creation, you have God creating everything out of nothing.  Instead of
gods who are basically a bunch of fickle, warring, backbiting superheroes, you
have God who is perfect in his wisdom, justice, goodness, truth, etc.  I have
no doubt that Moses could've cooked up a killer mythology, if he'd wanted to,
but what we have in Genesis 1--11 is simply not that.

Who was the original intended audience?
+++++++++++++++++++++++++++++++++++++++

Something else we need to keep in mind is authors often have a particular
audience in mind when they sit down to write.  It's important for authors to
consider their audiences, because they want their words to make sense to them.
For instance, if I'm writing an introduction to quantum mechanics, writing that
for high school students is going to be very different from writing it for
graduate students in physics.  Similarly, if I sit down to write a book of
parental advice for my children, that'll necessarily look different from a
similar book directed at random people I don't know who happen to be growing
into adulthood.  And if you, as an aspiring public speaker, are writing a
speech for a wide audience (friends, family, neighbors, members of your church
congregation, etc.), that won't be the same as writing a speech on the same
topic to deliver only to your peers.

If we want to make sure we don't accidentally misunderstand what an author has
written, that'll involve a bit of getting to know the original audience.  Here
are some questions to keep in mind:

* Who was the original audience?
* What do we know about them?
* What was going on in their lives at the time of writing?
* What kind of relationship did the author have with them?

Let's return---for the last time; I promise we'll move on soon---to the example
of Genesis to see what additional insight this examination can give.  The
Pentateuch was written sometime during the 40 years of wandering in the
wilderness, and the original audience was the people of Israel after they'd
left Egypt.  Way back in their history, they were familiar with the customs and
religions of the other ancient Near Eastern peoples, and often participated in
them (see, e.g., Rachel's pilfering of the household gods/idols in
|GEN.31.22-GEN.31.35|).  More recently, as the extended family of Jacob and his
12 sons grew into the nation of Israel, it did so immersed in the culture of
ancient Egypt `for 200+ years`_.  Why does this matter?

.. |GEN.31.22-GEN.31.35| replace:: :raw-html:`<a data-gb-link="GEN.31.22-GEN.31.35">Genesis 31:22&ndash;35</a>`
.. _for 200+ years: https://answersingenesis.org/bible-questions/how-long-were-the-israelites-in-egypt/

If Genesis 1--11 was meant to be understood as mythology, it would've needed to
be understood as such by the original audience.  The Israelites pre-exodus were
strongly influenced by the mythology of ancient Egypt.  Indeed, the ten plagues
that God brought against the Egyptians were intended in part `to demonstrate
God's superiority over the Egyptian gods`_, not just to Pharaoh and the
Egyptians, but also to God's chosen people, Israel.  They later demonstrate
just how much staying power their Egyptian upbringing had when, after Moses
spends too long chilling with God on Mount Sinai, they say to Aaron, "Dude, you
better make us some gods to go before us," and he's like, "Yeah, this God who's
just demonstrated his power over the whole host of Egypt and the Red Sea---he's
totally a baby cow" (|EXO.32.1-EXO.32.6|).  I'm afraid the first 11 chapters of
Genesis are distinctly unlike any mythology the original audience was familiar
with.  They would've understood it as true, historical narrative, which is what
the author intended it to be, which matches both the overall literary context
and genre of the rest of the book.

.. _to demonstrate God's superiority over the Egyptian gods: https://www.gotquestions.org/ten-plagues-Egypt.html
.. |EXO.32.1-EXO.32.6| replace:: :raw-html:`<a data-gb-link="EXO.32.1-EXO.32.6">Exodus 32:1&ndash;6</a>`

.. admonition::  Multiple Audiences

   Before we move on, it's worth noting that sometimes works of literature will
   have multiple intended audiences, and this is true with scripture.  You
   have, at the very least, both the original biblical audience, and then you
   have people today.  Sometimes there are other audiences in between (e.g., a
   letter was written to a particular church in a certain city, but then was
   intended to be circulated throughout the churches around the world).  In
   such cases, it's possible that the author intended the work to mean one
   thing to one audience, and another thing to another audience, but we need to
   be careful here.  It's completely legitimate for the original audience to
   understand something specific, and a later, wider audience to understand
   something general.  To the Jewish exiles in Babylon, |JER.29.11| is God
   telling them, "Look, I know this has been near-unbearable, but the
   punishment I brought against you because of your transgression of our
   covenant is almost over, and when it is, I have plans to bring you back to
   the land I promised your father Abraham, to build you back as my people, to
   rebuild my city and my temple, etc."  To the Christian graduating from high
   school today, it can be understood as saying more generally that God is in
   control---not that God promises no harm will befall you and you will live
   long and prosper---but note that these two interpretations are congruent.
   What you can't have is one meaning for one audience, another meeting for
   another audience, and the two understandings being mutually incompatible
   with one another.  That is, if you try to argue that the original audience
   of Genesis understood it as true, historical narrative, but we today are to
   understand it as myth and allegory, that simply doesn't make sense.

.. |JER.29.11| replace:: :raw-html:`<a data-gb-link="JER.29.11">Jeremiah 29:11</a>`

What other historical or cultural elements are significant?
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

As we wrap things up now for the historical context of a book, we need to look
beyond the author and original audience to the broader historical situation at
the time.  It's far too easy for us to forget that the books of the Bible were
written a long, long time ago, in a land far, far away.  To improve your
understanding of the disconnect, here are some questions to consider:

* When do the events take place?
* Where were the main characters?
* Are there any particular aspects of the geography or topography that might
  have bearing on the situation?
* What was the culture like?
* How were the social or religious customs of the time different from our own?
* What were the people like?
* What was the economic situation at the time?
* What political realities were in play?

To illustrate how ignorance of such historical cultural elements can lead to
misunderstandings of the text, let's now leave the opening chapters of Genesis
behind and turn instead to the account of the birth of Jesus contained in
|LUK.2.1-LUK.2.20|.  You're familiar with the story, right?  Joseph and Mary
roll into Bethlehem late at night in the cold of winter.  She's so pregnant
she's about to pop at any moment, but all the hotels in town have "No vacancy"
signs up.  They make their way to the local Holiday Inn Express & Suites, and
the concierge says, "Sorry boss, we're all full up.  But tell you what, I can
see that your wife's already in labor here, so there's a stable out back where
we keep all the camels parked, and that'll at least keep y'all out of the cold.
I'd offer you the lobby here, but you know, health and safety regs
¯\\_(ツ)_/¯."  So they go make friends with a camel, cow, sheep, and
chicken, and those are the only witnesses of the actual birth of Christ.
Slightly exaggerated for dramatic effect, but you get the idea.  What's wrong
with this picture?

.. |LUK.2.1-LUK.2.20| replace:: :raw-html:`<a data-gb-link="LUK.2.1-LUK.2.20">Luke 2:1&ndash;20</a>`

For one, we need to remember that Bethlehem was essentially podunk Judah.  This
was no thriving metropolis---it didn't have anything like a hotel.  We get
confused by the word that's translated "inn" in this passage, but that's the
same word used for the "upper room" or "guest room" where Jesus and his
disciples ate the last supper (|LUK.22.7-LUK.22.23|).  Dwellings in this part
of the world at this time (and even today, to a lesser extent) were generally
single-room homes, which makes the whole "put a light on a stand so it gives
light to the whole house" thing make sense (|MAT.5.15|).  A well-off family,
though, might have another room attached to the house for visiting family.
This guest room was raised a step or two from the main room, such that the dirt
and grime from the main room wouldn't infiltrate the upper room where the
honored guests would stay.  Where does all the dirt and grime come from?

.. |LUK.22.7-LUK.22.23| replace:: :raw-html:`<a data-gb-link="LUK.22.7-LUK.22.23">Luke 22:7&ndash;23</a>`
.. |MAT.5.15| replace:: :raw-html:`<a data-gb-link="MAT.5.15">Matthew 5:15</a>`

Well the main room of the house is actually elevated off the ground, though
there's a portion at ground level, where the entrance is, which is where you
keep the livestock.  You bring them inside at night, not just to keep them safe
from harm, but also to heat the house, and you let them out again first thing
in the morning.  This is why it makes sense that Jephthah vows to sacrifice the
first thing to come out of his house on his return from battle
(|JDG.11.29-JDG.11.40|).  He was completely convinced that whatever came out of
his house first would be one of the livestock; under no circumstances did he
expect it to be his daughter, or anyone else in his family.  While the animals
stayed in this lower part of the main room overnight and during the heat of the
day, their feed troughs were actually dug into the floor of the elevated main
room, and this is where the new baby was placed.

.. |JDG.11.29-JDG.11.40| replace:: :raw-html:`<a data-gb-link="JDG.11.29-JDG.11.40">Judges 11:29&ndash;40</a>`

Another common version of the nativity story has Mary and Joseph staying in a
cave, because there was no room for them anywhere.  But does this make sense?
We need to remember that though Bethlehem was a small, out-of-the-way place, it
was still an important one because it was "the city of David," Israel's
greatest king.  And not only that, but Joseph was a descendent of David,
meaning he was of royal blood, returning to his hometown.  This concept of
lineage and of family ties may not mean much to us in 21st century America, but
2,000 years ago in the Middle East, it meant a great deal.  Joseph, therefore,
would have had a number of different extended family members he could have
stayed with.  It would have been the height of dishonor to the people of
Bethlehem to leave him out either in some cave or in somebody's barn.  But then
if he was such an honored guest, why was there no room for them in the inn?
Well with the census taking place, the town was full of people coming back
home, and the family Joseph and Mary stayed with already had their guest room
full of relatives.  It would've been dishonorable for the hosts to kick the
guests out of the upper room for the sake of Mary and Joseph, so they just
hunkered down in the main room with the rest of the family.

And this brings up another point.  The nativity is always portrayed as Joseph,
Mary, and Jesus, some shepherds, wise men, and angels, and that's it.  Nobody
else is around for the birth, but this also makes no sense.  For one thing, the
house they're staying at is packed, so there's plenty of relatives around.  For
another, we need to understand what a big deal a birth was in the first century
in the Middle East, or really in any Middle or Near Eastern culture even today.
There would have been plenty of ladies around, tending to Mary's needs.  Joseph
wouldn't have been nearby for the birth itself, as that would've been
inappropriate, but he would've been outside with the other men of the house and
others in the town, eagerly awaiting the birth of his first son.  He wouldn't
have passed out cigars, but it would've been quite the celebration when the
birth was signalled by `the ladies' loud trilling of the tongue`_.  It's a
beautiful song, but *Silent Night* is quite the misnomer.

.. _the ladies' loud trilling of the tongue:  https://www.youtube.com/watch?v=xxr0moPYxng

We could keep digging through our modern misconceptions of the nativity story
until the camels come home, but for that, I'll point you to the first chapter
of |jesus_through_middle_eastern_eyes|_, by Ken Bailey.  The rest of that book,
along with the author's |paul_through_mediterranean_eyes|_, is quite helpful
when it comes to giving you a better understanding of the historical culture in
which the events of the New Testament take place.  Luckily our
misunderstandings in this instance haven't led to any serious doctrinal errors,
but the birth of Jesus is a good example to illustrate how easy it is for us to
take our modern context and read it into the text, instead of making sure we
understand the historical cultural context, such that we understand the text as
well as possible.

.. _jesus_through_middle_eastern_eyes:  https://www.amazon.com/Jesus-Through-Middle-Eastern-Eyes/dp/0281059756/ref=tmm_pap_swatch_0?_encoding=UTF8&sr=8-1
.. |jesus_through_middle_eastern_eyes| replace::  *Jesus Through Middle Eastern Eyes*
.. _paul_through_mediterranean_eyes:  https://www.amazon.com/Paul-Through-Mediterranean-Eyes-Corinthians/dp/0830839348/ref=pd_bxgy_sccl_1/133-8095979-5611462?content-id=amzn1.sym.26a5c67f-1a30-486b-bb90-b523ad38d5a0&pd_rd_i=0830839348&psc=1
.. |paul_through_mediterranean_eyes| replace::  *Paul Through Mediterranean Eyes*

The Text Itself
^^^^^^^^^^^^^^^

Now that you're familiar with how to study the overall context of a passage,
let's move on to analyzing the actual text under consideration.  The verse I
have in mind is |MAT.7.1|, because it often comes up in conversations on ethics
(right and wrong) with believers and unbelievers alike.  The scenario is
usually something like this:

.. |MAT.7.1| replace:: :raw-html:`<a data-gb-link="MAT.7.1">Matthew 7:1</a>`

Well-intentioned believer, attempting to correct a perceived error:
    I don't think you should be thinking/saying/doing this.  [Insert reasons
    here.]

Other individual:
    You can't tell me what to think/say/do!  Jesus said so:  "Judge not, lest
    ye be judged"!

Well-intentioned believer, quoting |inigo_montoya_from_the_princess_bride|_:
    "You keep using that [phrase].  I do not think it means what you think it
    means."

.. _inigo_montoya_from_the_princess_bride:  https://www.youtube.com/watch?v=dTRKCXC0JFg
.. |inigo_montoya_from_the_princess_bride| replace::  Iñigo Montoya from *The Princess Bride*

For the rest of our study here, I'd like to dive into assessing the accuracy of
this common interpretation of this verse.

Before we jump right in, though, it'd be worthwhile to have a broad
understanding of Matthew's gospel as a whole, so if you have time, listen to
the following reading of it:

.. raw:: html

    <div style="position: relative;
                padding-bottom: 56.25%;
                height: 0;
                overflow: hidden;
                max-width: 100%;
                height: auto;">
        <iframe
         src="https://www.youtube.com/embed/y1idCq1wQs4"
         frameborder="0"
         allowfullscreen
         style="position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                padding: 10px;">
        </iframe>
    </div>

If you appreciate this speaker's reading of the text, he has `a complete audio
Bible`_ available as well.

.. _a complete audio Bible:  https://www.amazon.com/Complete-NIV-Audio-Bible-International/dp/1444786407/ref=sr_1_2?keywords=david+suchet+audio+bible&sr=8-2

The Divisions of a Text
~~~~~~~~~~~~~~~~~~~~~~~

.. sidebar:: Layers of a Text

   +-------------+
   | Book        |
   +-------------+
   | Part        |
   +-------------+
   | Chapter     |
   +-------------+
   | Section     |
   +-------------+
   | Sub-section |
   +-------------+
   | ...         |
   +-------------+
   | Paragraph   |
   +-------------+
   | Sentence    |
   +-------------+
   | Phrase      |
   +-------------+
   | Word        |
   +-------------+
   | Letter      |
   +-------------+

Rather than jump right into analyzing the verse in question, let's start with a
brief discussion of the natural divisions of the written word.  Letters are
grouped together to form words, words are collected into phrases, and phrases
make up sentences, which are then collected into paragraphs.  Multiple
paragraphs constitute a section (actually there can be many layers of nested
sections), sections may then be grouped into chapters, which may be grouped
into parts, and then the outermost division is the book itself.  Analyzing
something in a text means appropriately analyzing it in the various layers in
which it's found.

Now all of this pertains to the written word in general, but a particular text
may not have all these subdivisions.  For instance, though you're used to
reading your Bible with chapter and verse markings, these weren't in the
original manuscripts.  Rather, they were `added later`_ to assist readers in
referring to particular passages of scripture.  Indeed, it's even the case that
sentences, as we think of them, aren't present in scripture, because the
original languages in which it was recorded `don't use punctuation`_.  That's a
bit of a rabbit hole that we don't have time to dive into at the moment, so for
our purposes we'll focus on analyzing the text at the section, paragraph, and
sentence level.

.. _added later:  https://bible.org/question/how-and-when-was-bible-divided-chapters-and-verses
.. _don't use punctuation:  https://www.christianity.com/wiki/bible/how-was-biblical-punctuation-decided.html

Sections
~~~~~~~~

Since you've been able to successfully read this far, I'm assuming you already
know how to tell sentences and paragraphs apart.  Discovering where sections
begin and end, though, is a bit more difficult, because the authors of
scripture didn't include any kind of headings in the text to indicate that
higher-level organization.

.. note::

   Your Bible probably has such headings in place, but I'm going to encourage
   you to ignore those for now, because developing them on your own will aid in
   your understanding of the text.

Our verse in question is |MAT.7.1|, which falls in the midst of Jesus' Sermon
on the Mount, found in |MAT.5.1-MAT.7.29|.  This is the outermost section we'll
be analyzing through the rest of our study here.  Read straight through it, and
figure out how to subdivide it into smaller sections.  You should be able to
produce an outline along these lines (if yours looks a little different, that's
totally fine):

.. |MAT.5.1-MAT.7.29| replace:: :raw-html:`<a data-gb-link="MAT.5.1-MAT.7.29">Matthew 5&ndash;7</a>`

.. table::

   +---------+----------+---------------------------------------------+
   | Section | Verses   | Subject                                     |
   +=========+==========+=============================================+
   | 1       | 5:1--12  | Blessings for various people groups         |
   +---------+----------+---------------------------------------------+
   | 2       | 5:13--16 | You are salt & light                        |
   +---------+----------+---------------------------------------------+
   | 3       | 5:17--20 | The Law still applies                       |
   +---------+----------+---------------------------------------------+
   | 4       | 5:21--26 | Maintaining right relationships             |
   +---------+----------+---------------------------------------------+
   | 5       | 5:27--32 | Right relationships in marriage             |
   +---------+----------+---------------------------------------------+
   | 6       | 5:33--37 | True speech required                        |
   +---------+----------+---------------------------------------------+
   | 7       | 5:38--48 | Repay evil with good                        |
   +---------+----------+---------------------------------------------+
   | 8       | 6:1--8   | Your piety shouldn't be for show            |
   +---------+----------+---------------------------------------------+
   | 9       | 6:9--15  | A model prayer                              |
   +---------+----------+---------------------------------------------+
   | 10      | 6:16--21 | Seek heavenly treasure, not worldly acclaim |
   +---------+----------+---------------------------------------------+
   | 11      | 6:22--24 | Good and evil are mutually exclusive        |
   +---------+----------+---------------------------------------------+
   | 12      | 6:25--34 | Don't worry; trust God                      |
   +---------+----------+---------------------------------------------+
   | 13      | 7:1--6   | Don't be hypocritical                       |
   +---------+----------+---------------------------------------------+
   | 14      | 7:7--12  | God gives good things to those who ask      |
   +---------+----------+---------------------------------------------+
   | 15      | 7:13--23 | False believers plentiful                   |
   +---------+----------+---------------------------------------------+
   | 16      | 7:24--29 | Following is wise; disregarding is foolish  |
   +---------+----------+---------------------------------------------+

Once you have your outline in place, read back over the three chapters again,
taking note of any of the following:

* Connections between paragraphs or sections
* Major shifts in the subject or narrative
* Bouncing back and forth between topics
* Distinctive literary structures (e.g., `bracketing`_, `chiasm`_)

.. _bracketing:  https://en.wikipedia.org/wiki/Inclusio
.. _chiasm:  https://www.gotquestions.org/chiasm-chiastic.html

After this second pass, you should be able to add a fourth column to your
outline to indicate the overall organization of the passage:

.. table::
   :width: 100
   :widths: 15 15 40 30

   +---------+----------+---------------------------------------------+-------------------------------------------+
   | Section | Verses   | Subject                                     | Organization                              |
   +=========+==========+=============================================+===========================================+
   | 1       | 5:1--12  | Blessings for various people groups         | Introduction                              |
   +---------+----------+---------------------------------------------+                                           +
   | 2       | 5:13--16 | You are salt & light                        |                                           |
   +---------+----------+---------------------------------------------+                                           +
   | 3       | 5:17--20 | The Law still applies                       |                                           |
   +---------+----------+---------------------------------------------+-------------------------------------------+
   | 4       | 5:21--26 | Maintaining right relationships             | Point 1:  Right relationships with others |
   +---------+----------+---------------------------------------------+                                           +
   | 5       | 5:27--32 | Right relationships in marriage             |                                           |
   +---------+----------+---------------------------------------------+                                           +
   | 6       | 5:33--37 | True speech required                        |                                           |
   +---------+----------+---------------------------------------------+                                           +
   | 7       | 5:38--48 | Repay evil with good                        |                                           |
   +---------+----------+---------------------------------------------+-------------------------------------------+
   | 8       | 6:1--8   | Your piety shouldn't be for show            | Point 2:  Right relationship with God     |
   +---------+----------+---------------------------------------------+                                           +
   | 9       | 6:9--15  | A model prayer                              |                                           |
   +---------+----------+---------------------------------------------+                                           +
   | 10      | 6:16--21 | Seek heavenly treasure, not worldly acclaim |                                           |
   +---------+----------+---------------------------------------------+                                           +
   | 11      | 6:22--24 | Good and evil are mutually exclusive        |                                           |
   +---------+----------+---------------------------------------------+                                           +
   | 12      | 6:25--34 | Don't worry; trust God                      |                                           |
   +---------+----------+---------------------------------------------+-------------------------------------------+
   | 13      | 7:1--6   | Don't be hypocritical                       | Point 3:  Right relationships with both   |
   +---------+----------+---------------------------------------------+                                           +
   | 14      | 7:7--12  | God gives good things to those who ask      |                                           |
   +---------+----------+---------------------------------------------+-------------------------------------------+
   | 15      | 7:13--23 | False believers plentiful                   | Conclusion                                |
   +---------+----------+---------------------------------------------+                                           +
   | 16      | 7:24--29 | Following is wise; disregarding is foolish  |                                           |
   +---------+----------+---------------------------------------------+-------------------------------------------+

Note that after an introduction, Jesus first talks for a bit about you
maintaining right relationships with others, and then goes on to talk about you
maintaining a right relationship with God.  After that, his third point is kind
of revisiting the first two points, but treating them together rather than
separately, and then he wraps things up with a conclusion.

Paragraphs
~~~~~~~~~~

Now that you have a good picture of the 30,000-foot view, it's time to dive a
little deeper and analyze the text at the paragraph level.  As you study each
paragraph, take note when you come across any of the following:

* General vs specific statements
* Questions posed and answers provided
* Purpose/result statements
* Means by which something is accomplished
* Conditional clauses (if, unless, when)
* The actions/roles of people and the actions/roles of God
* Emotional terms
* Tone (e.g., sarcastic, mad, etc.)
* Dialogue (who says what)

At this point, you'll want to start marking up your Bible.  You may want to
grab a copy that's just for note-taking, like the `Inductive Study Bible`_ or
the `Note-taking Bible`_.  Alternatively, you could grab the text from
somewhere like `BibleGateway`_, paste it into a document, and edit the
formatting to give yourself sufficient space between lines and in the margins.
The advantage to taking notes in your actual Bible, though, is that your notes
come with you any time you have your Bible on-hand.

.. _Inductive Study Bible:  https://www.amazon.com/New-Inductive-Study-Bible-NASB/dp/0736928014/ref=sr_1_1?keywords=inductive+study+bible&sr=8-1
.. _Note-taking Bible:  https://www.amazon.com/Holy-Bible-Note-taking-Leather-Hardcover/dp/1642726613/ref=sr_1_3?keywords=note-taking+bible&sr=8-3
.. _BibleGateway:  https://www.biblegateway.com/

Additionally, you'll want to grab a set of multi-colored pens, pencils,
highlighters, and maybe even crayons.  Then you'll want to grab a sheet of
paper to create your own legend for how you're going to mark up your Bible.
For instance, say for this paragraph-level analysis you decide to use pens,
then choose a different color for each thing you should be on the lookout for
(e.g., red for general vs specific, orange for Q&A, yellow for purpose/result,
etc.).  The scheme you choose is completely up to you, but the important thing
is that you be consistent with it, because as you continue to use your system,
you'll get used to it, and be able to readily identify what something is on the
page at a glance by recognizing the color and instrument used to mark it up.

The one exception to this rule is dialogue, because it tends to bounce back and
forth between speakers.  If everything is marked one color, that's not as
helpful as it could be.  Instead, choose a different utensil (e.g.,
highlighter), and then within a particular passage, pick a different color for
each speaker.  That way you can see at a glance who says what.

Now you'll want to go back and re-read |MAT.5.1-MAT.7.29| , this time going
paragraph by paragraph, and marking it up according to the legend you've
created for yourself.

.. note::

   You'll see a picture of what this looks like down below.

Sentences
~~~~~~~~~

After you've completed your paragraph-level analysis, it's time to get down
into the sentences.  As you study each sentence, take note when you come across
any of the following:

* Repeated words (or very similar ones)
* Conjunctions (and, but, for, so, etc.)
* Verbs
* Pronouns
* Comparisons
* Contrasts
* Cause and effect
* Figures of speech
* Lists

Much of this is just to ensure you're correctly understanding the language of
the text itself.  For instance, what noun does the pronoun refer to?  For any
verbs, what's the subject and what's the predicate?  Misunderstanding the
grammer can possibly lead to significant misunderstandings of the meaning.

You'll want to pick a different writing utensil (e.g., colored pencils) and add
on to your marking legend, picking different colors for the different things to
note above.  The one exception might be lists, because as you're marking up
your passage you might find it more beneficial to enumerate a summarized list
in the margin than to highlight the items in the list in the text itself.
Again, how you do this is entirely up to you.

You can now return to |MAT.5.1-MAT.7.29|, this time analyzing the text sentence
by sentence.  As you do so, mark up each sentence, following the additions
you've made to your marking legend.  At this point your marking of the passage
is complete, and your Bible may look something like this:

.. figure:: /../images/bible-marking.jpg

Contextualization
=================

Whew!  Time for us to take a breather for a minute.  We've finally finished
with the exegesis, where we first did some research into the literary and
historical contexts, and then dug into the text itself.  Now that we know what
the text actually says, we can finally get around to figuring out what it means
to us today.  This process of contextualization consists of four parts, though:

1. understanding the disconnect between you and the original audience,
2. determining the underlying principles in the text,
3. ensuring those principles jive with the rest of the Bible, and finally
4. applying the principles in your current context.

The good news is this part of the interpretive process will go much more
quickly, thanks to all the hard leg-work you've done up to this point.

Understand the Disconnect
^^^^^^^^^^^^^^^^^^^^^^^^^

The first thing you want to do as you move into contextualization is to make
sure you understand the disconnect between yourself and the original audience.
You've actually done most of this work already as part of analyzing the
historical context, but it's good to remind yourself at this point that while
you have much in common with the original hearers of the text, you are also
pretty far-removed from them in certain respects.  Go ahead and jot down the
differences you can recall from your investigation earlier.  Think in terms of:

* People
* Place
* Time
* Culture
* Language
* Customs

Determine the Principles
^^^^^^^^^^^^^^^^^^^^^^^^

With the disconnect firmly in mind, it's now time to see what principles you
can glean from the text.  Refer back to all your Bible marking and notes, and
see if you can find any of the following, from the perspective of the original
audience:

* Truths to believe
* Commands to obey
* Promises to claim
* Exhortations to take to heart
* Warnings to heed
* Examples to either follow or avoid
* Prayers to pray
* Sins to repent or steer clear of

If you do this with |MAT.5.1-MAT.7.29|, you may come up with a list similar to
the following:

* **Truths**

  * The Beatitudes (|MAT.5.3-MAT.5.12|)
  * You are salt and light (|MAT.5.13-MAT.5.16|)
  * Adultery starts in the heart (|MAT.5.27-MAT.5.32|)
  * Good and evil are mutually exclusive (|MAT.6.22-MAT.6.24|)
  * God gives good things to those who ask (|MAT.7.7-MAT.7.11|)
  * False believers are plentiful (|MAT.7.13-MAT.7.23|)

.. |MAT.5.3-MAT.5.12| replace:: :raw-html:`<a data-gb-link="MAT.5.3-MAT.5.12">5:3&ndash;12</a>`
.. |MAT.5.13-MAT.5.16| replace:: :raw-html:`<a data-gb-link="MAT.5.13-MAT.5.16">5:13&ndash;16</a>`
.. |MAT.5.27-MAT.5.32| replace:: :raw-html:`<a data-gb-link="MAT.5.27-MAT.5.32">5:27&ndash;32</a>`
.. |MAT.6.22-MAT.6.24| replace:: :raw-html:`<a data-gb-link="MAT.6.22-MAT.6.24">6:22&ndash;24</a>`
.. |MAT.7.7-MAT.7.11| replace:: :raw-html:`<a data-gb-link="MAT.7.7-MAT.7.11">7:7&ndash;11</a>`
.. |MAT.7.13-MAT.7.23| replace:: :raw-html:`<a data-gb-link="MAT.7.13-MAT.7.23">7:13&ndash;23</a>`

* **Commands**

  * Take no oath (|MAT.5.33-MAT.5.37|)
  * Don’t get even (|MAT.5.38-MAT.5.39|)
  * Love your enemies (|MAT.5.43-MAT.5.48|)
  * Don’t worry; trust God (|MAT.6.25-MAT.6.34|)
  * Don’t be a hypocrite (|MAT.7.1-MAT.7.5|)
  * Don’t give what’s holy to dogs (|MAT.7.6|)
  * Treat people the way you want to be treated (|MAT.7.12|)

.. |MAT.5.33-MAT.5.37| replace:: :raw-html:`<a data-gb-link="MAT.5.33-MAT.5.37">5:33&ndash;37</a>`
.. |MAT.5.38-MAT.5.39| replace:: :raw-html:`<a data-gb-link="MAT.5.38-MAT.5.39">5:38&ndash;39</a>`
.. |MAT.5.43-MAT.5.48| replace:: :raw-html:`<a data-gb-link="MAT.5.43-MAT.5.48">5:43&ndash;48</a>`
.. |MAT.6.25-MAT.6.34| replace:: :raw-html:`<a data-gb-link="MAT.6.25-MAT.6.34">6:25&ndash;34</a>`
.. |MAT.7.1-MAT.7.5| replace:: :raw-html:`<a data-gb-link="MAT.7.1-MAT.7.5">7:1&ndash;5</a>`
.. |MAT.7.6| replace:: :raw-html:`<a data-gb-link="MAT.7.6">7:6</a>`
.. |MAT.7.12| replace:: :raw-html:`<a data-gb-link="MAT.7.12">7:12</a>`

* **Promises**

  * The law still applies till heaven and earth pass away (|MAT.5.18|)

.. |MAT.5.18| replace:: :raw-html:`<a data-gb-link="MAT.5.18">5:18</a>`

* **Exhortations**

  * Your light must shine (|MAT.5.16|)
  * Keep commandments and teach others to do so (|MAT.5.19|)
  * Be reconciled (|MAT.5.23-MAT.5.26|)
  * Be generous (|MAT.5.40-MAT.5.42|)
  * Store up treasures in heaven (|MAT.6.19-MAT.6.21|)
  * Be wise (|MAT.7.24-MAT.7.27|)

.. |MAT.5.16| replace:: :raw-html:`<a data-gb-link="MAT.5.16">5:16</a>`
.. |MAT.5.19| replace:: :raw-html:`<a data-gb-link="MAT.5.19">5:19</a>`
.. |MAT.5.23-MAT.5.26| replace:: :raw-html:`<a data-gb-link="MAT.5.23-MAT.5.26">5:23&ndash;26</a>`
.. |MAT.5.40-MAT.5.42| replace:: :raw-html:`<a data-gb-link="MAT.5.40-MAT.5.42">5:40&ndash;42</a>`
.. |MAT.6.19-MAT.6.21| replace:: :raw-html:`<a data-gb-link="MAT.6.19-MAT.6.21">6:19&ndash;21</a>`
.. |MAT.7.24-MAT.7.27| replace:: :raw-html:`<a data-gb-link="MAT.7.24-MAT.7.27">7:24&ndash;27</a>`

* **Warnings**

  * Don’t let your salt become tasteless (|MAT.5.13|)
  * Don’t break commandments or teach others to do so (|MAT.5.19|)
  * Your anger has consequences (|MAT.5.21-MAT.5.26|)
  * If your righteousness is for show, you have no reward in heaven (|MAT.6.1-MAT.6.8|, |MAT.6.16-MAT.6.18|)
  * If you don’t forgive, you won’t be forgiven (|MAT.6.14-MAT.6.15|)
  * Don’t be a fool (|MAT.7.24-MAT.7.27|)

.. |MAT.5.13| replace:: :raw-html:`<a data-gb-link="MAT.5.13">5:13</a>`
.. |MAT.5.21-MAT.5.26| replace:: :raw-html:`<a data-gb-link="MAT.5.21-MAT.5.26">5:21&ndash;26</a>`
.. |MAT.6.1-MAT.6.8| replace:: :raw-html:`<a data-gb-link="MAT.6.1-MAT.6.8">6:1&ndash;8</a>`
.. |MAT.6.16-MAT.6.18| replace:: :raw-html:`<a data-gb-link="MAT.6.16-MAT.6.18">6:16&ndash;18</a>`
.. |MAT.6.14-MAT.6.15| replace:: :raw-html:`<a data-gb-link="MAT.6.14-MAT.6.15">6:14&ndash;15</a>`

* **Examples**

  * Follow the wise man; don't follow the fool (|MAT.7.24-MAT.7.27|)

* **Prayers**

  * The Lord’s prayer (|MAT.6.9-MAT.6.13|)

.. |MAT.6.9-MAT.6.13| replace:: :raw-html:`<a data-gb-link="MAT.6.9-MAT.6.13">6:9&ndash;13</a>`

* **Sins**

  * Breaking God's law; teaching others to do so (|MAT.5.19|)
  * Murder/anger (|MAT.5.21-MAT.5.26|)
  * Lust/adultery (|MAT.5.27-MAT.5.32|)
  * Making false vows (|MAT.5.33-MAT.5.37|)
  * Retaliation (|MAT.5.38-MAT.5.42|)
  * Hypocrisy (|MAT.6.1-MAT.6.8|, |MAT.6.16-MAT.6.18|, |MAT.6.22-MAT.6.24|,
    |MAT.7.1-MAT.7.5|, |MAT.7.15-MAT.7.23|)

.. |MAT.5.38-MAT.5.42| replace:: :raw-html:`<a data-gb-link="MAT.5.38-MAT.5.42">5:38&ndash;42</a>`
.. |MAT.7.15-MAT.7.23| replace:: :raw-html:`<a data-gb-link="MAT.7.15-MAT.7.23">7:15&ndash;23</a>`

Most of the principles we came across have only a single passage from which
they were drawn.  The notable exception is the last sin to repent and steer
clear of, hypocrisy, which Jesus addresses no fewer than five times in this one
brief sermon.  This is telling, particularly because our verse in question,
|MAT.7.1|, is in one of these passages.  Instead of the principle being,
"You're not supposed to judge me," it looks like perhaps it's something more
along the lines of, "You're not supposed to be a hypocrite."

.. note::

   In an ironic turn of events, the verse that's been thrown at you as an
   accusation actually winds up condemning your accuser instead, since their
   whole "You can't judge me!" routine is them judging you to be wrong for
   having judged them.  Why is making value judgments okay for them but not for
   you?  If they say it's because they don't believe the Bible, so it doesn't
   apply to them, then why are they trying to hold you accountable to a
   principle which they believe to be false?  You can continue down this rabbit
   hole of moral relativism for some time.  Ideally they would eventually
   realize their absurdity and change their minds, but more likely than not
   they'll just wind up leaving or changing the topic, but at least you've
   planted a seed of doubt that may germinate some day.

Check the Rest of Scripture
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Now that you have a list of potential principles to apply to your life, you'll
want to do one last check and ensure that they all line up with the rest of the
truths revealed in scripture.  The reason for this final check is that `the
Bible is self-consistent`_, and this internal consistency across 40+ authors
over ~1,500 years, three continents, three languages, and a variety of literary
genres, is an evidence of its divine origin.  You're not going to be able to
find some truth in one passage of scripture that's at odds with some other
truth supported elsewhere in scripture, or rather if you do, you've made a
mistake somewhere.

.. _the Bible is self-consistent:  https://www.gotquestions.org/is-the-Bible-true.html

To do this cross-checking, you'll probably want to reference the topical index
in the back of your study Bible.  For instance, consider the principle "don’t
get even" that we pulled from |MAT.5.38-MAT.5.39-with-book-name|.  If you were
to look up, say, "retaliation" or "revenge" in your index, you might come
across verses like |LEV.19.18|, |PRO.20.22|, |PRO.24.29|,
|ROM.12.17-ROM.12.21|, |1PE.3.9|, etc.  These other passages all seem to
support the command you gleaned from the passage, so it looks like it agrees
with the rest of scripture, though perhaps a better way to phrase it would be
"don't take revenge" or "don't retaliate."

.. |MAT.5.38-MAT.5.39-with-book-name| replace:: :raw-html:`<a data-gb-link="MAT.5.38-MAT.5.39">Matthew 5:38&ndash;39</a>`
.. |LEV.19.18| replace:: :raw-html:`<a data-gb-link="LEV.19.18">Leviticus 19:18</a>`
.. |PRO.20.22| replace:: :raw-html:`<a data-gb-link="PRO.20.22">Proverbs 20:22</a>`
.. |PRO.24.29| replace:: :raw-html:`<a data-gb-link="PRO.24.29">Proverbs 24:22</a>`
.. |ROM.12.17-ROM.12.21| replace:: :raw-html:`<a data-gb-link="ROM.12.17-ROM.12.21">Romans 12:17&ndash;21</a>`
.. |1PE.3.9| replace:: :raw-html:`<a data-gb-link="1PE.3.9">1 Peter 3:9</a>`

.. note::

   Here I made sure to grab some verses from different authors, time periods,
   and genres, just to make extra sure that I was dealing with a general
   principle that should be applicable across time to me and my current
   context.

We could go through this process to check all the principles we jotted down
against the rest of the Bible, but that would take a good bit of time, so for
expediency's sake let's turn now to our passage in question.  Recall that the
common interpretation of |MAT.7.1| is that it contains the command that
"Christians must not make value judgments regarding another's thoughts, speech,
or actions."  The question, then, is whether this interpretation matches what
we know from the rest of the Bible.

At first glance, it looks like it doesn't even match the rest of Jesus' sermon.
Consider the following judgements he proclaims:

+-------+-----------------------------+
| Verse | Against Those Who           |
+=======+=============================+
| 5:12  | persecuted the prophets     |
+-------+-----------------------------+
| 5:13  | are "tasteless salt"        |
+-------+-----------------------------+
| 5:19  | teach lawlessness           |
+-------+-----------------------------+
| 5:22  | harbor unrepentant anger    |
+-------+-----------------------------+
| 5:28  | lust                        |
+-------+-----------------------------+
| 5:32  | divorce, with one exception |
+-------+-----------------------------+
| 5:34  | break oaths                 |
+-------+-----------------------------+
| 5:39  | retaliate                   |
+-------+-----------------------------+
| 5:43  | hate their enemies          |
+-------+-----------------------------+
| 6:1   | show off by giving          |
+-------+-----------------------------+
| 6:5   | show off by praying         |
+-------+-----------------------------+
| 6:15  | don't forgive               |
+-------+-----------------------------+
| 6:16  | show off by fasting         |
+-------+-----------------------------+
| 6:19  | are greedy                  |
+-------+-----------------------------+
| 6:34  | worry                       |
+-------+-----------------------------+
| 7:3   | are hypocrites              |
+-------+-----------------------------+
| 7:15  | are false prophets          |
+-------+-----------------------------+
| 7:23  | practice lawlessness        |
+-------+-----------------------------+
| 7:26  | ignore him                  |
+-------+-----------------------------+

That's pretty clear evidence that this common interpretation isn't what Jesus
intended, but just to be sure, let's look elsewhere.  In |1CO.2.14-1CO.3.3|,
|1CO.5.3|, and |1CO.5.11-1CO.5.13|, we see Paul passing judgment on believers
living in sin.  Then in |1CO.6.2-1CO.6.3|, he mentions that in the future
believers will judge the whole world, and states that we should therefore be
able to judge matters in this life now.  Both |GAL.6.1| and |JAS.5.20| speak to
judging in the context of restoring a sinning brother.  Finally, the parallel
Sermon on the Mount passage in Luke's gospel echoes our verse under
consideration in |LUK.6.36-LUK.6.37|, indicating that our judgments are to be
merciful, like God's, which agrees with Jesus' statement in |JHN.7.24| to judge
rightly, and the need to discern between good and evil in |HEB.5.14|.  A
complete treatment of biblically acceptable and unacceptable ways of passing
judgment on others would take a good deal more work on our part.  If you're up
for it, it would be greatly edifying, but for our purposes here it seems we can
safely say that |MAT.7.1| does not in fact mean that you can't judge others.

.. |1CO.2.14-1CO.3.3| replace:: :raw-html:`<a data-gb-link="1CO.2.14-1CO.3.3">1 Corinthians 2:14&ndash;3:3</a>`
.. |1CO.5.3| replace:: :raw-html:`<a data-gb-link="1CO.5.3">5:3</a>`
.. |1CO.5.11-1CO.5.13| replace:: :raw-html:`<a data-gb-link="1CO.5.11-1CO.5.13">5:11&ndash;13</a>`
.. |1CO.6.2-1CO.6.3| replace:: :raw-html:`<a data-gb-link="1CO.6.2-1CO.6.3">6:2&ndash;3</a>`
.. |GAL.6.1| replace:: :raw-html:`<a data-gb-link="GAL.6.1">Galatians 6:1</a>`
.. |JAS.5.20| replace:: :raw-html:`<a data-gb-link="JAS.5.20">James 5:20</a>`
.. |LUK.6.36-LUK.6.37| replace:: :raw-html:`<a data-gb-link="LUK.6.36-LUK.6.37">Luke 6:36&ndash;37</a>`
.. |JHN.7.24| replace:: :raw-html:`<a data-gb-link="JHN.7.24">John 7:24</a>`
.. |HEB.5.14| replace:: :raw-html:`<a data-gb-link="HEB.5.14">Hebrews 5:14</a>`

.. note::

   In everything above, I've always referred to a person's thoughts, speech,
   and actions in concert.  However it's worth noting that only the latter two
   are observable.  In the case of passing judgments on a person's thoughts,
   I've been assuming they've told you in conversation what they're thinking,
   and you've responded with something like, "That's not right."  That's fine,
   but if you were to make assumptions about what their thoughts are and then
   pass judgment on them without actually knowing, that's both wrong and
   entirely foolish.

Apply the Principle in Your Context
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

At this point you should have your list of principles gleaned from the text
that are consistent with the rest of scripture.  Any potential principles that
were at odds with other passages, you've either cast aside or reinterpreted in
light of the fuller biblical context.  Keep in mind, though, that these
remaining principles are from the perspective of the original audience.  The
last bit of work we have to do is to figure out how to apply them in our
current cultural context.  To do so, you'll want to examine the original
situations from which the principles were extracted, and then see if there are
parallel situations here and now to which we can apply them.  If so, then how
do they apply?  When answering that question, you'll want to be as specific as
possible.

Sometimes this is fairly straightforward.  Consider our "don't get even"
example from |MAT.5.38-MAT.5.39-with-book-name| earlier.  Jesus' audience was
familiar with the concept of reciprocal justice from the law of Moses in
|EXO.21.23-EXO.21.25|, and numerous other ancient and modern cultures have
accepted and applied this standard of justice.  Indeed, if you have young
children, you probably realize this concept of fairness is just part of human
nature.  With all that in mind, it seems like it should be the case that
reciprocal justice applies today, as it did for Jesus' listeners, as it did in
the `Code of Hammurabi`_ for the Babylonians, etc.  But Jesus says we're not
supposed to get even, and he didn't come to abolish the law (|MAT.5.17|), so
what's going on here?

.. |EXO.21.23-EXO.21.25| replace:: :raw-html:`<a data-gb-link="EXO.21.23-EXO.21.25">Exodus 21:23&ndash;25</a>`
.. _Code of Hammurabi:  https://en.wikipedia.org/wiki/Code_of_Hammurabi
.. |MAT.5.17| replace:: :raw-html:`<a data-gb-link="MAT.5.17">Matthew 5:17</a>`

Keep in mind that things like |EXO.21.1-EXO.22.31|, Hammurabi's code, and
others, are legal documents specifying right behavior and any particular just
penalties to be imposed for particular infractions.  The dealing out of this
"ox for ox" justice was to be handled by the established legal system, which
should be able to judge fairly and impartially all the details of the case.
However, we have a natural human tendency---given our fallen, sinful state---to
want to exact revenge immediately, rather than see that justice is served in
due time.  This is why our initial interpretation of "don't get even" was not
precise enough.  Should we desire to see justice served when and where
appropriate?  Yes.  Should we retaliate in anger when someone wrongs us?  No.

.. |EXO.21.1-EXO.22.31| replace:: :raw-html:`<a data-gb-link="EXO.21.1-EXO.22.31">Exodus 21&ndash;22</a>`

.. note::

   For guidelines on when to seek justice for what kinds of infractions, we'd
   need to do a much more extensive study of scripture.  Note, however, that
   Jesus addressed the issue of taking someone to court earlier in the sermon
   in |MAT.5.25|.

.. |MAT.5.25| replace:: :raw-html:`<a data-gb-link="MAT.5.25">Matthew 5:25</a>`

Now there are other times when figuring out how to apply a principle from the
original audience to our current context is a bit more difficult.  Take, for
instance:

.. raw:: html

   <div style="border: 2px solid ForestGreen;
               border-radius: 5px;
               padding: 10px;
               background-color: HoneyDew;">
       <div data-gb-widget="passage" data-passage="MAT.5.13">
       </div>
   </div>
   <br>

What on earth does this mean?  The earth is bland and we need to give it some
zest by sprinkling some Christians over it?  And how on earth does salt lose
its saltiness?

To understand this principle we need to dig into how Jesus' listeners would
have understood it.  In the ancient world, salt wasn't primarily a seasoning,
but rather a preservative.  It's how you cured meats so they wouldn't spoil and
would instead stay good and healthy to eat for much longer.  On top of that,
ancient Near Eastern people didn't have little boxes of salt in their cupboard.
Rather, they stored a great deal of salt in a large clay pot near the entrance
to the home (recall the description of the typically dwelling from earlier).
To preserve your food, you would use your hands to grab salt from the pot and
smother it on the food.  But this part of the world was and is a dusty place,
so dust was constantly infiltrating your house, landing everywhere, and your
hands were dirty (remember there was no running water).  This meant over time,
before you worked your way to the bottom of the pot, enough dirt had been mixed
with the salt such that it was no longer any good for its purpose of
preservation.  When that happened, there was no way to fix it, so you'd tip the
pot out the front door and scatter it on the street, and then go refill your
salt pot.

When Jesus tells his listeners that they're the salt of the earth, this is what
they're understanding.  So how do we translate their understanding to us today?
Those who hearken to Jesus' teachings---both the original listeners, and
believers today---act as a preservative agent for the rest of the world.  They
keep the world from spoiling.  If you'd like to see how this has played out
over the past two millennia, I highly recommend Glenn Sunshine's
|why_you_think_the_way_you_do|_.  But in order for Jesus' followers to fulfill
that purpose, they must remain pure.  If over time they lose their
purity---allowing themselves to be corrupted by sin, false teachings,
etc.---they degrade to the point where they can no longer fulfill their
purpose.  Given the state of all who call themselves Christians today, and the
state of the world around us, we can see how this too has played out over time.
The takeaway, then, is that it is imperative that we stay faithful to God's
truth revealed in scripture, that we may fulfill our purpose in proclaiming
Christ as Lord to all the earth (|MAT.28.18-MAT.28.20|).

.. _why_you_think_the_way_you_do:  https://www.amazon.com/Why-You-Think-Way-Worldviews/dp/0310292301/ref=sr_1_1?keywords=why+you+think+the+way+you+do&sr=8-1
.. |why_you_think_the_way_you_do| replace::  *Why You Think the Way You Do:  The Story of Western Worldviews from Rome to Home*
.. |MAT.28.18-MAT.28.20| replace:: :raw-html:`<a data-gb-link="MAT.28.18-MAT.28.20">Matthew 28:18&ndash;20</a>`

Summary
=======

At this point you've finished your whirlwind introduction to the discipline of
biblical hermeneutics, and you should be sufficiently better equipped to
rightly interpret God's word, and guard against its frequent mininterpretation.
To recap:  Before you can understand what a passage of scripture means *to
you*, you must first understand what it means, *period*.  This involves
researching both the literary and historical contexts of the verses under
consideration, including things like genre, author, intended audience, etc.
With the context well-understood, you can move on to an analysis of the text
itself, grasping the meaning of larger sections, then smaller paragraphs,
then sentences.  With your exegesis complete, you can move on to
contextualizing what you've learned, and you do so by noting the disconnect
between yourself and the original audience, determining the underlying
principles put forth, ensuring the principles you've discovered are congruent
with the rest of scripture, and then applying them in your current context.
That's an awful lot, and practicing what you've learned here---both in your own
Bible study, and in training others in the discipline---is more than enough to
keep you busy.

However, it's worth noting that this was only an introduction, and there's
plenty of material that was left out.  For instance:

* **Details of Bible translation:**  How exactly translation takes place, the
  differences between word-for-word vs thought-for-thought vs paraphrases,
  which kinds you should use for which parts of the hermeneutical process, etc.
* **The role of the Holy Spirit:**  How does God's presence in your life (or
  lack thereof) impact the interpretive process?
* **Specifics for interpretting particular genres:**  Guidelines for
  interpreting poetry vs apocalyptic literature vs historical narrative, etc.
* **Word studies:**  Analyses of the original language, usage elsewhere in
  scripture, meanings in various contexts, etc.
* **The use of commentaries:**  What kinds to use at what points in the
  process.
* **Interpretive falacies:**  Various errors into which people typically fall,
  how to identify them, tactics for avoiding them, etc.

If you're eager to learn more and build your Bible study skills even further,
here are some resources worth considering:

If you'd like to have a reference text on your shelf, grab a copy of
|grasping_gods_word|_, by J. Scott Duvall and J. Daniel Hays.  It's one of the
texts I referenced when putting together this module, and it's well worth
having in your collection.

.. _grasping_gods_word:  https://www.amazon.com/Grasping-Gods-Word-Fourth-Hands/dp/0310109175/ref=sr_1_1?keywords=grasping+god%27s+word&sr=8-1
.. |grasping_gods_word| replace::  *Grasping God's Word*

If you'd like to dive deeper with some instructor-led courses, head on over to
`biblicaltraining.org`_, create a free account, and check out the following:

.. _biblicaltraining.org:  https://biblicaltraining.org

+--------------+-----------------------------------+
| Level        | Course                            |
+==============+===================================+
| Introductory | `How to Read Your Bible`_         |
+--------------+-----------------------------------+
| Intermediate | `A Guide to Bible Study Methods`_ |
+              +-----------------------------------+
|              | |intermediate_hermeneutics|_      |
+--------------+-----------------------------------+
| Advanced     | `Biblical Hermeneutics`_          |
+--------------+-----------------------------------+

.. _How to Read Your Bible:  https://www.biblicaltraining.org/learn/foundations/nt102-how-to-read-your-bible
.. _A Guide to Bible Study Methods:  https://www.biblicaltraining.org/learn/academy/nt110-a-guide-to-bible-study-methods
.. _intermediate_hermeneutics:  https://www.biblicaltraining.org/learn/academy/nt310-hermeneutics
.. |intermediate_hermeneutics| replace::  Hermeneutics
.. _Biblical Hermeneutics:  https://www.biblicaltraining.org/learn/institute/nt510-biblical-hermeneutics

Alternatively, if you're willing to invest some money in further study, check
out the `in-person workshops`_ or the `online courses`_ offered by the `Simeon
Trust`_.

.. _in-person workshops:  https://simeontrust.org/workshops/
.. _online courses:  https://simeontrust.org/online-courses/
.. _Simeon Trust:  https://simeontrust.org/

Finally, after teaching this module I came across the following video recording
of a Bible study class led by `Voddie Baucham Jr`_.  It's an interactive
session, and the presentation is geared toward those in the class with him, not
those watching it after the fact, but it's still helpful to see much of the
above from a different perspective.

.. _Voddie Baucham Jr:  https://www.voddiebaucham.org/

.. raw:: html

    <div style="position: relative;
                padding-bottom: 56.25%;
                height: 0;
                overflow: hidden;
                max-width: 100%;
                height: auto;">
        <iframe
         src="https://www.youtube.com/embed/l5-e86-fFoo"
         frameborder="0"
         allowfullscreen
         style="position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                padding: 10px;">
        </iframe>
    </div>

.. raw:: html

   <script src="https://bibles.org/static/widget/v2/widget.js"></script>
   <script>
       GLOBALBIBLE.init({
           url: "https://bibles.org",
           bible: "f421fe261da7624f-01",
           autolink: false,
       });
   </script>

