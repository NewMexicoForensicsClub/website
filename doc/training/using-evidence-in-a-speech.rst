Using Evidence in a Speech
--------------------------

:raw-html:`<div align="right">🕑 5 min.</div>`

At this point, you've spent plenty of time :doc:`gathering evidence
<conducting-research>` and then :doc:`organizing your thoughts
<organizing-your-research>`.  Now it's time to start crafting your speech,
backing up your claims with the research you've done.  How exactly you go about
using your evidence in your speech will depend on the type of speech you're
giving.

For Platform Speeches
=====================

After researching for a platform speech, you should have a number of different
decks of cards, one per source you've consulted.  Each deck should consist of a
citation card, five credibility cards, and numerous evidence cards.  Here's how
each type of card factors into your speech.

Evidence Cards
~~~~~~~~~~~~~~

As you're writing up your speech, you'll reach a point where you want to
include one of your pieces of evidence to back up your point.  To do so,
mention the source, and then incorporate the evidence into the flow of the
text.  You don't have to use the wording on the card verbatim unless you're
wanting to preserve a direct quote from the original source.  For instance:

  Josh McDowell indicates that Matthew's gospel was likely composed in the
  early to mid-60s AD, because Irenaeus records that it was written when Peter
  and Paul were preaching and founding the church in Rome.

Note that this example only includes the author, which may be sufficient,
depending on how well-known the author is, and the specifics of the evidence
included.  Other times you may wish to include the published work as well.
E.g.:

  In *Evidence that Demands a Verdict*, Josh McDowell indicates that Matthew's
  gospel was likely composed in the early to mid-60s AD, because Irenaeus
  records that it was written when Peter and Paul were preaching and founding
  the church in Rome.

In either case, at the end of the sentence, you'll want to insert a footnote,
and then down in the footnote include the abbreviated citation from your card
(e.g., "McDowell, *Evidence*, 43.").  The footnote will be the only place the
evidence's location (i.e., page number) appears, and the footnotes will not be
spoken by you when reciting your speech---they're just there in case anyone
wants to question your sources.

Credibility Cards
~~~~~~~~~~~~~~~~~

More often than not, the information on your credibility cards will not wind up
in the speech itself.  However, if the evidence you're presenting might be
perceived as controversial or hard to believe, it will likely be worthwhile to
include a summary of your credibility information alongside the evidence
itself.  For instance:

  Matthew's gospel was likely composed in the early to mid-60s AD, because
  Irenaeus records that it was written when Peter and Paul were preaching and
  founding the church in Rome.  This comes from *Evidence that Demands a
  Verdict*, one of most influential books of the latter half of the 20th
  century, written by renowned evangelical apologist Josh McDowell.

The Citation Card
~~~~~~~~~~~~~~~~~

Finally, for each of the sources you wind up using in your speech, you'll need
to include the full citation from the citation card in a bibliography at the
end of the speech.  As with the footnotes throughout the text, the bibliography
won't be recited by you when speaking, but is included for when you need to
point others directly to your sources of information.

Once you've completed your speech, you'll also need to go back through all the
footnotes and replace the first abbreviated citation per source with the full
citation.  This means the full citation will appear in two places in your
document:  (1) in the first footnote from the source, and (2) in the
bibliography.  All other footnotes for the same source will only use the
abbreviated citation.

For Limited Preparation Speeches and Debates
============================================

After researching for limited preparation speeches or debates, you still have a
bunch of note cards, but they're not organized into decks per source as for
platform speeches above.  Instead each card should contain a tag line, the full
citation, a statement on the qualifications of the source, and then the
verbatim quote itself.  When it comes time to integrate one of these pieces of
evidence into your speech, you don't just read each component of the card, one
after the other.  If you did so, it would be a jarring experience for the
audience.  Instead, you're going to want to work it into the flow of the speech
more naturally, following the pattern below.

Lead-In
  You likely don't want to jump right into your tag line.  Instead you'll want
  to introduce your piece of evidence in the context of where you are in your
  speech.  How does what you are about to say connect to what you've said up to
  this point?
Tag
  Once you've transitioned from where you were in your speech to this new piece
  of evidence, state the tag line.  Feel free to change it up a bit to fit
  where it falls in your speech nicely.  You still want it to be short, sweet,
  and to the point, but it doesn't need to be an exact match of what's on your
  card.  If people are taking notes, this is what they'll write down to
  remember what you've said.
Source
  After hearing the tag line, your audience needs to know who's behind the
  evidence.  When stating the source, you only really need to include where
  it's from (e.g., the name of the journal), and when it was published.  You
  want to have the rest of the citation on hand if your sources are called into
  question, but you don't want to include the rest of the citation details in
  the speech itself.  Depending on the evidence, it may also be worthwhile to
  include the source's credentials here too.
Evidence
  Once you've sufficiently introduced the piece of evidence, you're ready to
  read the evidence itself.  The recommendation would be to copy the whole
  contents of the quote into your document, but then make the parts your intend
  to speak **bold.**  You want the whole context of the quote there on the page
  for your reference, but in the context of where you are in the speech, you
  might not want to read the entire thing.
Conclusion
  Finally, it's not sufficient to simply read a quote from someone else that
  you think is pertinent to the point you're trying to make.  Conclude with a
  sentence or two summarizing the quote in your own words and making clear its
  application to your point.

.. note::

   This pattern of including evidence in a speech is akin to the usual "tell
   them what you're going to tell them, tell them, and then tell them what you
   told them" mantra.
