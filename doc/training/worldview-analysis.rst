Worldview Analysis
------------------

.. role:: raw-html(raw)
   :format: html

:raw-html:`<div align="right">🕑 6 min.</div>`

The following are a number of good things to keep in mind.  These will be
particularly helpful in the context of the
:doc:`/events/limited-preparation-speeches/apologetics` and
:doc:`/events/limited-preparation-speeches/mars-hill` events, but they are also
of general utility when developing and analyzing arguments in any context.

.. contents::  Contents
   :local:

The Bible
=========

Our worldview is based on the truth revealed in the Bible.  A natural question
that arises when pointing to the authority of scripture is, "Why should I
believe the Bible?"  The answer is, "I choose to believe the Bible because it
is

1. a reliable collection of historical documents
2. written down by eyewitnesses
3. during the lifetime of other eyewitnesses;
4. they report supernatural events
5. that took place in fulfillment of specific prophecies
6. and claim that their writings are divine rather than human in origin."

.. toctree::
   :titlesonly:
   :hidden:

   worldview-analysis/the-bible

For details supporting this statement, see :doc:`here
<worldview-analysis/the-bible>`.

The Law
=======

What God requires of man is summed up in the ten commandments:

+------------------------------------+-------------------------------+
| Love God                           | Love Others                   |
+====================================+===============================+
| 1. Have no other gods.             | 5. Honor your parents.        |
+------------------------------------+-------------------------------+
| 2. Have no idols.                  | 6. Do not murder.             |
+------------------------------------+-------------------------------+
| 3. Do not profane the Lord's name. | 7. Do not commit adultery.    |
+------------------------------------+-------------------------------+
| 4. Remember the Sabbath.           | 8. Do not steal.              |
+------------------------------------+-------------------------------+
|                                    | 9. Do not bear false witness. |
+------------------------------------+-------------------------------+
|                                    | 10. Do not covet.             |
+------------------------------------+-------------------------------+

.. _catechism:

Catechism
=========

Apologetics isn't about knowing what everyone else believes better than them,
such that you can point out the inconsistencies in their beliefs; rather, it's
about knowing what you believe such that you can spot something that doesn't
line up with it.  The best tool for building these foundational beliefs is
catechism, which is a series of questions and answers used for instruction.  A
resource we recommend is `A Catechism for Boys and Girls
<https://founders.org/library/section-1-catechism-for-girls-and-boys/>`_.
Tackling a single, brief question and answer per day, you can have all the
doctrines of the Christian faith memorized in a few months.

.. note::

   Keep in mind that such extra-biblical resources, though helpful, are not
   inerrant like the Bible itself is.

The Four Fundamental Questions
==============================

There are four fundamental questions that people wrestle with:

1. Who am I?
2. Why am I here?
3. What is wrong with the world?
4. How can what is wrong be made right?

Everyone has an answer to these questions, whether or not they've thought
through them intentionally.  The biblical worldview answers these questions as
follows:

1. I am an individual (|PSA.139.13|) made in the image of God, the crowning
   glory of his creation (|GEN.1.26-GEN.1.31|).
2. I am here to glorify God (|1CO.10.31|) and enjoy him forever (|PSA.144.15|).
3. The problem with the world is I don’t do what I was created to do
   (|LEV.5.17|; |JAS.4.17|).
4. To address the problem, I can surrender my life to the lordship of Christ
   (|ROM.10.9|) and allow God to transform me more and more each day into his
   likeness (|2CO.3.18|).  Ultimately Christ will return to judge the world and
   make all things right again (|MAT.25.31-MAT.25.46|; |REV.21.1-REV.21.8|).

.. |PSA.139.13| replace:: :raw-html:`<a data-gb-link="PSA.139.13">Psalm 139:13</a>`
.. |GEN.1.26-GEN.1.31| replace:: :raw-html:`<a data-gb-link="GEN.1.26-GEN.1.31">Genesis 1:26&ndash;31</a>`
.. |1CO.10.31| replace:: :raw-html:`<a data-gb-link="1CO.10.31">1 Corinthians 10:31</a>`
.. |PSA.144.15| replace:: :raw-html:`<a data-gb-link="PSA.144.15">Psalm 144:15</a>`
.. |LEV.5.17| replace:: :raw-html:`<a data-gb-link="LEV.5.17">Leviticus 5:17</a>`
.. |JAS.4.17| replace:: :raw-html:`<a data-gb-link="JAS.4.17">James 4:17</a>`
.. |ROM.10.9| replace:: :raw-html:`<a data-gb-link="ROM.10.9">Romans 10:9</a>`
.. |2CO.3.18| replace:: :raw-html:`<a data-gb-link="2CO.3.18">2 Corinthians 3:18</a>`
.. |MAT.25.31-MAT.25.46| replace:: :raw-html:`<a data-gb-link="MAT.25.31-MAT.25.46">Matthew 25:31&ndash;46</a>`
.. |REV.21.1-REV.21.8| replace:: :raw-html:`<a data-gb-link="REV.21.1-REV.21.8">Revelation 21:1&ndash;8</a>`

A non-biblical worldview will have different answers to one or more of these
questions.

The Historical Meta-Narrative
=============================

History tells a story, the main components of which are:

1. Creation
2. Fall
3. Redemption
4. Consumation

The biblical worldview tells us:

1. God created all things, both seen and unseen (|COL.1.16|), created man and
   woman in his image as the crowning glory of his creation, and deemed it all
   very good (|GEN.1.26-GEN.1.31|).
2. Mankind ruined our perfect relationship with our creator by sinning against
   him (|GEN.3.6-GEN.3.8|).  This fall from grace forever impacted God’s
   perfect creation (|ROM.5.12-ROM.5.21|; |ROM.8.18-ROM.8.22|).
3. After promising redemption for millennia (|GEN.3.15|; |DEU.18.15|), God sent
   his son Jesus (|GAL.4.4|) to become fully human (|JHN.1.14|), to live a
   perfect, sinless life (|HEB.4.15|), to die a substitutionary death on the
   cross in our place (|2CO.5.21|), that his righteousness might be credited to
   us (|ROM.4.20-ROM.4.25|) and we might be reconciled to God
   (|COL.1.19-COL.1.20|) through faith in him (|EPH.2.8-EPH.2.9|).
4. God has appointed a day for Christ to come again to judge the living and the
   dead (|ACT.10.42|) and make all things right (|2PE.3.10-2PE.3.13|).

.. |COL.1.16| replace:: :raw-html:`<a data-gb-link="COL.1.16">Colossians 1:16</a>`
.. |GEN.3.6-GEN.3.8| replace:: :raw-html:`<a data-gb-link="GEN.3.6-GEN.3.8">Genesis 3:6&ndash;8</a>`
.. |ROM.5.12-ROM.5.21| replace:: :raw-html:`<a data-gb-link="ROM.5.12-ROM.5.21">Romans 5:12&ndash;21</a>`
.. |ROM.8.18-ROM.8.22| replace:: :raw-html:`<a data-gb-link="ROM.8.18-ROM.8.22">Romans 8:18&ndash;22</a>`
.. |GEN.3.15| replace:: :raw-html:`<a data-gb-link="GEN.3.15">Genesis 3:15</a>`
.. |DEU.18.15| replace:: :raw-html:`<a data-gb-link="DEU.18.15">Deuteronomy 18:15</a>`
.. |GAL.4.4| replace:: :raw-html:`<a data-gb-link="GAL.4.4">Galatians 4:4</a>`
.. |JHN.1.14| replace:: :raw-html:`<a data-gb-link="JHN.1.14">John 1:14</a>`
.. |HEB.4.15| replace:: :raw-html:`<a data-gb-link="HEB.4.15">Hebrews 4:15</a>`
.. |2CO.5.21| replace:: :raw-html:`<a data-gb-link="2CO.5.21">2 Corinthians 5:21</a>`
.. |ROM.4.20-ROM.4.25| replace:: :raw-html:`<a data-gb-link="ROM.4.20-ROM.4.25">Romans 4:20&ndash;25</a>`
.. |COL.1.19-COL.1.20| replace:: :raw-html:`<a data-gb-link="COL.1.19-COL.1.20">Colossians 1:19&ndash;20</a>`
.. |EPH.2.8-EPH.2.9| replace:: :raw-html:`<a data-gb-link="EPH.2.8-EPH.2.9">Ephesians 2:8&ndash;9</a>`
.. |ACT.10.42| replace:: :raw-html:`<a data-gb-link="ACT.10.42">Acts 10:42</a>`
.. |2PE.3.10-2PE.3.13| replace:: :raw-html:`<a data-gb-link="2PE.3.10-2PE.3.13">2 Peter 3:10&ndash;13</a>`

A non-biblical worldview will address the same four areas of the historical
meta-narrative, but there will be differences in one or more of the stages.

.. note::

   The historical meta-narrative and the four fundamental questions line up as
   follows:  Who am I and why am I here?  Creation.  What’s wrong with the
   world?  Fall.  How can what’s wrong be made right?  Redemption and
   consummation.

The Five-Fold Breakdown
=======================

Your worldview is your lens through which you see the world.  There's nothing
in your life that isn't impacted by it.  People tend to break it down in
different ways, but one way that's helpful to remember is your worldview
consists of of your views on:

1. God
2. Man
3. Truth
4. Knowledge
5. Ethics

The biblical worldview specifies:

1. There is only one true God (|DEU.6.4|), eternally existent in the three
   persons of the Father, the Son, and the Holy Spirit (|MAT.28.19|).
2. God made man in his image as the crowning glory of his creation
   (|GEN.1.26-GEN.1.31|), and gave us the responsibility to exercise dominion
   over his created order (|GEN.2.15|).
3. Truth is absolute and unchanging (|PSA.119.160|), having its source in God
   himself (|JHN.14.6|).
4. We can know truth through what God revealed to man in the special revelation
   of the Bible (|JHN.17.17|), as well as in the general revelation of his
   creation (logic, math, science) (|ROM.1.20|; |PSA.19.1-PSA.19.4|;
   |ROM.2.14-ROM.2.15|).
5. Ethics are absolute and are based on the unchanging word and will of God
   (|EXO.20.1-EXO.20.17|; |MAT.5.17-MAT.5.18|).

.. |DEU.6.4| replace:: :raw-html:`<a data-gb-link="DEU.6.4">Deuteronomy 6:4</a>`
.. |MAT.28.19| replace:: :raw-html:`<a data-gb-link="MAT.28.19">Matthew 28:19</a>`
.. |GEN.2.15| replace:: :raw-html:`<a data-gb-link="GEN.2.15">Genesis 2:15</a>`
.. |PSA.119.160| replace:: :raw-html:`<a data-gb-link="PSA.119.160">Psalm 119:160</a>`
.. |JHN.14.6| replace:: :raw-html:`<a data-gb-link="JHN.14.6">John 14:6</a>`
.. |JHN.17.17| replace:: :raw-html:`<a data-gb-link="JHN.17.17">John 17:17</a>`
.. |ROM.1.20| replace:: :raw-html:`<a data-gb-link="ROM.1.20">Romans 1:20</a>`
.. |PSA.19.1-PSA.19.4| replace:: :raw-html:`<a data-gb-link="PSA.19.1-PSA.19.4">Psalm 19:1&ndash;4</a>`
.. |ROM.2.14-ROM.2.15| replace:: :raw-html:`<a data-gb-link="ROM.2.14-ROM.2.15">Romans 2:14&ndash;15</a>`
.. |EXO.20.1-EXO.20.17| replace:: :raw-html:`<a data-gb-link="EXO.20.1-EXO.20.17">Exodus 20:1&ndash;17</a>`
.. |MAT.5.17-MAT.5.18| replace:: :raw-html:`<a data-gb-link="MAT.5.17-MAT.5.18">Matthew 5:17&ndash;18</a>`

A non-biblical worldview can be broken down into the same five categories, but
will differ in what it specifies for one or more of them.

.. toctree::
   :titlesonly:
   :hidden:

   worldview-analysis/revelation-reason-and-reality

For a breakdown of the various ways we can know truth (#4), see :doc:`here
<worldview-analysis/revelation-reason-and-reality>`.

Apologetics
===========

When it comes to defending your faith, you don't need to study the answers to
thousands of possible questions.  Rather, all the questions you might run into
fall into a small handful of categories, so you just need to be prepared to
speak about those.  There's even a fun acronym to remember:  TASER.

Theology
  What we believe about God, Jesus, and the Holy Spirit.
Anthropology
  What we believe about mankind.
Soteriology
  What we believe about salvation.
Ethics
  What we believe about right behavior, or what's required of us.
Revelation
  What we believe about how God has revealed truth to us.

Additionally, questions can be classified as either:

Doctrinal
  Having to do with what exactly we believe.  Basically, what does the Bible
  say about a topic?  These are often easier to answer, and :ref:`catechism`
  can help you prepare for them.
Application
  Having to do with putting beliefs into practice.  These require more effort,
  as they often involve pulling together multiple doctrinal elements and then
  using logic to apply them to particular situations.

.. raw:: html

   <script src="https://bibles.org/static/widget/v2/widget.js"></script>
   <script>
       GLOBALBIBLE.init({
           url: "https://bibles.org",
           bible: "f421fe261da7624f-01",
           autolink: false,
       });
   </script>

