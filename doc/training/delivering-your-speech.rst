Delivering Your Speech
----------------------

:raw-html:`<div align="right">🕑 4 min.</div>`

When it comes to delivering your speech, there's more involved than simply
getting the words in your head out of your mouth.  You'll need to pay attention
to a number of things, including vocal quality, mannerisms, how you use the
space you've been given, etc.  It's a lot to learn all at once, but once you
get the hang of it, it all becomes second nature.

Tone
====

When giving a speech, you need to keep in mind that you are communicating with
an audience.  There's a tendency with novice public speakers to behave is if
they're talking to a wall rather than a human.  Remember that communication is
a two-way street, involving both the words coming out of your mouth, and how
they're received by the ears of your listeners.  You will likely be tempted to
make your speech sound overly formal and professional.  In the forensics
community, this is what's known as dropping into "speech voice".  Don't be
embarrassed---we all do it---but one of the most consistent pieces of advice I
give my students and those I judge is to imagine they're having a conversation
with friends over lunch and their topic happens to be the topic of their
speech.  How would you say the same thing if you were trying to convince your
peers in a casual setting?

.. note::

   The advice above doesn't necessarily apply 100% of the time.  It'll depend
   on your setting and the content of your presentation.  That said, it's a
   good general rule to keep in mind that a conversational delivery will be
   more readily received.

Hand Motions
============

As you're speaking, you'll want to use your hands and upper body to convey
meaning and add emphasis to your points.  That said, there are a number of
mannerism to try to avoid, because they will distract your audience and
detract from what you're saying.

* Avoid fidgeting with things in your hands:  a button, piece of jewelry,
  clothing, etc.
* You don't want your hands behind your back; rather, you want them in front of
  you where you can use them.
* Generally hand motions below your waist aren't helpful.  Your audience will
  naturally focus on your face, so hands above the waist are more easily
  perceived.
* Beware of messing with your hair or clothing.  If you'll have a tendency to
  do so, make sure your hair is pulled back for the speech itself, and plan
  your clothing appropriately.

There are two things you can do to improve your use of gestures throughout your
piece.  One is to perform it in front of others, asking them to pay attention
to these things, and then debrief with them afterwards.  Another is to record
yourself speaking, and then watch the performance.  Again, it will be helpful
to do this with others, as they can give advice for what might be a useful
motion here, or something else to try there, etc.

Blocking
========

We usually use the term *blocking* to refer to the various movements around the
stage in a play.  That said, the term also applies to speeches as well.  Though
you won't exactly be acting something out in front of your audience, you still
have a "stage" on which to work.  It won't be large, and will depend on the
space you've been given in which to speak, but you'll usually have roughly an
eight-foot by eight-foot square in which to work.  This is your stage; how do
you utilize it?

Start center stage, fairly far back, for your introduction.  Make sure you
address the whole audience with your eyes and hand motions.  Stay comfortably
rooted in place for the duration of your introduction.  What I mean by that is
you're not a pole, rigidly planted in the ground, but you do keep yourself in
the same spot on the stage without fidgeting.  You want all of your motions to
be intentional and help convey meaning.

As you transition from the introduction into your first point, take a few steps
forward and to one side.  This is your new spot on the stage for your first
point.  As you move from your previous spot to this new one, that's a subtle
indication to your audience that you've finished talking about one thing, and
you're moving on to another.  When speaking from this position, try to address
more of your eye connections, hand motions, etc., to the folks on this side of
the room, while at the same time not ignoring everyone else.  It's as if you're
having a more intimate side conversation with a few people over here, but it's
actually for the whole room to hear.

As you transition from your first to your second point, take a few steps toward
the other side of the room.  Again, this is a subtle indication that one
subject is over and you're moving on to another.  This also gives you the
opportunity to more intimately connect with the folks on this other side of the
room.

Depending on the number of points you have, you can repeat this process of
alternating which sections of your audience you're talking with for each point.
When it comes time for the conclusion, you want to wind up front and center,
again addressing the entire audience.

Engaging Your Audience Video Lesson
===================================

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/UaS3i8lNn_g?start=213" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

