Memorizing Your Piece
---------------------

:raw-html:`<div align="right">🕑 3 min.</div>`

For any speeches/performances that won't be given extemporaneously (that is,
for any :doc:`platform speeches </events/platform-speeches>` or
:doc:`interpretations </events/interpretations>`), you'll need to memorize your
piece ahead of time.  There are a variety of techniques for doing so, and
figuring out which ones work best for you will require some experimentation on
your part, as everyone's a bit different.  Here are some general
recommendations.

Break It Up
===========

A first step is taking the whole and breaking it up into smaller chunks.  If
you're memorizing a speech, consider one paragraph at a time, but if it's a
play, consider one scene at a time, for instance, or perhaps break the scene
into smaller chunks.  Start with just the first chunk and work on it till you
have it completely memorized.  Once you're good there, set it aside and move on
to the second chunk.  Work on that until you're good there, and then put the
first two chunks together.  Work on them together until you're confident you've
got it, then move on to chunk number three, etc.

How do you memorize any given chunk?  You do the same thing, but smaller:
break the paragraph up into bite-size pieces.  You'll probably want to break at
sentence boundaries (though not always), and you might want to break within
sentences as well, depending on their length.  Once you've subdivided your
paragraph, start with the first phrase and memorize that.  Once you've got it,
move on to the second.  When you're good there, put the first and second
together.  When you're comfortable with that, move on to the third phrase, etc.

Write It Out
============

Another tip for memorizing a piece is to write it out.  If you've broken it up
into bite-size pieces as mentioned above, then a suggestion would be to write
each phrase on an index card (be sure you number them).  Then you can use these
as flash cards as you work on memorizing.  The act of writing down the words
engages more of your senses and helps to solidify the words into your memory.
Additionally, as you go through your piece in your head, you'll wind up having
mental images of each card, so you're not just remembering the words
themselves, but also what they look like written on the page.

As you work your way through the piece, it can also help to write it out in
abbreviated form using the first letters of every word.  If I were memorizing
Marc Antony's eulogy in Shakepseare's *Julius Caesar*, I would jot down on the
page "FRclmyeIctbCntph," in place of "Friends, Romans, countrymen, lend me your
ears; I come to bury Caesar, not to praise him."  While it doesn't seem like
much, engaging your tactile and visual senses in this way can help solidify
things in your mind.

Record Yourself
===============

A final recommendation would be to record yourself reciting the piece, and then
play it back to yourself regularly.  You may be tempted to think it would be
just as good to listen to someone else's recording of the same piece, but that
likely won't be as effective, as you'll more easily slip from active into
passive listening.  Record yourself with the words in front of you.  Listen to
the recording, going through the words in your head.  After you've done that
for a while, record yourself again, and start listening to the new recording.
At some point, transition to recording yourself speaking from memory instead of
from the script.

Have Someone Feed You Lines
===========================

To test your memory of the piece, have someone feed you lines, and see if you
can pick up where they leave off.  In the beginning, your assistant should
choose lines at the beginning of a paragraph or equivalent chunk of text.  As
you get better, they can pick some sentence in the middle of a paragraph.  To
really test your memory skills, they can start in the middle of a sentence.  Be
sure you keep this fun, though.  If it starts to get stressful, take a break
and come back to it later.
