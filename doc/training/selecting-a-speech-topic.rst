Selecting a Speech Topic
------------------------

:raw-html:`<div align="right">🕑 1 min.</div>`

Before you sit down to write a speech, you first need to figure out what you'll
be speaking about.  Pick something you're passionate about.  If you're going to
be speaking to a room full of people, they'll need to feel your energy and
enthusiasm for your topic.  When writing a
:doc:`Persuasive Oratory </events/platform-speeches/persuasive-oratory>`,
consider questions like:

* What's something wrong with the world that you desperately want to fix?
* What's something commendable that you want to encourage others to do?
* If you only have a few minutes left to live, what's one message that you
  absolutely must pass on before you go?

When writing an
:doc:`Informative Oratory </events/platform-speeches/informative-oratory>`,
consider questions like:

* What's something really neat that you want to share with others?
* What's something you can't believe you never knew before?
* How can you improve someone's life by cluing them in to something they've
  been oblivious to?

In some events, you won't have the luxury of selecting your topic from out of
thin air.  In
:doc:`Congressional Debate </events/debates/congressional-debate>`, you'll be
speaking for or against a particular bill that you probably didn't write.  In
the other debate events, you'll be either affirming or negating a resolution
that was set in advance.  In these instances, though, you still get to bring
your own perspective to the discussion at hand, so while you don't have
complete and total freedom in deciding what you'll talk about, make sure what
you decide to focus on is something you can fully support with zeal.

Finding Your Voice Video Lesson
===============================

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/PnauY2dGQE4?start=338" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>
