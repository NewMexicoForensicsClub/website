Structuring Your Speech
-----------------------

:raw-html:`<div align="right">🕑 7 min.</div>`

Once you have your subject matter clear in your mind, it's time to start
putting words to the page.  In terms of its structure, a speech will have three
main components: the *introduction*, the *body*, and then the *conclusion*.
Each of these sections, in turn, will also consist of certain elements.  Let's
walk through each of them in detail.

Introduction
============

The *introduction* is just that: a means of introducing yourself and your
topic to your audience.  It's what sets the stage for the next few minutes, and
lets your listeners know why they really want to pay attention to what you have
to say.  Time is precious, and your introduction needs to convey to your
audience that you deeply value the time they're about to lend you.  How do you
do that?

Hook
^^^^

The first component to include in the introduction is what's known as the
*hook* (sometimes also known as the *attention-getting device* or AGD).  This
is what grabs your listeners' attention right off the bat.  Sometimes it's a
thought-provoking quote related to your thesis.  Other times it might be a
probing question to get the audience to think.  Perhaps it's an engaging story
that pulls your audience in and leaves them wanting more.  Regardless of the
type you use, the hook is what gets the audience to set aside whatever it is
they've been thinking about and focus their attention on you for the next few
minutes.

Thesis
^^^^^^

Once you have everyone's attention, it's time to tell them what you're going to
be talking about.  You do this through the use of a
`thesis statement <https://en.wikipedia.org/wiki/Thesis_statement>`_, which you
should already be familiar with from writing essays.  It's your main point,
your reason for speaking---it's what you want everyone to remember as they walk
away.

Outline
^^^^^^^

Now that your audience has an idea of what you're going to talk about, the next
step is to tell them how you're going to address the issue.  If you've heard
the expression, "Tell your audience what you're going to tell them, tell them,
and then tell them what you told them," the outline is the first step in the
process.  You want to paint a clear picture in each listener's mind of where
you'll be going in the next few minutes, so they can start forming a mental
model into which they can later place the information you'll give them.

You're going to want to back up your thesis statement with a handful of
supporting points, and each of these points should have a short (three to seven
words or so) but descriptive *tag line*.  Using these tag lines in your
introduction helps your audience to take notes, at least mentally, about what
you're saying.  Later on you'll be able to refer back to them to continue to
remind your audience of the organization of your speech.

Motivation
^^^^^^^^^^

If the thesis is *what* you're talking about, and the outline gives an idea of
*how* you're going to address the issue, another question you need to answer
for your audience is "*Why?*"  Why should they care?  Why should they listen
intently for the next few minutes?  What's in it for them?  You may have
already answered some of these questions with the hook, but perhaps not.  Make
sure you don't leave the introduction without giving the audience motivation to
tune in, though, otherwise you may be left with disengaged listeners, and
that's *no bueno*.

Body
====

The *body* of your speech is where all the meat is.  As mentioned before,
you'll want to support your thesis with a handful of supporting points or areas
of analysis, which will contain some of the following elements.

Claim
  What exactly is the point you're trying to make?  Think of this like a
  mini-thesis of sorts.
Support
  What support do you have for your claim?  It's one thing for you to say, "I
  think something," but another thing to say, "Here are some people who have
  thought something over the years, and I heartily agree with them."  You can
  support your claims with things like quotes, research, logical reasoning,
  philosophy, current events, etc.  See :doc:`using-evidence-in-a-speech` for
  more details.
Tie-In
  How does this point relate to your thesis?  The connection may be clear in
  your mind, but make sure it's concrete for your hearers as well.
Significance
  Why are you making this point?  What impact should this point have on your
  audience.
Transitions
  How do you transition from one point to the next?  You can refer back to the
  outline and help the audience understand your thinking as you flow from one
  thought to another.

How many supporting points should you have?  That'll depend on a few factors.
For one, keep in mind your audience and their ability to remember what you've
said after you're done speaking.  Generally you'll want to limit yourself to
two to four areas of analysis---any more and your listeners will have a hard
time remembering all the points you're trying to get across; any less and it'll
seem like your thoughts aren't fully supported.  The other significant factor
is the time you have to deliver your speech.  If you only have a few minutes to
speak, you might want to restrict yourself to fewer, better-developed points,
rather than trying to fit in more ideas, but then not having time to develop
them fully.

Conclusion
==========

After making all your points, it's finally time to draw things to a close with
your *conclusion*.  This is the "then tell them what you told them" part of the
adage.  Remind your audience of the journey you've been on together in the past
few minutes.  Remind them of your thesis, and how each of your points supported
it.  The bits and pieces of the introduction are generally recapitulated here.
If possible, relate back to your hook to bring your audience full circle.  You
may also wish to include the following:

Thanks
  Make sure your audience knows how much you've appreciated their attention.
Call to Action
  Help your audience understand what's next for them after having listened to
  you.  It may be to continue ruminating on your topic, or to simply think
  differently about something.  Then again, you may have a specific course of
  action in mind for them---something for them to actually go and do.

Vehicle
=======

.. note::

   This last component of your speech is a more advanced one, so if this is
   your first time writing a speech, don't worry about it for now.  After you
   have a few under your belt, then come back to this and see how you can
   incorporate it into your next one.  Note that it's not an additional section
   to add to your speech, but rather a mechanism to use throughout.

A *vehicle* is a device that gets you from where you are to where you want to
go.  In the context of delivering a speech, "where you are" is wherever your
audience is before you start speaking---not physically, but rather
intellectually, spiritually, emotionally, etc.  "Where you want to go" is your
desired end state for your audience once you've finished speaking.  What do you
want them to think, believe, or feel as they walk away?  What do you want them
to do in the days and weeks to come?

Once you have a good idea of your audience's starting and ending points, you
have some questions to ask and answer:  How do we get from here to there?  What
stops do we need to make along the way?  Think of the vehicle as a kind of
extended metaphor that helps convey your audience on the journey you intend for
them to take with you.  In practice, this extended metaphor winds up permeating
your speech, being included in the hook, outline, and any transitions between
points.

As an example, let's consider a speech in which you're trying to alert your
audience to the dangers of worry and anxiety.  As a vehicle for the delivery of
your analysis, you could consider using Disney's *The Lion King*.  For your
hook, start off singing your best rendition of the first little bit of *Hakuna
Matata*---that'll get people's attention!  From there, you could use Simba's
journey as a means of walking your audience through your points:  starting with
his upbringing and the onset of his troubles, moving on to his introduction to
a new problem-free philosophy via Timon and Pumbaa, and then concluding with
his realization that you only really get past the problems of yesterday by
confronting them head on.  As you wrap up in your conclusion, finish with the
last little bit of *Hakuna Matata*, and then you've come full circle.  It's
important to note that this speech isn't about Simba's story; rather, you're
using that as an illustration that pervades the speech for the sake of giving
structure to the story you're trying to tell.

Structuring Your Speech Video Lesson
====================================

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/fOMR6213zgs?start=268" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>
