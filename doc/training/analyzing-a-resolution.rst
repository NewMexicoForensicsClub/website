Analyzing a Resolution
----------------------

:raw-html:`<div align="right">🕑 4 min.</div>`

When it comes to academic debate, the topic to be debated is what's known as
the *resolution*.  For instance:

.. admonition::  Sample Policy Debate Resolution

   Resolved:  The European Union should substantially reform its immigration
   policy.

.. admonition::  Sample Value Debate Resolution

   Resolved:  Civil disobedience in a democracy is morally justified.

They always begin with the word "resolved," as if they were being presented as
a piece of legislation.

Identifying the Affirmative and Negative Positions
==================================================

An academic debate consists of two opposing sides, typically called the
*affirmative* and *negative* (though some particular debate styles use other
terms).  Sometimes these sides have two debaters each (e.g.,
:doc:`/events/debates/policy-debate`), sometimes only one (e.g.,
:doc:`/events/debates/value-debate`), and sometimes it varies.  Regardless of
the number of debaters or what the different sides are called, the first step
in analyzing a resolution is to identify what the affirmative and negative
positions are.  Often the affirmative position is stated in the resolution
itself, and the negative position is its inverse.  For instance:

* Resolved:  On balance, the benefits of creating the United States Space Force
  outweigh the harms.

  * Affirmative Position:  On balance, the benefits of creating the United
    States Space Force outweigh the harms.
  * Negative Position:  On balance, the harms of creating the United States
    Space Force outweigh the benefits.

* Resolved:  The United States federal government should substantially increase
  its protection of water resources in the United States.

  * Affirmative Position:  The United States federal government should
    substantially increase its protection of water resources in the United
    States.
  * Negative Position:  The United States federal government should *not*
    substantially increase its protection of water resources in the United
    States.

Identifying Limiting Terms
==========================

Once you have a clear picture of what the two positions are in the debate, it's
important to identify any limiting terms in the resolution.  For the sake of
having a focused discussion, the resolution limits the boundaries of the
debate.  You don't want to start an argument about the benefits of the
International Monetary Fund, and then find moments later that you've wandered
into debating the pros and cons of low-frequency active sonar technology.  Such
wandering dialogues are not as productive as ones that remain focused, and the
resolution provides that focal point for us.

In addition to the general topic in the resolution itself, the resolution may
also contain certain *limiting terms* that help to constrain the debate even
further.  For instance, in the sample policy debate resolution above, we're
limiting the debate to discussion of the immigration policy of *the European
Union*, not the United States or any other country.  In the sample value debate
resolution, we're only concerned with *civil* disobedience, not, e.g., rioting,
and only concerned with it taking place *in a democracy*, not under any other
form of government.  These limiting terms are important to identify so we know
what exactly we are, and are not, talking about.

Defining Terms
==============

Once you have a good idea of the boundaries the resolution is setting for the
debate, you want to make those boundaries as clear as possible by defining your
terms.  Confusion abounds and you fail to achieve a meeting of the minds when
people converse holding alternate definitions of shared vocabulary.  If you're
using the same words, but meaning different things by them, the conflict in the
debate won't be focused on the ideas at play, and you'll instead wind up just
talking past each other.  Take time to identify any key terms in the resolution
and look up definitions for them.

What's Next?
============

Now that you're finally completely clear on the boundaries of the debate to be
had, the next step is to dig into research.  Don't worry about constructing an
affirmative or negative case just yet.  Before you form your arguments on
either side, you need to spend some time digging into the topic itself.  Check
out :doc:`conducting-research` for where to look and how to catalogue what you
find.  Once you've marinated in the topic for a while, you can then start to
figure out what kind of arguments you might make on either side.

Applications Outside Academic Debate
====================================

So far all that we've talked through here has been in the context of academic
debate; however, the same principles apply in any conversation with some clash
or disagreement at its heart.  You want to:

* Be clear what the topic at hand is, and what the opposing viewpoints are.
* Clearly dilineate between what you are and are not talking about.
* Ensure you come to an understanding on the meanings of the terms you're
  using.

Failure to do so is a recipe for frustration and confusion.  Here's a
recommendation from the world of contract negotiation:  "Spend about 30 seconds
saying what you need to say, and then the next 30 minutes clarifying what you
didn't mean by that."
