Outlining Your Speech
---------------------

When giving extemporaneous speeches (any of the
:doc:`limited preparation </events/limited-preparation-speeches>` or
:doc:`debate </events/debates>` events), you'll need to figure out what you're
going to say relatively quickly.  It can help to
:download:`use a template </../files/speech-outline-template.pdf>` to quickly
sketch out all your points so you can see the overall structure of your speech
at a glance.  Such a template is intended to help you keep in mind everything
you know about
:doc:`structuring your speech </training/structuring-your-speech>`.

.. note::

   Though not included explicitly in the template, remember to state your
   outline in the introduction so your audience has an idea of where you're
   going, use transitions between different sections of your speech, and
   recapitulate the components of your introduction in your conclusion.

.. note::

   Make sure each of your points/claims has a tag line---something short,
   sweet, and to the point that will be easy for your audience to remember.
