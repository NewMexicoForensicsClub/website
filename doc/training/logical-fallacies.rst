Logical Fallacies
-----------------

:raw-html:`<div align="right">🕑 13 min.</div>`

The following video is a quick overview of a few dozen of the most common
logical fallacies you'll run into.  For a more comprehensive list, see `this
list of fallacies on Wikipedia
<https://en.wikipedia.org/wiki/List_of_fallacies>`_.

.. raw:: html

    <br>
    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/Qf03U04rqGQ" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Fallacy of Composition
======================

Taking what's true of some of the parts and asserting it's true of the whole as
well.  For example, Josiah and Emma are awesome ultimate frisbee players,
therefore the team they play on is also awesome.

Fallacy of Division
===================

The opposite of the fallacy of composition, this is taking what's true of the
whole and asserting it's true of all the parts as well.  For example, this
dinner was absolutely amazing, therefore each and every dish in it was
absolutely amazing.

The Gambler's Fallacy
=====================

The thought that streaks of good or bad luck exist.  If playing the slot
machines at a casino, the thought is that one that's been doing well is *hot*
and will continue to do well.  The flip side of this fallacy is when it works
in the other direction; that is, if I flip a coin and get five heads in a row,
*surely* the next one will be tails.  Past behavior is not necessarily a
predictor of future outcomes.

Tu Quoque
=========

Otherwise known as the "Who are you to talk?" fallacy, or the appeal to
hypocrisy, this happens when instead of attacking your opponent's argument, you
point out how their behavior doesn't line up with their argument.  For
instance, if someone was arguing that you should donate a certain percentage of
your income to charity, but you know they don't hold themselves to the same
standard they're arguing for, saying something like "You don't do that,
*therefore I shouldn't*," is a *tu quoque* fallacy.

.. note::

   This shouldn't prevent you from asking the legitimate question, "Hang on,
   why do your actions not line up with your argument here?"  There may be a
   legitimate reason behind it, or your opponent may not have realized the
   disconnect.  Either way, clarification is helpful to the discussion.

Strawman
========

Instead of attacking the argument itself, you construct a misrepresentation of
the argument and attack that instead.  The misrepresentation resembles the
actual argument in the sense that a scarecrow resembles a human.  For example,
if someone argues for establishing a declaratory nuclear policy of no first use
in the United States, and their opponent brings up all the potential harms of
complete nuclear disarmament, the opponent has set up a strawman (complete
nuclear disarmament) in place of the argument itself (no first use policy) and
is attacking that strawman instead.

Ad hominem
==========

Latin for "to the person", this is where instead of attacking the argument
itself, you attack the person making the argument.  `A perfect example
<https://www.youtube.com/watch?v=RseLZ9LqQv0>`_ comes from *The Emperor's New
Groove*, where Kronk's shoulder demon attacks his shoulder angel with, "Look at
that guy.  He's got that sissy stringy music thing[...] and that's a dress."
While a person's background should be carefully considered in evaluating their
arguments, you always want to make sure you're attacking the idea itself.

.. note::

   The *tu quoque* fallacy is a form of an *ad hominem* attack.

Genetic Fallacy
===============

Thinking an argument is true or false simply because of where it comes from.
An example would be thinking someone's claim must be true because they have a
PhD from Harvard.  This fallacy can extend beyond the person making the claim,
though, to other aspects of the idea's provenance.  For instance, if an
argument comes from a particular culture, geographic region, time period in
history, etc., then it must be right/wrong.

.. note::

   It is worthwhile to question whether or not a source can legitimately speak
   with authority on a particular topic, though.

.. _fallacious_appeal_to_authority:

Fallacious Appeal to Authority
==============================

This is where you appeal to someone who isn't necessarily qualified to speak
authoritatively on the matter under consideration.  For example, your favorite
movie star says you should vote in favor of a particular proposition, but they
don't have the expertise to evaluate the long-term societal implications of the
two options before you.

Red Herring
===========

The fallacy of distraction.  Rather than focus on the real issue, divert the
attention to something only vaguely related.  For example:

Senator 1
   "Regarding the antiabortion amendment, do you not care at all
   about the unborn children whose lives are being indiscriminately blotted
   out?"
Senator 2
   "If you care so much about lives being blotted out, why don't you
   support our gun-control legislation?"

Appeal to Emotion
=================

Attempting to win an argument by manipulating the emotions of the audience
rather than appealing to facts and logical reasoning.  For example, "Your
honor, my client has spent his entire childhood growing up on the streets.  He
lost his parents, has no other family; indeed his only friend in life is a
monkey.  At the end of his rope, he swiped a loaf of bread to try to stay alive
for just a few more days.  How can we judge him for that?"

.. note::

   This does not mean that our argumentation should be emotionless.

Appeal to Popularity
====================

Also known as jumping on the bandwagon, this is an argument that something is
true or false because "everyone else" agrees.  History is full of examples of
this fallacy, often with disastrous consequences.  For instance, leading
medical experts agree that to treat this illness the humours must be restored
to a proper balance through bloodletting.

Appeal to Tradition
===================

Closely related to the appeal to popularity, this fallacy is when you assume
something is right because that's what people have thought for ages.  For
instance, the earth is the center of the universe.

.. note::

   It may be the case that something we've thought for a really long time is
   true (e.g., God made the heavens and the earth), but if you fail to develop
   the argument beyond, "Everyone's thought this for ages!" you're falling
   victim to this fallacy.

Appeal to Nature
================

Assuming something's good because it's perceived as natural, or assuming
something's bad because it's perceived to be unnatural.  For instance,
antibiotics are unnatural, so they're bad for you.

Appeal to Ignorance
===================

If you can't prove my claim to be false, then it must be true (and *vice
versa*).  For instance, if you can't prove your innocence, then you must be
guilty.  In argumentation, the burden of proof lies on the one making the
claim.  This fallacy attempts to shift the burden of proof onto the opponent
instead.

Begging the Question
====================

The phrase that something "begs the question" is often misused in conversation.
Typically people mean that the preceding statement naturally leads to the
following question, but that's not what the phrase means.  Begging the question
is when your premise, rather than supporting the claim, instead assumes it to
be true.  For instance, people who disagree with critical theory are simply
currying favor with the cultural hegemony.

Equivocation
============

Using the same word to mean two different things.  For example, the evolution
of species in small seasonal ways proves that humans evolved from lower
primates.  In the first sense, evolution means seasonal adaptation, while in
the second it means all life developed from single-celled organisms via
natural selection alone.

False Dichotomy
===============

Also known as the black or white fallacy, this is when something is either one
way or another, and there is no middle ground whatsoever in between.  For
instance, either you agree with everything I say, or you're a racist, or you
agree with everything I say, or you're trying to kill my grandma.

Middle Ground Fallacy
=====================

A bit of an inverse of the previous one, this occurs when some people think one
thing, some people think another, therefore the answer *must* be somewhere in
between.  For instance, if some people think the best thing for children is for
them to be educated by their parents in the home, while others think the best
thing is for them to be brought up by professional educators in a school, then
the right answer must be that children should be homeschooled some of the time
and taught in school the rest of the time.

Decision Point Fallacy
======================

Also known as the continuum fallacy or Sorites paradox, this fallacy occurs
when you can't draw a hard and fast line between two states, and therefore
conclude that you will never be able to distinguish between them.  For
instance, if someone must be 18 years old to make medical decisions without
parental consent, and if there's really no difference between that and 17 years
and 364 days, then 11-year-olds should be able to agree to receiving a vaccine
without parental consent.  (See `DC bill 23-171
<http://app.cfo.dc.gov/services/fiscal_impact/pdf/spring09/FIS%20Bill%2023-171%20Minor%20Consent%20for%20Vaccinations.pdf>`_.)

Slippery Slope Fallacy
======================

Arguing that one small step inevitably leads to another, which inevitably leads
to another, etc., which inevitably leads to certain doom.  The typical example
in debate is when all roads lead to nuclear war.  For instance, by taking the
initiative in standing up a Space Force, the US communicates to the world that
we intend to dominate outer space the same way we dominate the planet.
International relations will degrade rapidly, and with nuclear tensions with
Iran and China already at an all-time high, this will inevitably trigger a
nuclear holocaust.

Hasty Generalizations
=====================

This is otherwise known as using an anecdote to try to prove a general point.
The problem is you can find an anecdote to back up anything.  A recent example
I heard:  "Well I went to high school with a kid who was homeschooled up till
then, and he was really far behind in things like algebra, so there's something
to the argument that homeschooling doesn't serve kids as well as public
school."

Faulty Analogy
==============

Assuming that because two things are alike in some ways, they are also alike in
other ways.  For instance, there's no problem with a professional consulting a
reference book when doing their job, so I should be able to use my books when
taking my exams.

Burden of Proof
===============

Rather than proving an assertion yourself, you assume it's true until someone
proves you wrong.  This is related to the appeal to ignorance mentioned above.
For instance, if someone tells you that universal mask mandates save lives
(without giving you any evidence to support that claim), you say that you don't
think they do, and they respond with something like, "Can you prove that they
don't?"

Affirming the Consequent
========================

This is a formal fallacy where someone takes a logical statement, like A
implies B, and flips the order around; that is, B implies A, which may not
necessarily be true.  There is also the tendency to loosen the restrictions of
the formal fallacy and say, "A *might* imply B, and B is true, therefore A is
as well."  This fallacy is common in everyday thinking, often due to a failure
to consider other causes.  For instance, if you're sick with COVID-19, you'll
likely experience flu-like symptoms.  You have a bit of a fever, therefore you
have COVID-19.

Denying the Antecedent
======================

Another formal fallacy, this is where one takes a logical statement, like if A
is true then B is true, and negates both sides of it to say if A is false then
B must be false, which is not necessarily the case.  For instance, the right to
bear arms allows criminals to use firearms.  If we remove the right to bear
arms, then criminals won't use firearms.

Moving the Goalposts
====================

This fallacy occurs when someone makes a claim, evidence is presented that counters that specific claim, the person making the claim dismisses the evidence presented and demands more or greater evidence.  For instance:

Civic Leader
    Universal mask mandates protect both you and those you're around.
Constituent
    Here's decades of public health policy saying they're not recommended once a virus is out in the population.
Civic Leader
    That information is old.  The most recent recommendations are to wear masks in public.
Constituent
    Here's a study over the last year indicating next to no efficacy.
Civic Leader
    That's only a single study.  We don't yet have corroborating evidence.
Constituent
    Here's another two dozen studies supporting the same conclusions.
Civic Leader
    Well we're going to keep using them anyway just to be safe.

False Cause
===========

Assuming that correlation implies causation.  Again, this happens often when
failing to account for other possible causes.  For instance, the level of
carbon dioxide in the atmosphere has generally been increasing over the last
hundred years.  The global temperature trend has also been positive in that
same timeframe, so human carbon dioxide emissions are contributing to the
warming of the planet.  As of yet there's no empirical way to test that
hypothesis, so before assuming the argument is correct, consider if there might
be other factors at play.

.. note::

   One cannot say that since the argument above is a logical fallacy, we
   therefore bear no responsibility to care for our planet.

Loaded Question
===============

When you ask a question that contains an implicit assumption, usually putting
the person being questioned in a defensive or otherwise unfavorable position.
The presupposition that's built in usually also protects the one asking the
question from accusations of false claims.  For instance, "Are you going to
vote for that incompetent candidate?" or "Wait, are you one of those
science-hating creationists?"

.. note::

   This tactic is only fallacious if the built-in assumption is not verified.
   There's nothing wrong with asking a question with an implied assumption of a
   widely accepted fact.

No True Scotsman
================

A particular form of moving the goalposts, this typically happens when you
makes a claim about a particular people group, an opponent presents an example
invalidating your claim, and then you claim the person used in the example is
not a *true* member of the people group.  For instance:

Person 1
    No black people vote for Trump.
Person 2
    I'm black and I voted for Trump.
Person 1
    Then you ain't black.

Alternatively:

Person 1
    No Christian could support the Democrat party platform in 2020.
Person 2
    I'm a Christian, and I voted for Biden.
Person 1
    Then you're not really a Christian.

Personal Incredulity
====================

A particular form of the appeal to ignorance, this happens when you can't or refuse to believe something and therefore conclude it must not be true.  For instance:

Person 1
    The United States is on the brink of embracing a totalitarian regime.
Person 2
    What?  No way!  That's ridiculous.  I can't believe that.  (And therefore I
    won't look into any of the evidence you provide.)

The Fallacy Fallacy
===================

This occurs when an argument contains a logical fallacy, or has just been
poorly argued, and you conclude that the claim must be false.  The quality of
the argumentation does not actually say anything about the truth status of the
claim.  For instance, "We should strive for cleaner automobile emissions,
because fossil fuel emissions are warming the planet."  The motivation is an
example of the false cause fallacy, but that doesn't mean there aren't
additional legitimate reasons for supporting cleaner car emissions.
