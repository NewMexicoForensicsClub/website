Crafting an Argument
--------------------

:raw-html:`<div align="right">🕑 15 min.</div>`

.. contents::  Contents
   :local:

The vast majority of argumentation that happens in the world today is
ineffective.  That shouldn't be of any surprise to you.  Whether the context is
politics, religion, the economy, philosophy, sports teams, the environment, or
movies and television, arguments tend to boil down to opposing sides lobbing
contradictory assertions back and forth at each other, and little progress is
made toward achieving a meeting of the minds and ascertaining truth.  This is
likely due to a whole host of issues---our inability to listen carefully, think
critically, find common ground, etc.---but part of the reason is our arguments
are incomplete.

.. sidebar:: The Six Components of an Argument

   1. Claim
   2. Data
   3. Warrant
   4. Qualifications
   5. Backing
   6. Rebuttals

Believe it or not, an argument actually consists of six separate components,
the first three of which are absolutely essential, and the latter three of
which are so beneficial in terms of persuasiveness that you never want to be
without them.  This particular way of structuring an argument is known as the
*Toulmin Model* of argumentation, named after the British philosopher `Stephen
Toulmin`_.  He didn't invent the method; rather, he just happened to be the
first person to set it all down in writing in his book |the_uses_of_argument|_.

.. _Stephen Toulmin:  https://en.wikipedia.org/wiki/Stephen_Toulmin
.. |the_uses_of_argument| replace::  *The Uses of Argument* (1958)
.. _the_uses_of_argument:  https://smile.amazon.com/Uses-Argument-Stephen-Toulmin/dp/0521534836/ref=sr_1_1?crid=UTELIWHJEZ4Q&dchild=1&keywords=the+uses+of+argument+toulmin&qid=1616437658&sprefix=the+uses+of+argument%2Caps%2C229&sr=8-1

Let's walk through each of the components in turn.

The Essentials:  Claim, Data, & Warrant
=======================================

The first essential element of an argument is the *claim*, which is whatever
you are claiming is true.  It's

* the main point you're trying to get across,
* the thing you want people to walk away believing,
* the conclusion, the merits of which you're trying to establish,
* the assertions lobbed back and forth mentioned above,
* etc.

.. sidebar:: Gathering data

   See :doc:`conducting-research` for how to go about finding and assessing the
   credibility of this data.

A claim all by itself isn't an argument, though, because you don't have any
reason to believe it yet.  That's where the second essential piece comes into
play: the *data*.  These are the facts we appeal to as a foundation for the
claim---the observations, expert testimony, prior experience, logic,
philosophy, history, etc., that supports it.  With this addition, our argument
is starting to take shape.

.. tikz::
   :alt: Data supporting a claim
   :libs: arrows.meta,positioning

   [
     element/.style={
       rectangle,
       draw=black,
       rounded corners=2px,
       text height=height{"Q"},text depth=depth{"Q"},
       font=\sffamily,
     },
     transition/.style={
       font=\sffamily,
     },
   ]
   \node[element] (data) {Data};
   \node (point) [right=of data] {};
   \node[transition] (so) [right=of point] {so};
   \node[element,right] (claim) at (so.east) {Claim};
   \draw[->,>={Stealth[round]}] (data.east) -- (so.west);

However, the argument is actually incomplete until you include the third
essential component---the *warrant*---which is whatever ties the data to the
claim (facts, rules, principles, inference, etc.).  The data alone are just a
statement of fact, not necessarily related to the claim.  The warrant is the
analysis followed to connect the two, and is often the component of an argument
that gets accidentally omitted.  Without it, though, we're unable to reason
about the claim.

.. tikz::
   :alt: The essential elements of an argument
   :libs: arrows.meta,positioning

   [
     element/.style={
       rectangle,
       draw=black,
       rounded corners=2px,
       text height=height{"Q"},text depth=depth{"Q"},
       font=\sffamily,
     },
     transition/.style={
       font=\sffamily,
     },
   ]
   \node[element] (data) {Data};
   \node (point) [right=of data] {};
   \node[transition] (so) [right=of point] {so};
   \node[element,right] (claim) at (so.east) {Claim};
   \node[transition] (since) [below=of point] {since};
   \node[element,below] (warrant) at (since.south) {Warrant};
   \draw[->,>={Stealth[round]}] (data.east) -- (so.west);
   \draw[-] (since.north) -- (point.center);

.. sidebar:: Logic texts

   If you're looking for a good logic book, check out the :ref:`Logic
   <logic_books>` section of our :doc:`../resources` page.

At this point, if you've taken a course in logic, you might notice a similarity
to a logical syllogism, where the warrant corresponds to the *major premise*,
the data to the *minor premise*, and the claim to the *conclusion*.  The
canonical example of a logical syllogism is:

* **Major Premise:**  All men are mortal
* **Minor Premise:**  Socrates is a man
* **Conclusion:**  Socrates is mortal

This similarity is worth noting, but it's also worth pointing out that the form
of argumentation we're walking through here is more general than what you learn
in a logic class.  Out in the real world, the vast majority of arguments you
run into don't fit into the nice tidy form of a syllogism, but you still need
to be able to reason about and debate them.  This more general form helps you
to do that.

Let's walk through some more examples to start getting comfortable with these
three essential components.  In the first case, I claim that my hair is not
blonde.  Okay, but what data do I supply in support of my claim?  Well, my hair
is actually brown, which is an observable fact.  But then what on earth is the
warrant?  If you can see that my hair is brown, isn't it obvious that my hair
isn't blonde?  Yes, and in many instances the warrant can go unstated, but
let's spell it out to be explicit:  Something that is brown is not blonde.

Another example:  I claim that I am a U.S. citizen, and I support the claim by
telling you I was born in the United States.  That data is a matter of
historical fact that you could look into, or you could just take my word for
it.  The warrant that completes the argument is the fact that someone born in
the United States is a U.S. citizen.

One last example, to illustrate that sometimes the warrant is difficult to
discern:  I claim that the sky is blue.  You ask me to prove it to you, and we
both walk outside and see that the sky is blue.  What's the warrant here?  It's
a whole host of things that we take for granted:

* The human eye works in a particular way.
* Light works in a particular way.
* My eyes can accurately perceive the color of something I'm looking at.
* Your eyes perceive color in the same way, so we can agree on the color of
  what we're looking at.
* All of the above is consistent across time, space, and individuals.
* Etc.

Now you may have noticed that in our examples so far that the data and warrants
appear to both be facts, though of different types.  The facts supplied in the
data are *specific* to the claim at hand---my hair is brown, I was born in the
U.S., we observe the sky to be blue, Socrates is a man.  They're only useful
when assessing a particular claim---whether my hair is blonde, whether I'm a
U.S. citizen, whether the sky is blue, whether Socrates is mortal.  On the
other hand, the facts contained in the warrant are *general*---something brown
is not blonde, someone born in the U.S. is automatically a citizen, light and
optics work in a particular way, all men are mortal---and applicable to any
number of claims.

When you're the one making an argument, you'll want to be sure these three
essential elements are in place, but what do you do if someone else is making
the argument and you're unclear about the different pieces?  That's where
:doc:`asking questions <asking-questions>` comes into play.  Are you confused
about what exactly the claim is or what it means?  Ask questions to
:ref:`clarify terms <clarifying_terms>` (e.g., "What do you mean by that?").
Do you need more information about the data to be able to assess its validity?
Ask questions to :ref:`clarify sources <clarifying_sources>` (e.g., "Where'd
you get your information?").  Does the warrant seem unclear to you?  Ask
questions to :ref:`clarify analysis <clarifying_analysis>` (e.g., "Why do you
think that?").  The discipline of making compelling arguments and dismantling
poor ones is closely tied to the art of effective questioning.

Qualifications
==============

Now we move on from the essential elements of an argument to the first "not
essential, but so beneficial you should absolutely use it" piece.  The
*qualifications* indicate the degree of certainty you have that the data and
warrant actually do imply the claim, and they range on a spectrum from no
confidence whatsoever to supremely confident.

.. tikz::
   :alt: A qualified argument
   :libs: arrows.meta,positioning

   [
     element/.style={
       rectangle,
       draw=black,
       rounded corners=2px,
       text height=height{"Q"},text depth=depth{"Q"},
       font=\sffamily,
     },
     transition/.style={
       font=\sffamily,
     },
   ]
   \node[element] (data) {Data};
   \node (point) [right=of data] {};
   \node[transition] (so) [right=of point] {so};
   \node[element,right] (qualifications) at (so.east) {Qualifications};
   \node[element,right] (claim) at (qualifications.east) {Claim};
   \node[transition] (since) [below=of point] {since};
   \node[element,below] (warrant) at (since.south) {Warrant};
   \draw[->,>={Stealth[round]}] (data.east) -- (so.west);
   \draw[-] (since.north) -- (point.center);

Let's look at some examples.

.. admonition:: Categorical Example

   I'm absolutely a mammal, because I'm a human, and all humans are mammals.

   * **Claim:**  I am a mammal.
   * **Data:**  I am a human.
   * **Warrant:**  All humans are mammals.
   * **Qualifications:**  Necessarily, always, totally, without a doubt,
     absolutely, etc.

   The truth of the argument here is a matter of category; that is, the data
   and warrant automatically imply the claim.  This particular subset of
   arguments is where logical syllogisms fit.

.. admonition:: Probabilistic Example

   My friend Abdullah is almost certainly a Muslim, because he's from Saudi
   Arabia, and Saudis are Muslims.

   * **Claim:**  Abdullah is a Muslim.
   * **Data:**  He's from Saudi Arabia.
   * **Warrant:**  Someone from Saudi Arabia is a Muslim.
   * **Qualifications:**  Probably, most likely, almost certainly, etc.

   The truth of the argument here is a matter of probability; that is,
   statistics can give us a degree of certainty, but not complete assurance,
   that the data and warrant imply the claim.

.. admonition:: Circumstantial Example

   My friend Tom is probably a British subject, because he was born in the
   U.K., and folks born there are often British.

   * **Claim:**  Tom is a British subject.
   * **Data:**  He was born in the U.K.
   * **Warrant:**  Someone born in the U.K. is a British subject.
   * **Qualifications:**  Maybe, sometimes, often but not always, etc.

   The truth of the argument here is a matter of circumstances; that is,
   whether the data and warrant imply the claim very much depends on the
   details of the situation.

But why should you go through this extra effort of adding qualifications to the
arguments you make?  Because most humans are naturally argumentative, even if
they don't vocalize their arguments, but just keep them to themselves.  When
you say something as simple and straightforward as, "Abdullah's a Muslim
because he's from Saudi Arabia," a good portion of your audience is already
thinking things like, "But how do you know?  Surely there are some Saudi
Arabians who aren't Muslims.  Why should I listen to anything else you have to
say if you're ignorant of such a possibility?"  Inserting qualifications into
your argument is the first step in communicating to your audience, before such
doubts even pop into their heads, "Hang on, I know you may have questions and
be thinking of various circumstances in which my argument doesn't apply, but
I've thought through all that already and have really good answers, so please
hear me out."

Backing
=======

The next step in demonstrating that you've thought through any counterarguments
that might come up is to back up your warrant with additional supporting data.
We call this the *backing*, to distinguish it from the data we've already
discussed.  If you look back at all the warrants in all our examples so far,
you'll note that they are all actually truth claims, and your audience would
be justified in asking, "Yes, but how do you know?"  While the data is the
evidence used to support the specific claim or conclusion of the argument, the
backing is whatever additional evidence is needed to support the general claim
contained in the warrant.

.. tikz::
   :alt: Backing up the warrant
   :libs: arrows.meta,positioning

   [
     element/.style={
       rectangle,
       draw=black,
       rounded corners=2px,
       text height=height{"Q"},text depth=depth{"Q"},
       font=\sffamily,
     },
     transition/.style={
       font=\sffamily,
     },
   ]
   \node[element] (data) {Data};
   \node (point) [right=of data] {};
   \node[transition] (so) [right=of point] {so};
   \node[element,right] (qualifications) at (so.east) {Qualifications};
   \node[element,right] (claim) at (qualifications.east) {Claim};
   \node[transition] (since) [below=of point] {since};
   \node[element,below] (warrant) at (since.south) {Warrant};
   \node[transition] (because) [below=of warrant] {because};
   \node[element,below] (backing) at (because.south) {Backing};
   \draw[->,>={Stealth[round]}] (data.east) -- (so.west);
   \draw[-]
     (since.north) -- (point.center)
     (because.north) -- (warrant.south);

Let's revisit all our examples to see how backing lends additional credibility.

.. admonition:: Categorical Example

   * **Warrant:**  All humans are (necessarily) mammals.
   * **Backing:**  The `Linnaean taxonomy`_ classifies humans `under the
     mammalia class`_.

.. admonition:: Probabilistic Example

   * **Warrant:**  Someone from Saudi Arabia is (probably) a Muslim.
   * **Backing:**  Recent statistics show that Saudi Arabia is 93% Muslim, 4.4%
     Christian, 1.1% Hindu, etc.

.. admonition:: Circumstantial Example

   * **Warrant:**  Someone born in the U.K. is (often, but not always) a British
     subject.
   * **Backing:**  `It depends on a whole host of circumstances`_:  when and
     where you were born, your parents' citizenship, and sometimes their
     professions, etc.

.. _Linnaean taxonomy:  https://en.wikipedia.org/wiki/Linnaean_taxonomy
.. _under the mammalia class:  https://en.wikipedia.org/wiki/Human_taxonomy
.. _It depends on a whole host of circumstances:  https://www.gov.uk/check-british-citizenship

Note, however, that sometimes your backing will be nothing more than common
sense, in which case there's no need to call it out explicitly.  If you supply
the warrant that all men are mortal, or that our eyes, light, perceptions,
etc., work the way we know they do, and the other person responds with, "But
how do you know?", it's a fair guess that they're just out to have some fun at
your expense rather than pursue truth with you.

Conditions for Rebuttal
=======================

As we examined the qualifications and backing, you hopefully started to realize
that there will be scenarios in which your argument doesn't apply.  If you
don't address these at the outset, your audience will wind up thinking
something like, "Well, sure, this guy talks a good talk, but he didn't think
about x, y, and z, so clearly he doesn't know what he's talking about."  If
that happens, you've lost them, so you want to do your best to anticipate any
counterarguments ahead of time and acknowledge that if certain things happen
(e.g., more information comes to light, some analysis is determined to be
faulty, etc.), then certain parts of your argument are invalidated.  These are
the *conditions for rebuttal*.

.. tikz::
   :alt: Conditions for rebuttal
   :libs: arrows.meta,positioning

   [
     element/.style={
       rectangle,
       draw=black,
       rounded corners=2px,
       text height=height{"Q"},text depth=depth{"Q"},
       font=\sffamily,
     },
     transition/.style={
       font=\sffamily,
     },
   ]
   \node[element] (data) {Data};
   \node (point) [right=of data] {};
   \node[transition] (so) [right=of point] {so};
   \node[element,right] (qualifications) at (so.east) {Qualifications};
   \node[element,right] (claim) at (qualifications.east) {Claim};
   \node[transition] (since) [below=of point] {since};
   \node[element,below] (warrant) at (since.south) {Warrant};
   \node[transition] (because) [below=of warrant] {because};
   \node[element,below] (backing) at (because.south) {Backing};
   \draw[decoration={brace,mirror,raise=1ex},decorate]
     (qualifications.south west) -- (claim.south east)
     node (brace) [pos=0.5,anchor=north] {};
   \node[transition] (unless) [below=of brace] {unless};
   \node[element,below] (rebuttal) at (unless.south) {Rebuttals};
   \draw[->,>={Stealth[round]}] (data.east) -- (so.west);
   \draw[-]
     (since.north) -- (point.center)
     (because.north) -- (warrant.south)
     (unless.north) -- (brace.south);

Let's revisit our examples one last time to see how we can intercept our
audience ahead of time and agree with them that there are situations that would
invalidate our arguments.

.. admonition:: Categorical Example

   I'm absolutely a mammal, because I'm a human, and all humans are mammals.

   * **Claim:**  I am a mammal.
   * **Data:**  I am a human.
   * **Warrant:**  All humans are mammals.
   * **Qualifications:**  Necessarily, always, totally, without a doubt,
     absolutely, etc.
   * **Backing:**  The `Linnaean taxonomy`_ classifies humans `under the
     mammalia class`_.
   * **Rebuttals:**  Realistically, there are none, but historically speaking,
     the classification system might've changed recently.  For instance, when
     Linnaeas first came up with it, he thought spiders were insects, so that's
     been corrected.  If you look further back in history to Aristotle's
     classification system, he didn't realize whales were mammals.

.. admonition:: Probabilistic Example

   My friend Abdullah is almost certainly a Muslim, because he's from Saudi
   Arabia, and Saudis are Muslims.

   * **Claim:**  Abdullah is a Muslim.
   * **Data:**  He's from Saudi Arabia.
   * **Warrant:**  Someone from Saudi Arabia is a Muslim.
   * **Qualifications:**  Probably, most likely, almost certainly, etc.
   * **Backing:**  Recent statistics show that Saudi Arabia is 93% Muslim, 4.4%
     Christian, 1.1% Hindu, etc.
   * **Rebuttals:**  Abdullah might've left Islam; he might've been born to
     non-Muslim parents; etc.

.. admonition:: Circumstantial Example

   My friend Tom is probably a British subject, because he was born in the
   U.K., and folks born there are often British.

   * **Claim:**  Tom is a British subject.
   * **Data:**  He was born in the U.K.
   * **Warrant:**  Someone born in the U.K. is a British subject.
   * **Qualifications:**  Maybe, sometimes, often but not always, etc.
   * **Backing:**  `It depends on a whole host of circumstances`_:  when and
     where you were born, your parents' citizenship, and sometimes their
     professions, etc.
   * **Rebuttals:**  If any of the various conditions for British citizenship
     mean he didn't become a British subject at birth; if he renounced his
     citizenship; etc.

.. sidebar:: Note

   The components of the argument don't necessarily need to be presented in
   this order.

At this point, you have the three essential pieces of an argument---the claim,
data, and warrant---in place, and you've strengthened your argument
considerably by including qualifications, backing, and conditions for rebuttal.
All this is in an effort to foster truly effective dialogue.  When working up
your arguments, you may find it beneficial to :download:`use a template
</../files/argument-template.pdf>` to help you organize your thoughts and
ensure you don't omit any of the components.

Real-World Example
==================

Before we wrap up, let's take a look at a real-world example to see what
arguments actually look like out in the wild.  Often they don't present
themselves in the form we've described here, but with practice you can
recognize the components even when they're not readily apparent.  The following
was an interchange I observed upon returning home after teaching a class on
this subject.

Daughter
    Mommy, I'm going to feed the rabbits lots of marigolds.
Wife
    Don't feed them too many until we find out they're safe for bunnies.
Daughter
    But the box of hay says it has marigolds in it.
Wife
    Oh, okay, then we already have all the information we need.

How on earth does this exchange map to the form of argument we've been using?
Let's do some translation.

Daughter:  Mommy, I'm going to feed the rabbits lots of marigolds.
    * **Implicit claim:**  Marigolds are good for bunnies to eat.
    * **Implicit qualifications:**  Always.
Wife:  Don't feed them too many until we find out they're safe for bunnies.
    I don't believe you.  What data do you have to support your claim?
Daughter:  But the box of hay says it has marigolds in it.
    * **Data:**  Rabbit hay contains marigolds.
    * **Implicit warrant:**  Rabbit food manufacturers know what is safe for
      bunnies to eat.
    * **Implicit backing:**  They stay in business.
Wife:  Oh, okay, then we already have all the information we need.
    My counter-claim has no merits, and you've won the argument.

Note that the only piece missing was conditions for rebuttal, but that's
because this particular scenario was pretty cut and dry, and any possible
rebuttals I can think of are automatically discredited by the fact that the
rabbits had already been eating this hay mixture for weeks and were still
alive and well.

Video
=====

The following is a helpful video that presents all of the above from a slightly
different perspective.

.. raw:: html

    <div style="position: relative;
                padding-bottom: 56.25%;
                height: 0;
                overflow: hidden;
                max-width: 100%;
                height: auto;">
        <iframe src="https://www.youtube.com/embed/1vArfwlX04I"
                frameborder="0"
                allowfullscreen
                style="position: absolute;
                       top: 0;
                       left: 0;
                       width: 100%;
                       height: 100%;
                       padding: 10px;">
        </iframe>
    </div>
    <br>

