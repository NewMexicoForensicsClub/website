Organizing Your Research
------------------------

:raw-html:`<div align="right">🕑 2 min.</div>`

As you go about :doc:`conducting your research <conducting-research>`, you'll
wind up with a bunch of decks of note cards from all the various sources you
consult.  Eventually you'll reach a point where you think, "Okay, at this point
I know plenty about my topic.  Now what do I do?"

Determine Your Main Points
==========================

Your next step is to organize all of the information you have on hand.  Start
by finding a large, flat, empty space to work with (a table, bed, floor, etc.).
Then grab your :ref:`evidence cards <evidence_cards>` and start arranging them
into piles, depending on what's on them.  How you group them is up to you, and
there will likely be multiple groupings that make sense.

For example, say your topic is Benjamin Franklin.  You might wind up organizing
your cards into piles about his childhood, early professional life, the events
surrounding the birth of our country, and then his later life representing the
US abroad.  On the other hand, perhaps it'd be better to group your cards into
piles based on his different careers (author, scientist, politician,
postmaster, etc.).  As you try organizing and reorganizing your cards, you'll
need to figure out what exactly it is you want to say about your topic.  Once
you have that pegged down, one particular organization will probably make more
sense than the rest.

.. note::

   As you're moving cards around, you may find that some cards don't really fit
   in any of your piles.  That's totally fine---expected, even.  Just set them
   aside in a "discard" pile, but don't throw them out.  You might come back to
   them later if you decide to reorganize your thoughts.

.. figure:: /../images/organizing-your-research.jpg

Organize Each Main Point
========================

Once you have things appropriately divided at the high level, your next task is
to organize the pile of cards you have for each main point.  Since you'll be
using all this evidence in a speech, you need to figure out the order in which
it should all be presented.  For each of your main points, start arranging the
cards from top to bottom in the order in which you'd like to go through the
information.  Feel free to shuffle things around to experiment with different
orderings.  As you do so, you may discover you actually have a few different
sub-points.  If that's the case, arrange each sub-point into a separate column.

.. figure:: /../images/organizing-your-main-points.jpg

Once you have your thoughts organized, it's time to move on to
:doc:`structuring your speech <structuring-your-speech>`.

