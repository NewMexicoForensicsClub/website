Taking Notes in Debate
----------------------

:raw-html:`<div align="right">🕑 3 min.</div>`

The process of taking notes in debate is known as *flowing*, as you'll be
tracing the flow of arguments throughout the debate.  The following video gives
you an idea of what it looks like in the context of
:doc:`/events/debates/policy-debate`, but the principles apply to the other
forms of debate as well.

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/Rf6HBKgkSAM" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

Supplies
========

The supplies you'll need for flowing include:

* **Paper:**  Plenty of it, in pad or loose-leaf form.  Legal size paper can be
  beneficial, as it gives you more space to work with.
* **Pens:**  At least two different colors (preferably three).

Setup
=====

Before the debate begins, your setup will look something like the following:

.. figure:: /../images/example-flow-setup.jpg

Paper
~~~~~

To get yourself ready to flow a debate round, take at least two sheets of paper
and divide them vertically into equal-sized columns for each of the speeches in
the debate.  Label these columns with an abbreviation for each speech (the
boxes in the top-left of each column in the figure above).  You may wish to
include the time allotted to each speech as a reminder to yourself (the circles
in the top-right of each column).  This setup will vary slightly depending on
the form of debate being used.

In addition to your two sheets of flow paper, set aside an additional sheet to
jot down questions or other thoughts on.  These notes won't necessarily fit
into the flow of the debate, but you'll want to be able to remember them in
cross-examination or in giving a speech later on.  It will also be worth your
while to jot down some generic questions before the debate begins, based on
your knowledge of the subject area.  This will make it such that you're never
trying (and failing) to think of a question on the spot during
cross-examination.

Pens
~~~~

Choose one color to represent the affirmative side of the debate, and another
distinctly different color to represent the negative side.  Flowing with two
different colors makes it such that you can easily tell at a glance who said
what.

The optional third color is for jotting questions and other thoughts on the
piece of question paper set off to the side.  Additionally, you can use it to
make notations on the flow sheets (e.g., circle/box something, make an
asterisk/star, etc.) to remind you of key things you want to bring up later
(e.g., "make sure to press them on this point").

Novice Flowing
==============

If you're relatively new to debate, your main task in flowing is going to be
keeping track of the main points your opponent makes.  These will be the
:doc:`claims <crafting-an-argument>` made in their arguments, along with the
:doc:`tag lines <conducting-research>` for their pieces of evidence.  During
the first speech, jot these points down in the order in which they occur,
leaving some space between them to potentially come back and fill in extra
details later.  In subsequent speeches, any given note should be taken to the
right of the note to which it responds in the prior speech.

.. tip::

   To save yourself time, and ensure you're as prepared as possible, fill out
   the flow for your own cases ahead of time.  That is, prepare flow sheets in
   advance, and fill in the details of your affirmative case on one, and the
   details of your negative case on another.  This way you don't need to take
   notes on your own cases during the round itself.

Advanced Flowing
================

.. warning::

   This section will be completed at some point in the future.
