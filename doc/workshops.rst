Workshops
---------

In addition to our regular club meetings, we hope to host quarterly workshops
to help equip parents to incorporate the various skill-building activities we
do into the daily and weekly rhythms of their homeschools.  These workshops are
generally geared toward parents, but the content will be appropriate for those
as young as middle school, if your children would like to attend with you.
Workshops will usualy consist of an hour of instructional content, followed by
an hour of practice in small groups, while anything labeled a "seminar" will
usually be about 90 minutes of interactive instructional content, followed by a
half-hour of Q&A.

.. raw:: html

   <p style="text-align:left;">
       <span style="float:right;">
           2024-02-03
       </span>
   </p>

Interrogation Workshop
======================

*The Art of Effective Questioning*

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/TJpUr7oTPCo" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

This is the first half of a two-hour workshop on using questions effectively in
dialogue.  We begin with why everyday believers, including your children, must
learn the discipline, and then we walk through how to use different kinds of
questions to either clarify something, make a point, or redirect a
conversation.  Your kids will frequently run into unclear communication both
outside and inside the church.  Part of our responsibility as parents is to
ensure they're well-equipped to stand firm for truth while helping others to
understand it.  Our aim in this workshop is to help you help them build some of
the skills needed to do just that.

:download:`Download the handout </../files/interrogation-workshop-handout.pdf>`

.. raw:: html

   <p style="text-align:left;">
       <span style="float:right;">
           2023-12-02
       </span>
   </p>

Hermeneutics Seminar
====================

*How Do We Interpret Scripture?*

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/sDscTZ7vFOg" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

This is an interactive seminar on biblical hermeneutics, the practice of
interpreting scripture.  We begin with why everyday believers, including your
children, must learn the discipline, after which we walk through the process of
figuring out what the text says, and then figuring out what it means to us
today.  We conclude with ways for you to incorporate the development of these
skills into your homeschool.  Your kids will frequently run into faulty
interpretations of scripture---Jesus said not to judge; God'll never give you
more than you can handle; etc.---from both outside and inside the church.  Part
of our responsibility as parents is to ensure they're well-equipped to handle
God's word rightly, and to stand firm when a hostile world tries to get them to
compromise.  Our aim in this seminar is to help you help them build some of the
skills needed to do just that.

:download:`Download the handout </../files/hermeneutics-seminar-handout.pdf>`

.. raw:: html

   <p style="text-align:left;">
       <span style="float:right;">
           2023-08-26
       </span>
   </p>

Epistemology Seminar
====================

*Skills Required for Learning and Discerning Truth*

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/86EX5awEf_U" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

This is an interactive seminar on epistemology, the study of what we can know
and how we can know it.  We begin with why everyday believers, including
teenagers, must be concerned with a fancy word like “epistemology,” after
which we construct a taxonomy of the various ways we can know things.  We then
discuss a number of skills associated with each area in the taxonomy, along
with ways for you to incorporate the development of these skills into your
homeschool.  When your children leave the house (and even before), they will be
inundated with innumerable counterfeit truth claims.  Part of our
responsibility as parents is to ensure they’re well-equipped not only to
distinguish truth from falsehood, but to stand firm for truth when pressed.
Our aim in this seminar is to give you a vision for what that entails.

:download:`Download the handout </../files/epistemology-seminar-handout.pdf>`

.. raw:: html

   <p style="text-align:left;">
       <span style="float:right;">
           2023-02-18
       </span>
   </p>

Apologetics Workshop
====================

*Incorporating Apologetics Training into Your Weekly Family Rhythm*

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/j78ZEfCjJng" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

This is the first half of a two-hour workshop on incorporating training in
apologetics into your weekly routine.  The presentation consists of instruction
on an overview of the concept of apologetics, followed by a discussion of
various tools and practices you can incorporate into your weekly routine to
prepare you to defend your faith in every-day life.  One day your kids will no
longer be under your protective care, and will be facing the full force of the
world's attacks on their beliefs.  Our aim in this workshop is to help you
prepare them both to stand firm and to train others to do the same.

:download:`Download the handout </../files/apologetics-workshop-handout.pdf>`

.. raw:: html

   <p style="text-align:left;">
       <span style="float:right;">
           2022-11-12
       </span>
   </p>

Worldview Workshop
==================

*Tools for Identifying, Analyzing, and Refuting Counterfeit Worldviews*

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/Zm8YPUQDkrw" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; padding: 10px;"></iframe>
    </div>

This is the first hour of a two-hour workshop on making worldview analysis a
part of your weekly routine.  The presentation consists of instruction on an
overview of the concept of worldview, followed by an introduction to three
tools you can use to identify, analyze, and then refute counterfeit worldviews
you encounter in every-day life.  Your child’s worldview is largely
solidified by age thirteen or so, and must be refined and tested in the years
between then and when they strike out on their own.  Our aim in this workshop
is to help you with that refinement and testing.

:download:`Download the handout </../files/worldview-workshop-handout.pdf>`

Future Workshops
================

June, 2024
  If you'll be joining us at the `CAPE Homeschool Convention`_ on June 28--29,
  we'll be offering both the **Worldview Workshop** and the **Apologetics
  Workshop** in a condensed format.

.. _CAPE Homeschool Convention:  https://www.cape-nm.org/events-convention/homeschool-convention/

