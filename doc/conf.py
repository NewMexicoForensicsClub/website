master_doc = "index"
project = "New Mexico Forensics Club"
copyright = "2020–2024, New Mexico Forensics Club"
author = "Jason M. Gates"
version = release = ""
extensions = [
    "sphinx_rtd_theme",
    "sphinxcontrib.tikz",
    "sphinxcontrib.youtube",
]
html_theme = "sphinx_rtd_theme"
tikz_proc_suite = "GhostScript"

