About
-----

Welcome to the New Mexico Forensics Club, an academic speech and debate
organization geared toward the homeschool Christian community, but open to
participants from any educational setting or faith background.

What's in a Name?
=================

Chances are you may be confused by the word "forensics" in our club name.  It's
from the Latin *forensis*, meaning "in open court" or "in public".
Historically forensics has referred to the activity of speaking or arguing in a
public forum of some sort.  It's only in the last decade or two, with the rise
of crime procedurals on television, that people have begun to conflate the term
with "forensic science", which is the scientific practice of collecting
evidence to be used "in open court" when prosecuting a criminal case.  Because
of that confusion, you'll likely hear this activity referred to today as
"speech and debate".  We prefer "forensics" because of the history tying the
practice back to antiquity.

.. _our_mission:

Our Mission
===========

The driving mission of the New Mexico Forensics Club, in a nutshell, is to save
the world.  Since only God can do that through his regenerative work in the
hearts, minds, and lives of individuals, families, and communities, what part
do we play?  We seek to develop in young men and women the skills that are
foundational to fostering open and honest dialogue---the ability to:

* communicate your views in an eloquent, effective, winsome, and gracious
  manner
* confidently speak to both friends and total strangers in intimate or public
  settings
* boldly persuade others and motivate them to action
* connect with an audience and evoke a response via performance
* know exactly what it is you believe, and why you believe it
* be aware of any holes in your own understanding
* think critically, research, analyze, and draw well-reasoned conclusions
* naturally view complex issues from multiple perspectives
* thoroughly support a viewpoint with evidence, logic, and philosophy
* ask probing questions to dig beneath the surface to the root of the issue
* find points of commonality with those holding differing perspectives

Such skills are what the world sorely needs today, in pretty much all arenas of
life---religion, politics, immigration, climate change, red or green---pick
whatever charged topic you like.  Our world continues to fall apart due to a
whole host of factors, but a problem that exacerbates all the others is we’ve
lost the ability to communicate as a people.  Our mission is to change that.

How do we do that?  By coaching both students and parents alike as they
participate in :doc:`a number of different public speaking and debate events </events>`.

National Competition Leagues
============================

We want to emphasize that competition, particularly winning in competition, is
not the goal of our endeavor, and should not be the primary motivation for any
of our participants.  That being said, it is an *exceptional* way to both
practice and learn from others in the pursuit of improving the life skills
mentioned above.  There are a handful of national leagues in which students can
compete.

.. figure:: /../images/ncfca-logo.png
   :figwidth: 100px
   :align: left

The `National Christian Forensics and Communication Association <https://www.ncfca.org/>`_
started in 1997 as the `Home School Legal Defense Association <https://hslda.org/>`_
Debate League, with the goal of training up Christian homeschool students to
learn the skills necessary to reach their world with God's truth.  In the
following decades, the NCFCA has grown and developed with regard to systems and
programs, but the core focus of their mission has not shifted.  Ultimately,
they are about making a real difference in the real world, teaching students to
"address life issues from a biblical worldview in a manner that glorifies
God."\ [#ncfca_history]_

.. note::

   To compete in NCFCA tournaments, families first `affiliate with the NCFCA
   <https://www.ncfca.org/join/>`_ and then register their students for
   tournaments themselves.

.. figure:: /../images/nsda-logo.png
   :figwidth: 100px
   :align: left

The `National Speech & Debate Association <https://www.speechanddebate.org>`_,
formerly the National Forensic League, was created in 1925 to provide
recognition and support for high school students participating in speech and
debate activities.\ [#nsda_history]_  It connects, supports, and inspires a
diverse community committed to empowering students through speech and debate,
and envisions a world in which every school provides speech and debate programs
to foster each student’s communication, collaboration, critical thinking, and
creative skills.\ [#nsda_mission_vision]_

.. note::

   The `New Mexico Speech & Debate Association <http://nmspeechanddebate.org/>`_
   is the organization that runs tournaments in the NSDA's New Mexico
   district.  To compete in these tournaments, we'll need to connect you with
   the coach of the local high school you're zoned for such that you can
   compete with that school's team.

.. figure:: /../images/stoa-logo.png
   :figwidth: 100px
   :align: left

`Stoa <https://stoausa.org/>`_, which grew out of the NCFCA in 2009, is a
speech and debate league serving the needs of privately educated Christian
homeschooling families.  Named for the long covered porches fronting or
surrounding buildings in ancient Greece, where people would gather to listen to
politicians, philosophers, and poets, Stoa believes the ability to communicate
with the spoken word is from God, and that it is only through the triune God
that we can speak with grace, truth, and beauty.  The organization helps to
develop young people into winsome and God-honoring orators in all walks of
life.  In the broad sense, Stoa embodies an entire culture of thought, public
discourse, apologetics, and intellectual inquiry.\ [#stoa_mission]_

.. note::

   To compete in Stoa tournaments, families first `register with Stoa
   <https://stoausa.org/events/membership-registration/>`_ and then register
   their students for tournaments themselves.

Our Beliefs
===========

We affirm both
`Stoa's statement of faith <https://stoausa.org/statement-of-faith/>`_ and
`the NCFCA's foundational beliefs <https://www.ncfca.org/about/foundational-beliefs/>`_,
including the Nicene Creed and the positional statements on scriptural
inerrancy and `marriage and gender <https://cbmw.org/nashville-statement>`_.
Anyone who would take up a position of leadership or teaching in the club would
be required to affirm the same.  We reserve the right to disallow any content
or behavior counter to these beliefs.  In any cases of conflict, we seek to
abide by the :download:`Peacemaker Commitment </../files/peacemaker-commitment.pdf>`.

Our Leadership
==============

Head Coach
^^^^^^^^^^

.. figure:: /../images/headshot-jason-gates.png
   :figwidth: 200px
   :align: left

Jason Gates grew up in Egypt and the United Arab Emirates, attending
international, interdenominational, evangelical churches.  His upbringing
overseas exposed him to the world of apologetics, particularly in the context
of world religions, early on in life.  When his family repatriated and settled
in small town, Oklahoma, he dove into the world of forensics, competing in
*Standard* and *Persuasive Oratory*, *Extemporaneous Speaking*, *Humorous* and
*Duo Interpretation*, and *Policy*, *Value*, and *Congressional Debate*
throughout high school.  After graduating, he helped judge tournaments for a
number of years.  Most recently, he's served Eldorado High School's speech and
debate team as their assistant coach for the 2018/2019 season, and then head
coach for the 2019/2020 season, winning the New Mexico district's "New Coach of
the Year" award.

Outside the realm of forensics, Jason and his wife Hannah homeschool their
five children in Albuquerque.  By day he's a software engineer at Sandia
National Laboratories, but in terms of gifting and passion, he's a teacher,
through and through, having taught everything from College Algebra through
Advanced Engineering Mathematics before making the jump to software
engineering.  Coaching speech and debate is squarely in the intersection of
what he's skilled in and what he's passionate about---helping people do
whatever they do better, becoming more and more the people God's designed them
to be.  Operating in that intersection is what motivates him most.

Assistant Coach
^^^^^^^^^^^^^^^

Do your worldview, skills, and passions line up with ours?  If so, this might
be your next big endeavor.  If you're interested in being a part of the New
Mexico Forensics Club, please get in touch by emailing
newmexicoforensicsclub@gmail.com.

.. rubric:: References

.. [#ncfca_history]
   "NCFCA History,"
   2020,
   National Christian Forensics and Communications Association,
   https://www.ncfca.org/about/ncfca-history/,
   retrieved September 11, 2020.

.. [#nsda_history]
   "History:  90+ Years of Commitment,"
   2020,
   National Speech & Debate Association,
   https://www.speechanddebate.org/history/,
   retrieved September 11, 2020.

.. [#nsda_mission_vision]
   "Mission & Vision,"
   2020,
   National Speech & Debate Association,
   https://www.speechanddebate.org/mission/,
   retrieved September 11, 2020.

.. [#stoa_mission]
   "Stoa Mission & Facts,"
   2017,
   Stoa Christian Homeschool Speech and Debate,
   https://stoausa.org/stoa-facts/,
   retrieved September 11, 2020.
