Spring Showcase 2023
--------------------

.. role:: raw-html(raw)
   :format: html

Opening Remarks
~~~~~~~~~~~~~~~

.. youtube:: kpuF3vr8XHI
   :aspect: 18:9

:raw-html:`<br>`

What Is Love?
~~~~~~~~~~~~~

A :doc:`/events/platform-speeches/persuasive-oratory`, written and performed
by Ms Maci Robbs.

.. youtube:: DBhuBCUbgLo
   :aspect: 18:9

:raw-html:`<br>`

A Catastrophic Mistake
~~~~~~~~~~~~~~~~~~~~~~

A :doc:`/events/platform-speeches/persuasive-oratory`, written and performed by
Mr Sebastian Searfoss.

.. youtube:: 2cYzI-sS8Ao
   :aspect: 18:9

:raw-html:`<br>`

Journey to a New Best Friend
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A :doc:`/events/platform-speeches/persuasive-oratory`, written and performed by
Mr Eyob Robbs.

.. youtube:: HSZ7oQ6dZcE
   :aspect: 18:9

:raw-html:`<br>`

Lead Your Children the Right Way
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A :doc:`/events/platform-speeches/persuasive-oratory`, written and performed by
Ms Zoe Searfoss.

.. youtube:: z3CpbYsvlSs
   :aspect: 18:9

:raw-html:`<br>`

Every 40 Seconds
~~~~~~~~~~~~~~~~

A :doc:`/events/limited-preparation-speeches/mars-hill` extemporaneous speech,
given by Ms Maci Robbs.

.. youtube:: OKY5KRvpbNk
   :aspect: 18:9

:raw-html:`<br>`

The Right Way to Build a Wall
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

An :doc:`/events/platform-speeches/informative-oratory`, written and performed
by Mr Avery Cogburn.

.. youtube:: Yi4xwHT6_YE
   :aspect: 18:9

:raw-html:`<br>`

The Invention That Changed the World
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

An :doc:`/events/platform-speeches/informative-oratory`, written and performed
by Ms Emery Robbs.

.. youtube:: PJwwnAZrpts
   :aspect: 18:9

:raw-html:`<br>`

Protect Your Children
~~~~~~~~~~~~~~~~~~~~~

A :doc:`/events/platform-speeches/persuasive-oratory`, written and performed
by Ms Dahlia Searfoss.

.. youtube:: AatbPc28I7c
   :aspect: 18:9

:raw-html:`<br>`

The Killer in Your Home
~~~~~~~~~~~~~~~~~~~~~~~

A :doc:`/events/platform-speeches/persuasive-oratory`, written and performed
by Mr Haven Cogburn.

.. youtube:: 8Pz2_tUlQQI
   :aspect: 18:9

:raw-html:`<br>`

What Bird Would You Be?
~~~~~~~~~~~~~~~~~~~~~~~

An :doc:`/events/limited-preparation-speeches/impromptu` speech, given by Mr
Avery Cogburn.

.. youtube:: wTZZYi7TtIg
   :aspect: 18:9

:raw-html:`<br>`

Does God Forgive Really Big Sins?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

An :doc:`/events/limited-preparation-speeches/apologetics` extemporaneous
speech, given by Mr Sebastian Searfoss.

.. youtube:: Z1dD_2-_cWk
   :aspect: 18:9

:raw-html:`<br>`

The Rough Rider
~~~~~~~~~~~~~~~

An :doc:`/events/platform-speeches/informative-oratory`, written and performed
by Mr Eyob Robbs.

.. youtube:: 0fNzwtk84Yw
   :aspect: 18:9

:raw-html:`<br>`

Should We Intervene?
~~~~~~~~~~~~~~~~~~~~

A :doc:`/events/platform-speeches/persuasive-oratory`, written and performed by
Ms Gemma Patrick.

.. youtube:: fl3FMUwVyOA
   :aspect: 18:9

:raw-html:`<br>`

The Genocidal Artist
~~~~~~~~~~~~~~~~~~~~

An :doc:`/events/platform-speeches/informative-oratory`, written and performed
by Mr Haven Cogburn.

.. youtube:: mI9dZ9Sfxr0
   :aspect: 18:9

:raw-html:`<br>`

Biblical Hermeneutics
~~~~~~~~~~~~~~~~~~~~~

.. youtube:: C_TRjFTBqoA
   :aspect: 18:9

:raw-html:`<br>`

The Origins of Mormonism
~~~~~~~~~~~~~~~~~~~~~~~~

An :doc:`/events/platform-speeches/informative-oratory`, written and performed
by Ms Maci Robbs.

.. youtube:: 1QJBx-G4WZI
   :aspect: 18:9

:raw-html:`<br>`

Unacceptable
~~~~~~~~~~~~

A :doc:`/events/platform-speeches/persuasive-oratory`, written and performed by
Mr Avery Cogburn.

.. youtube:: q1pfwNCkddw
   :aspect: 18:9

:raw-html:`<br>`

Persuasion
~~~~~~~~~~

A :doc:`/events/interpretations/prose` interpretation, arranged and performed
by Ms Gemma Patrick.

.. youtube:: HIx14SeNB4w
   :aspect: 18:9

:raw-html:`<br>`

Siesta Life
~~~~~~~~~~~

A :doc:`/events/platform-speeches/persuasive-oratory`, written and performed by
Ms Emery Robbs.

.. youtube:: hWYegW13Ogg
   :aspect: 18:9

:raw-html:`<br>`

