Spring Showcase 2024
--------------------

.. role:: raw-html(raw)
   :format: html

Opening Remarks
~~~~~~~~~~~~~~~

.. youtube:: Wfb-OgpZDqo
   :aspect: 18:9

:raw-html:`<br>`

Ten Minutes to Doom
~~~~~~~~~~~~~~~~~~~

A :doc:`/events/interpretations/duo-interpretation` performed by Ms Gemma
Patrick and Ms Dahlia Searfoss.

.. youtube:: JkpNCl-ZJr4
   :aspect: 18:9

:raw-html:`<br>`

The Amazing Anchoress
~~~~~~~~~~~~~~~~~~~~~

An :doc:`/events/platform-speeches/informative-oratory` performed by Ms Zoe
Searfoss.

.. youtube:: 2ecQiHU8X9o
   :aspect: 18:9

:raw-html:`<br>`

What Would Darwin Think?
~~~~~~~~~~~~~~~~~~~~~~~~

A :doc:`/events/platform-speeches/persuasive-oratory` performed by Mr Sebastian
Searfoss.

.. youtube:: 86MN18VXonk
   :aspect: 18:9

:raw-html:`<br>`

The Art of Interrogation
~~~~~~~~~~~~~~~~~~~~~~~~

.. youtube:: qS9Nfu1ViLk
   :aspect: 18:9

:raw-html:`<br>`

Wisdom vs Intelligence
~~~~~~~~~~~~~~~~~~~~~~

A :doc:`/events/limited-preparation-speeches/mars-hill` extemporaneous speech
performed by Ms Dahlia Searfoss.

.. youtube:: k3R7ngV4bnA
   :aspect: 18:9

:raw-html:`<br>`

Two Poems by Shirley Hughes
~~~~~~~~~~~~~~~~~~~~~~~~~~~

A :doc:`/events/interpretations/poetry` interpretation performed by Ms Lily
Gates.

.. youtube:: -Kdib4VpzL0
   :aspect: 18:9

:raw-html:`<br>`

Is Jesus the Promised Messiah?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

An :doc:`/events/limited-preparation-speeches/apologetics` extemporaneous
speech performed by Mr Sebastian Searfoss.

.. youtube:: jSM4XcKi6nU
   :aspect: 18:9

:raw-html:`<br>`

The Art of Argumentation
~~~~~~~~~~~~~~~~~~~~~~~~

.. youtube:: rnJAWD4G6Wc
   :aspect: 18:9

:raw-html:`<br>`

Parents Should Not Give Their Children Chores at Home
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

An :doc:`/events/debates/extemporaneous-debate` in which the students had only
~10 minutes to prepare their remarks.

.. youtube:: k7nLs96ZsdE
   :aspect: 18:9

:raw-html:`<br>`

Overcoming Adversity
~~~~~~~~~~~~~~~~~~~~

An :doc:`/events/platform-speeches/informative-oratory` performed by Ms Dahlia
Searfoss.

.. youtube:: LwxN0fCBJhc
   :aspect: 18:9

:raw-html:`<br>`

The Maltese Falcon
~~~~~~~~~~~~~~~~~~

An :doc:`/events/limited-preparation-speeches/mars-hill` extemporaneous speech
performed by Ms Gemma Patrick.

.. youtube:: 0fPWHBOS-s8
   :aspect: 18:9

:raw-html:`<br>`

We're Going on a Bear Hunt
~~~~~~~~~~~~~~~~~~~~~~~~~~

A :doc:`/events/interpretations/prose` interpretation performed by Ms Lily
Gates.

.. youtube:: mRsmq93gYHI
   :aspect: 18:9

:raw-html:`<br>`

There and Back Again:  An Author's Tale
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

An :doc:`/events/platform-speeches/informative-oratory` performed by Ms Gemma
Patrick.

.. youtube:: eS81f0PpGws
   :aspect: 18:9

:raw-html:`<br>`

