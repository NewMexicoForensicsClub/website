Spring Showcase 2021
--------------------

.. role:: raw-html(raw)
   :format: html

Opening Remarks
~~~~~~~~~~~~~~~

.. youtube:: 2t3oSWu-yoE
   :aspect: 18:9

:raw-html:`<br>`

The Magician's Nephew
~~~~~~~~~~~~~~~~~~~~~

A :doc:`/events/interpretations/prose` interpretation, arranged and performed
by Ms Aunnaleah Morlang.

.. youtube:: 2xtNk0odSHY
   :aspect: 18:9

:raw-html:`<br>`

The Gettysburg Address
~~~~~~~~~~~~~~~~~~~~~~

A :doc:`/events/platform-speeches/standard-oratory` performed by Mr Jeremiah
Morlang.

.. youtube:: dgGDAA04Okc
   :aspect: 18:9

:raw-html:`<br>`

How Can We Believe the Bible When It's Been Translated So Many Times?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

An :doc:`/events/limited-preparation-speeches/apologetics` extemporaneous
speech by Ms Charlotte Radigan.

.. youtube:: lAkhndEdA7U
   :aspect: 18:9

:raw-html:`<br>`

The Misty Mountains
~~~~~~~~~~~~~~~~~~~

A :doc:`/events/interpretations/poetry` interpretation performed by Mr Josiah
Morlang.

.. youtube:: 01qXEwW9PY4
   :aspect: 18:9

:raw-html:`<br>`

Hope
~~~~

A :doc:`/events/platform-speeches/persuasive-oratory`, written and performed by
Ms Maci Robbs.

.. youtube:: m8JldmoZmsk
   :aspect: 18:9

:raw-html:`<br>`

Ephesians 5 & 6
~~~~~~~~~~~~~~~

A scripture recitation performed by Mr Josiah Radigan.

.. youtube:: QEIqP1imBj0
   :aspect: 18:9

:raw-html:`<br>`

The Bible in 30 Minutes or Less
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A :doc:`/events/interpretations/duo-interpretation` performed by Mr Josh and Ms
Anna Radigan.

.. youtube:: zP1CS4h-WRI
   :aspect: 18:9

:raw-html:`<br>`

Proverbs 4
~~~~~~~~~~

A scripture recitation performed by Ms Jocelyn Morlang.

.. youtube:: tuYMwDJAINc
   :aspect: 18:9

:raw-html:`<br>`

21 Years
~~~~~~~~

A :doc:`/events/interpretations/poetry` interpretation performed by Ms Maci
Robbs.

.. youtube:: OYrwoW6N74g
   :aspect: 18:9

:raw-html:`<br>`

Who Will You Serve?
~~~~~~~~~~~~~~~~~~~

A :doc:`/events/interpretations/thematic-interpretation`, arranged and
performed by Mr James Radigan.

.. youtube:: _FHVaGg8edM
   :aspect: 18:9

:raw-html:`<br>`

Standing for Truth While Fighting Lies
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A :doc:`/events/platform-speeches/persuasive-oratory`, written and performed by
Ms Charlotte Radigan.

.. youtube:: 3xkkyeokw7k
   :aspect: 18:9

:raw-html:`<br>`

