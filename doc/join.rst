Join
----

Who's This For?
===============

Forensic activities are intended for both middle and high school age students.
There are certain events, or certain pieces one might select for an event, that
require a bit more developmental maturity to handle, but anyone around age 12
and up should at least be able to get a start in these activities.  If you're
wanting to give your children a head start in developing these skills, but
they're not quite old enough yet, we have some recommendations:

* Encourage them to read high-quality literature.  One of the strongest ways to
  develop as a communicator is to learn from strong communicators of the past.
  This kind of learning via osmosis happens slowly and steadily, so the longer
  the exposure, the better.
* Focus on developing them as a writer.  The skills of the written word are in
  many ways a prerequisite to the skills of the spoken word.  There are
  countless resources available to you, but one we'd recommend is the
  `Institute for Excellence in Writing <https://iew.com/>`_.
* Help them develop their memorization and recitation skills.  Start small---a
  sentence here, a paragraph there---but you'll be amazed at how well kids can
  memorize things.  Again, resources abound, but two we'd recommend are
  `AWANA <https://www.awana.org/>`_ and
  `catechism
  <http://www.foundationchurcheuless.com/Files/FamilyCatechism.pdf>`_.

What Does Involvement Look Like?
================================

For the Students
^^^^^^^^^^^^^^^^

Consider this like any of your other academic subjects.  Throughout the week,
you'll be researching, writing, memorizing, and practicing.  As with most
activities, the more you put into it, the more you'll get out of it (up to a
point---we don't want you to be unhealthy here).

When we meet, our time will be broken up as follows:

* **Instruction:**  Some of our time will be spent introducing participants to
  new concepts, whether it's something general, like how to structure a speech,
  or something more specific, like how to write a negative brief for
  :doc:`Policy Debate </events/debates/policy-debate>`.
* **Worldview Analysis:**  Each week we'll spend some time going over a prompt
  for either
  :doc:`Apologetics </events/limited-preparation-speeches/apologetics>` or
  :doc:`Mars Hill </events/limited-preparation-speeches/mars-hill>`.  The
  prompt will be assigned two weeks in advance, and participants will have the
  opportunity to present short speeches, talk through research, and swap notes.
* **Practice:**  Some of our time will also be spent writing, memorizing, and
  giving speeches or performances.  In addition to performing themselves,
  participants will also be able to act as judges, learning to offer
  constuctive criticism, and applying those lessons learned to their own
  performances.

We strongly encourage your participation in as many scrimmages and tournaments
as you can fit into your schedule.  Which ones you attend will depend on what
:doc:`events </events>` you want to compete in, and where and how often your
family is willing to travel.  We also intend to host club exhibitions once or
twice per year, which will be chances to showcase what you've been working on
to your community of friends, family, neighbors, etc.

For the Parents
^^^^^^^^^^^^^^^

Parental involvement is crucial to the success of this activity, but what it
looks like can vary.  At the very least, talk through what your student is
working on regularly in the home.  Are they staying up to date on current
events?  Talk through them at the dinner table.  Are they developing a debate
case?  Have them walk through it with you and then play devil's advocate.  Are
they working on honing a performance?  Act as their audience and provide
constructive feedback.

If you're able to join us for our club meetings, we would very much appreciate
your help.  We'll wind up training you to both coach and judge right alongside
us.  The more people we have available to work with students individually or in
small groups, the better.

Are you interested in honing these same skills in yourself?  Feel free to jump
right in and play the role of student alongside your child.  There is no age
limit on forensic activities.  Public speaking is one of the things most
commonly feared by adults---we can help you overcome that.  If you're a budding
young orator, performer, or debater, we want to help develop that in you.

There's also involvement in getting your kids to and from activities, some of
which has already been mentioned.  The amount of travel will depend on the
competitions your student would like to attend.  The NSDA scrimmages and
tournaments are largely held in Albuquerque, but a few are also in Santa Fe and
Las Cruces.  There are currently no NCFCA or Stoa competitions held in New
Mexico (yet---we'll change that), so if you'd like to travel to those, you'll
have to look out of state.  If you happen to qualify to compete at one of the
national competitions, that will likely involve travel outside New Mexico
(though the NSDA National Tournament should be coming to Albuquerque in the
summer of 2024).

.. note::

   Some, but not all, of the NCFCA and Stoa tournaments, and all of the NSDA
   scrimmages and tournaments, will be held online this season, so there's no
   travel involved for those events this year.

What Does It Cost?
==================

At the moment, the club does not charge any participation fees.  We provide
this service because it's a service that needs to be provided.  That being
said, participation is not necessarily free, as there are a number of costs
that you may need to pay, depending on what you want to do.

.. note::

   As the club grows, if we wind up needing to rent space for our events, we'll
   need to defray that cost somehow.

Resources
^^^^^^^^^

Any materials the club provides will be provided free of charge; however, both
parents and students alike can benefit significantly by investing in additional
learning materials (curricula, books, etc.).  Check out our
:doc:`Resources </resources>` page to see our recommendations.

National League Dues
^^^^^^^^^^^^^^^^^^^^

Each of the national competition leagues has its own annual registration fee,
ranging from $20 per student to $150 per family.  Which of these you pay will
depend on the tournaments you want to take part in.  See the links below for
full details:

* `NCFCA:  Family Affiliation <https://www.ncfca.org/join/affiliation-benefits/league-affiliation/family-affiliation/>`_
* `NSDA:  Explore Membership <https://www.speechanddebate.org/explore-membership-students/>`_
* `Stoa:  Membership <https://stoausa.org/events/membership-registration/>`_

.. note::

   If you wish to compete in NSDA tournaments through the NMSDA, we'll need to
   connect you with your local high school team, which will likely have its own
   dues or payment structure.

Competition Registration Fees
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Throughout the year it will be possible for students to compete in various
tournaments or scrimmages.  Registration fees will vary, but tend to be around
$10 per competition event.  Regional or national championships will be a bit
more expensive (up to $75), but those happen less frequently.  Again, the total
cost to you is going to depend on how much you want to compete, both in how
many events you'd like to do at a tournament, and how many tournaments you'd
like to participate in.

Sign Me Up
==========

.. note::

   The club is on hiatus at the moment.

