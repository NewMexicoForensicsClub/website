Events
------

There are a number of different events in which to compete in the world of
forensics---different kinds of speeches or performances to give or forms of
debate in which to engage.  They can generally be subdivided into the following
categories:

.. toctree::
   :maxdepth: 1

   events/platform-speeches
   events/limited-preparation-speeches
   events/interpretations
   events/debates

A **platform speech** is probably what comes to mind when you think of someone
giving a speech to a crowd (a commencement speech,
`TED talk <https://www.ted.com/talks>`_, sermon).  While platform speeches are
prepared and memorized well in advance, **limited preparation speeches** are
given more on the spur of the moment with research done ahead of time
(discussions of current events, apologetics).  **Interpretations** tend to
exercise a performer's acting skills (plays, monologues, poetry).  Finally
**debates** are where two or more individuals clash when arguing either in
favor of or against a particular resolution.

Each of these four categories develop and hone different sets of skills in the
individuals participating in them.  Though there's a tendency to prefer one
type of event over another, students improve best by participating in at least
one event per category.  Improving your skills in one area will improve your
abilities in all areas.
